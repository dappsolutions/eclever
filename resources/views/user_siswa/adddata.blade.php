<input type="hidden" id='id' value="{{ isset($id) ? $id : '' }}">
<input type="hidden" id='user' value="{{ isset($users) ? $users : '' }}">
<input type="hidden" id='disabled' value="{{ isset($disabled) ? $disabled : '' }}">

<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">{{ isset($title) ? $title : 'TAMBAH '.strtoupper($module) }}</div>

            <div class="card-body">
                <div class="form-layout">
                    <div class="row mg-b-25">
                        @php
                            $hide = session('access') == 'superadmin' ? '' : 'hide';
                        @endphp
                        <div class="col-lg-4 {{ $hide }}">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Sekolah : <span class="tx-danger">*</span></label>
                                <select {{ isset($disabled) ? 'disabled' : '' }} onchange="User.getListSiswa(this)" id='sekolah' class="form-control required" error='Sekolah' data-placeholder="Choose country" tabindex="-1" aria-hidden="true">
                                    @if (!empty($data_sekolah))
                                        <option label="Pilih Sekolah"></option>
                                            @foreach ($data_sekolah as $item)
                                                @php
                                                    $selected = isset($sekolah) ? $item->id == $sekolah ? 'selected' : '' : '';
                                                @endphp
                                                <option {{ $selected }} value="{{ $item->id }}">{{ $item->nama_sekolah }}</option>
                                            @endforeach
                                    @else
                                        <option value=""></option>
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force" id="content-data-guru">
                                <label class="form-control-label">Siswa : <span class="tx-danger">*</span></label>
                                <select {{ isset($readonly) ? 'disabled' : '' }} {{ isset($disabled) ? 'disabled' : '' }} id='siswa' class="form-control select2-show-search required" error='Siswa' data-placeholder="Pilih Siswa" tabindex="-1" aria-hidden="true">
                                    @if (!empty($data_siswa))
                                            @foreach ($data_siswa as $item)
                                                @php
                                                    $selected = isset($siswa) ? $item['id'] == $siswa ? 'selected' : '' : '';
                                                @endphp
                                                <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['nama'] }}</option>
                                            @endforeach
                                    @else
                                        <option value=""></option>
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Username : <span class="tx-danger">*</span></label>
                                <input {{ isset($disabled) ? 'disabled' : '' }} id='username' class="form-control required" error="Username" value="{{ isset($username) ? $username : '' }}" type="text" placeholder="Username">
                            </div>
                        </div><!-- col-4 -->

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Password : <span class="tx-danger">*</span></label>
                                <input {{ isset($disabled) ? 'disabled' : '' }} id='password' class="form-control required" error="Password" value="{{ isset($password) ? $password : '' }}" type="text" placeholder="Password">
                            </div>
                        </div><!-- col-4 -->

                    </div><!-- row -->

                    <div class="form-layout-footer">
                        <div class="text-right">
                            @if (!isset($disabled))
                                <button class="btn btn-success mg-r-5" onclick="User.submit()">Submit Form</button>
                            @endif
                            @if (!isset($back_button))
                                <button class="btn btn-secondary" onclick="User.cancel()">Batal</button>
                            @endif
                        </div>
                    </div><!-- form-layout-footer -->
                </div>
            </div>

        </div>
    </div>
</div>
<br>

<script>
    $(function(){
        if ($('#disabled').val() == '') {
            User.setSelectOption('#guru');
        }
    });
</script>
