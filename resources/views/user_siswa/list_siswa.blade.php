<label class="form-control-label">Siswa : <span class="tx-danger">*</span></label>
<select {{ isset($readonly) ? 'disabled' : '' }} {{ isset($disabled) ? 'disabled' : '' }} id='siswa' class="form-control select2-show-search required" error='Siswa' data-placeholder="Pilih Siswa" tabindex="-1" aria-hidden="true">
    @if (!empty($data_siswa))
            @foreach ($data_siswa as $item)
                @php
                    $selected = isset($siswa) ? $item['id'] == $siswa ? 'selected' : '' : '';
                @endphp
                <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['nama'] }}</option>
            @endforeach
    @else
        <option value=""></option>
    @endif
</select>
