<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Shamcey">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <!--<meta name="twitter:image" content="http://themepixels.me/shamcey/img/shamcey-social.png">-->

    <!-- Facebook -->
    <!--<meta property="og:url" content="http://themepixels.me/shamcey">-->
    <meta property="og:title" content="Shamcey">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

   <!-- <meta property="og:image" content="http://themepixels.me/shamcey/img/shamcey-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/shamcey/img/shamcey-social.png">-->
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">


    <title>{{$title_top}}</title>

    <!-- Vendor css -->
   <link href="{{ asset('assets/lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
   <link href="{{ asset('assets/lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
   <link href="{{ asset('assets/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">

    <!-- Shamcey CSS -->
   <link rel="stylesheet" href="{{ asset('assets/css/shamcey.css') }}">
   <link rel="stylesheet" href="{{ asset('assets/css/owner.css') }}">
   <script src="{{ asset('assets/lib/jquery/jquery.js') }}"></script>
   <script src="{{ asset('assets/lib/popper.js/popper.js') }}"></script>
   <script src="{{ asset('assets/lib/bootstrap/bootstrap.js') }}"></script>
   <script src="{{ asset('assets/lib/jquery-ui/jquery-ui.js') }}"></script>
   <script src="{{ asset('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
   <script src="{{ asset('assets/lib/moment/moment.js') }}"></script>
   <script src="{{ asset('assets/lib/Flot/jquery.flot.js') }}"></script>
   <script src="{{ asset('assets/lib/Flot/jquery.flot.resize.js') }}"></script>
   <script src="{{ asset('assets/lib/flot-spline/jquery.flot.spline.js') }}"></script>
   <script src="{{ asset('assets/js/shamcey.js') }}"></script>
   <script src="{{ asset('assets/js/dashboard.js') }}"></script>

    @if (isset($header_data))
        @foreach ($header_data as $key => $v_head)
            @php
                $data_key = explode('-', $key);
            @endphp
            @if ($data_key[0] == 'css')
                <link rel="stylesheet" href="{{ $v_head }}">
            @else
                <script src="{{ $v_head }}"></script>
            @endif
        @endforeach
    @endif

</head>
