<input type="hidden" id='id' value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">{{ 'DETAIL '.strtoupper($module) }}</div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-layout">
                            <div class="row mg-b-25">
                                @php
                                    $hide = session('access') == 'superadmin' ? '' : 'hide';
                                @endphp
                                <div class="col-lg-4 {{ $hide }}">
                                    <div class="form-group mg-b-10-force">
                                        <label class="form-control-label">Sekolah : <span class="tx-danger">*</span></label>
                                        <select readonly id='sekolah' class="form-control select2  required" error='Mata Pelajaran' data-placeholder="Choose country" tabindex="-1" aria-hidden="true">
                                            @if (!empty($data_sekolah))
                                                <option label="Pilih Sekolah"></option>
                                                    @foreach ($data_sekolah as $item)
                                                        @php
                                                            $selected = isset($sekolah) ? $item->id == $sekolah ? 'selected' : '' : '';
                                                        @endphp
                                                        <option {{ $selected }} value="{{ $item->id }}">{{ $item->nama_sekolah }}</option>
                                                    @endforeach
                                            @else
                                                <option value=""></option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">Nama : <span class="tx-danger">*</span></label>
                                        <input readonly id='nama' class="form-control required" error="Nama" value="{{ isset($nama) ? $nama : '' }}" type="text" placeholder="Nama">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">Nip : <span class="tx-danger">*</span></label>
                                        <input id='nip' readonly value="{{ isset($nip) ? $nip : '' }}" class="form-control required" error='NIP' type="text" placeholder="NIP">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">Alamat : <span class="tx-danger">*</span></label>
                                        <textarea name="" readonly id="alamat" class='form-control required' error="Alamat">{{ isset($alamat) ? $alamat : '' }}</textarea>
                                    </div>
                                </div><!-- col-4 -->

                            </div><!-- row -->

                            {{-- <div class="form-layout-footer">
                                <button class="btn btn-success mg-r-5" onclick="Guru.submit()">Submit Form</button>
                                <button class="btn btn-secondary" onclick="Guru.cancel()">Batal</button>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <hr>

                <div class="content-action" style="border: 1px solid #ccc;padding: 16px !important;">
                    <div class="row">
                        <div class="col-md-6">
                            <u><i><h6 class="tx-inverse mg-b-0">Mengajar Mata Pelajaran</h6></i></u>
                        </div>
                        <div class="col-md-6 text-right">
                            <i class="fa fa-pencil hover" onclick="Guru.editMapel(this)" data-toggle="tooltip" data-placement="top" title="Ubah"></i>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        @if (!empty($data_mapel_guru))
                            @foreach ($data_mapel_guru as $item)
                                <div class="col-md-3 mg-t-20 mg-lg-t-0">
                                    <label class="ckbox">
                                        @php
                                            $checked = $item->handled == '1' ? 'checked' : ''
                                        @endphp

                                        <input {{ $checked }} class='action' guru_mapel_id='{{ $item->guru_mapel_id }}' disabled type="checkbox" data_id='{{ $item->id }}'><span>{{ $item->mata_pelajaran }}</span>
                                    </label>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="btn-action hide">
                                <a href="" onclick="Guru.changeMapel(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-check"></i></div></a>
                                <a href="" onclick="Guru.cancelMapel(this, event)" class="btn btn-outline-danger btn-icon rounded-circle mg-r-5"><div><i class="fa fa-close"></i></div></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="text-right">
                    <button class="btn btn-secondary" onclick="Guru.cancel()">Kembali</button>
                </div>
            </div><!-- form-layout-footer -->
        </div>
    </div>
</div>
<br>
