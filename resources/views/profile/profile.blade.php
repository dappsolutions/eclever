<div class="row row-sm">
    <div class="col-3">
      <div class="card bd-primary">
        <div class="card-header bg-primary tx-white">
          Foto Profil
        </div><!-- card-header -->
        <div class="card-body tx-center" id="img_content">
            @php
                $image_profile = $data->foto == '' ? asset('assets/img/no_picture.jpg') : $data->foto;
            @endphp
          <img id="img_profile" src="{{ $image_profile }}" class="wd-100 rounded-circle" alt="">
          <div class="mg-t-20">
            <a href="" class="btn btn-secondary pd-x-20" onclick="Profile.gantiFoto(this, event)">Ganti Foto</a>
          </div>
        </div><!-- card-body -->
      </div><!-- card -->

      <div class="card bd-primary mg-t-20">
        <div class="card-header bg-primary tx-white">Pemberitahuan</div>
        <div class="card-body">
          <div class="d-flex">
            <div class="ckbox mg-t-2 mg-r-15">
              <input type="checkbox">
              <span></span>
            </div><!-- ckbox -->
            <span>Email me when someone mentions me.</span>
          </div>

          <div class="d-flex mg-t-10">
            <div class="ckbox mg-t-2 mg-r-15">
              <input type="checkbox">
              <span></span>
            </div><!-- ckbox -->
            <span>Email me when someone follows me.</span>
          </div>
        </div><!-- card-body -->
      </div><!-- card -->

    </div><!-- col-3 -->
    <div class="col-9">

      <div class="card bd-primary">
        <div class="card-header bg-primary tx-white">
          Informasi Login
        </div><!-- card-header -->
        <div class="card-body">
          <div class="form-group row align-items-center">
            <label class="mg-b-0 col-3 tx-right">Username</label>
            <div class="col-6">
              <input type="text" disabled class="form-control" id="username" placeholder="Enter username" value="{{ $data->username }}">
            </div><!-- col-8 -->
          </div><!-- form-group -->
          <div class="form-group row align-items-center">
            <label class="mg-b-0 col-3 tx-right">Hak Akses</label>
            <div class="col-6">
              <input disabled type="text" class="form-control" id="hak_akses" value="{{ $data->access }}">
            </div><!-- col-8 -->
          </div><!-- form-group -->
          <div class="form-group row align-items-center mg-b-0">
            <label class="mg-b-0 col-3 tx-right">Password</label>
            <div class="col-6">
              <a href="" onclick="Profile.gantiPassword(this, event)">Ganti Password</a>
            </div><!-- col-8 -->
          </div><!-- form-group -->
        </div><!-- card-body -->
      </div><!-- card -->

      <input type="hidden" value="{{ $data->profile_id }}" id="profile_id">
      <div class="card bd-primary mg-t-20">
        <div class="card-header bg-primary tx-white">
          Informasi Personal
        </div><!-- card-header -->
        <div class="card-body">
          <div class="form-group row align-items-center">
            <label class="mg-b-0 col-3 tx-right">Nama Depan</label>
            <div class="col-6">
              <input type="text" class="form-control" id="firstname" value="{{ $data->firstname }}">
            </div><!-- col-8 -->
          </div><!-- form-group -->
          <div class="form-group row align-items-center">
            <label class="mg-b-0 col-3 tx-right">Nama Belakang</label>
            <div class="col-6">
              <input type="text" class="form-control" id="lastname" value="{{ $data->lastname }}">
            </div><!-- col-8 -->
          </div><!-- form-group -->
          <div class="form-group row">
            <label class="mg-b-0 col-3 tx-right mg-t-10">Tentang</label>
            <div class="col-8">
              <textarea class="form-control" id="tentang" rows="4">{{ $data->tentang }}</textarea>
            </div><!-- col-8 -->
          </div><!-- form-group -->
        </div><!-- card-body -->
      </div><!-- card -->

      <input type="hidden" value="{{ $data->media_id }}" id="media_id">
      <div class="card bd-primary mg-t-20">
        <div class="card-header bg-primary tx-white">
          Media Sosial
        </div><!-- card-header -->
        <div class="card-body">
          <div class="form-group row align-items-center">
            <label class="mg-b-0 col-3 tx-right">Facebook</label>
            <div class="col-6">
              <input type="text" class="form-control" id="facebook" value="{{ $data->facebook }}">
            </div><!-- col-8 -->
          </div><!-- form-group -->
          <div class="form-group row align-items-center">
            <label class="mg-b-0 col-3 tx-right">Twitter</label>
            <div class="col-6">
              <input type="text" class="form-control" id="twitter" value="{{ $data->twitter }}">
            </div><!-- col-8 -->
          </div><!-- form-group -->
          <div class="form-group row align-items-center">
            <label class="mg-b-0 col-3 tx-right">Instagram</label>
            <div class="col-6">
              <input type="text" class="form-control" id="instagram" value="{{ $data->instagram }}">
            </div><!-- col-8 -->
          </div><!-- form-group -->
          <div class="form-group row align-items-center">
            <label class="mg-b-0 col-3 tx-right">Whatsapp</label>
            <div class="col-6">
              <input type="text" class="form-control" id="wa" value="{{ $data->wa }}">
            </div><!-- col-8 -->
          </div><!-- form-group -->
        </div><!-- card-body -->
      </div><!-- card -->
    </div><!-- col-9 -->
  </div><!-- row -->

  <hr class="mg-y-30">

  <div class="text-right">
    <button class="btn btn-success pd-x-20 mg-r-2" onclick="Profile.submit()">Save Changes</button>
    <button class="btn btn-secondary pd-x-20" onclick="Profile.cancel()">Batal</button>
  </div>

  <hr class="mg-t-30">
