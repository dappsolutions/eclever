<div class="card bd-primary mg-t-20">
    <div class="card-header bg-primary tx-white">
        Ganti Password
    </div>
    <div class="card-body">
        <div class="form-group row align-items-center">
            <label class="mg-b-0 col-5 tx-right">Password Lama</label>
            <div class="col-6">
              <input type="text" class="form-control" id="password_lama" placeholder="Password Lama">
            </div><!-- col-8 -->
        </div><!-- form-group -->
        <div class="form-group row align-items-center">
            <label class="mg-b-0 col-5 tx-right">Password Baru</label>
            <div class="col-6">
              <input type="text" class="form-control" id="password_baru" placeholder="Password Baru">
            </div><!-- col-8 -->
        </div><!-- form-group -->
    </div>

    <div class="card-footer">
        <div class="text-right">
            <button class="btn btn-success pd-x-20 mg-r-2" onclick="Profile.submitPassword()">Save Changes</button>
        </div>
    </div>
</div>
