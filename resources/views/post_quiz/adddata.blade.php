<input type="hidden" id='id' value="{{ isset($id) ? $id : '' }}">
<input type="hidden" id='disabled' value="{{ isset($disabled) ? $disabled : '' }}">
<input type="hidden" id='total_soal_ujian' value="{{ isset($data_ujian_has_soal) ? count($data_ujian_has_soal) : '0' }}">

<div class="card bd-primary mg-t-20">
    <div class="card-header bg-primary tx-white">Tambah Quiz</div>
    <div class="card-body pd-sm-30">
        <p class="mg-b-20 mg-sm-b-30">Langkah pembuatan Quiz.</p>
        <div class="" style="border: 1px solid #ccc;padding: 16px !important;">
            <label for="" class="col-sm-2">UJIAN TERBUKA </label> :<i class="col-sm-9">Ujian yang dapat dilakukan pada jam yg tidak terikat.</i>
            <br>
            <label for="" class="col-sm-2">UJIAN TERTUTUP</label> : <i class="col-sm-9">Ujian yang harus dikerjaan saat pada jam tertentu.</i>
        </div>
        <br>

        <div id="form-quiz">
            <h3>Setting Quiz</h3>
            <section>
                <h5>Pengaturan Quiz</h5>
                <hr>
                @php
                    $hide = session('access') == 'superadmin' ? '' : 'hide';
                @endphp
                <div class="form-group wd-xs-300 {{ $hide }}">
                    <label class="form-control-label">Sekolah : <span class="tx-danger">*</span></label>
                    <select {{ isset($disabled) ? $disabled : '' }} id='sekolah' class="form-control select2 required" error='Sekolah' tabindex="-1" aria-hidden="true">
                        @if (!empty($data_sekolah))
                            <option label="Pilih Sekolah"></option>
                                @foreach ($data_sekolah as $item)
                                    @php
                                        $selected = isset($sekolah) ? $item->id == $sekolah ? 'selected' : '' : '';
                                    @endphp
                                    <option {{ $selected }} value="{{ $item->id }}">{{ $item->nama_sekolah }}</option>
                                @endforeach
                        @else
                            <option value=""></option>
                        @endif
                    </select>
                </div><!-- form-group -->
                <div class="form-group wd-xs-300">
                    <label class="form-control-label">Judul : <span class="tx-danger">*</span></label>
                    <input {{ isset($disabled) ? $disabled : '' }} id="judul" class="form-control required" error="Judul" name="judul" placeholder="Masukkan Judul" type="text" value="{{ isset($judul) ? $judul : '' }}">
                </div><!-- form-group -->
                <div class="form-group wd-xs-300">
                    <label class="form-control-label">Kategori Ujian : <span class="tx-danger">*</span></label>
                    <select disabled id='kategori_ujian' class="form-control select2 required" error='Kategori Ujian' tabindex="-1" aria-hidden="true">
                        <option value="1" {{ isset($kategori_ujian) ? $kategori_ujian == '1' ? 'selected' : '' : '' }}>TERBUKA</option>
                        <option value="2" {{ isset($kategori_ujian) ? $kategori_ujian == '2' ? 'selected' : '' : '' }}>TERTUTUP</option>
                    </select>
                </div><!-- form-group -->
                <div class="form-group wd-xs-300">
                    <label class="form-control-label">Mata Pelajaran : <span class="tx-danger">*</span></label>
                    <select {{ isset($disabled) ? $disabled : '' }} id='mapel' class="form-control select2 required" error='Mata Pelajaran' tabindex="-1" aria-hidden="true">
                        @if (!empty($data_mapel))
                            <option label="Pilih Mata Pelajaran"></option>
                                @foreach ($data_mapel as $item)
                                    @php
                                        $selected = isset($mata_pelajaran) ? $item['id'] == $mata_pelajaran ? 'selected' : '' : '';
                                    @endphp
                                    <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['mata_pelajaran'] }}</option>
                                @endforeach
                        @else
                            <option value=""></option>
                        @endif
                    </select>
                </div><!-- form-group -->

                @if (!isset($disabled))
                    <div class="btn-action text-right">
                        <a href="" onclick="PostQuiz.submitQuiz(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-check"></i></div></a>
                    </div>
                @endif
            </section>


            <h3>Setting Quiz Soal</h3>
            <section>
                <h5>Pengaturan Soal Quiz</h5>
                <hr>
                @if (!isset($disabled))
                    <div class="form-group wd-xs-300">
                        <label class="form-control-label">Kategori Soal : <span class="tx-primary">*</span></label>
                        <select id='kategori_soal' class="form-control select2 " tabindex="-1" aria-hidden="true" onchange="PostQuiz.getListSoal(this)">
                            @if (isset($data_kategori_soal))
                                @if (!empty($data_kategori_soal))
                                    <option label="Pilih Mata Kategori Soal"></option>
                                        @foreach ($data_kategori_soal as $item)
                                            <option value="{{ $item['id'] }}">{{ $item['kategori'] }}</option>
                                        @endforeach
                                @else
                                    <option value=""></option>
                                @endif
                            @endif
                        </select>
                    </div><!-- form-group -->
                @endif

                <div class="form-group">
                   <div class="row">
                       @if (!isset($disabled))
                        <div class="col-md-6">
                            <label class="form-control-label">Daftar soal berdasarkan kategori : <span class="tx-primary">*</span></label>
                            <br>
                            <div class="content-soal-list" style="border: 1px solid #ccc;padding: 16px !important;">
                                <div class="input-group">
                                    <input type="search" class="form-control" placeholder="Cari Soal" onkeyup="PostQuiz.searchInTableSoal(this, event)">
                                    <span class="input-group-btn">
                                        <button class="btn"><i class="fa fa-search"></i></button>
                                    </span><!-- input-group-btn -->
                                </div>
                                <br>
                                <div class="content-list-soal">
                                    <div class="table-responsive">
                                        <table class="table" id="tb-list-soal">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Kategori Soal</th>
                                                    <th>Soal</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="4">Tidak ada soal ditemukan</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                       @endif

                        <div class="{{ !isset($disabled) ? 'col-md-6' : 'col-md-12' }}">
                            <label class="form-control-label">Soal Terpilih : <span class="tx-danger">*</span></label>
                            <div class="content-soal-choosed" style="border: 1px solid #ccc;padding: 16px !important;">
                                <div class="table-responsive">
                                    <table class="table" id="tb-soal-ujian">
                                        <thead>
                                            <tr>
                                                <td>No</td>
                                                <td>Kategori Soal</td>
                                                <td>Soal</td>
                                                @if (!isset($disabled))
                                                    <td>Action</td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td colspan="{{ !isset($disabled) ? '4' : '3' }}"><i>Total Soal : {{ isset($data_ujian_has_soal) ? count($data_ujian_has_soal) : '0' }}</i></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (isset($data_ujian_has_soal))
                                                @if (!empty($data_ujian_has_soal))
                                                    @php
                                                        $no =1;
                                                    @endphp
                                                    @foreach ($data_ujian_has_soal as $item)
                                                        <tr data_id="{{ $item['id'] }}">
                                                            <td>{{ $no++ }}</td>
                                                            <td>{{ $item['kategori'] }}</td>
                                                            <td>
                                                                <a href="" soal="{{ $item['soal'] }}" onclick="PostQuiz.detailSoal(this, event)">{!! $item['soal_term'] !!}</a>
                                                            </td>
                                                            @if (!isset($disabled))
                                                                <td>
                                                                    <a href="" onclick="PostQuiz.deleteSoal(this, event)" class="btn btn-outline-danger btn-icon rounded-circle"><div><i class="fa fa-trash"></i></div></a>
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="3">Belum ada soal yang dipilih</td>
                                                    </tr>
                                                @endif
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                   </div>
                </div><!-- form-group -->
            </section>

            <!--Ujian Tertutup-->
            @if (isset($kategori_ujian))
                @if ($kategori_ujian == '2')
                    <h3>Setting Events Quiz</h3>
                    <section>
                        <h5>Pengaturan Event Quiz</h5>
                        <hr>
                        @if (isset($form_tanggal_ujian))
                        {!! $form_tanggal_ujian !!}
                        @endif
                    </section>
                @endif
            @endif

            @if (isset($form_penilaian_soal))
                <h3>Setting Penilaian Quiz</h3>
                <section>
                    <h5>Pengaturan Penilaian Quiz</h5>
                    <hr>
                    {!! $form_penilaian_soal !!}
                </section>
            @endif

            @if (isset($form_jadwal_ujian))
                <h3>Setting Jadwal Quiz</h3>
                <section>
                    <h5>Pengaturan Jadwal Quiz</h5>
                    <hr>
                    {!! $form_jadwal_ujian !!}
                </section>
            @endif

            @if (isset($form_peserta))
                <h3>Setting Peserta Quiz</h3>
                <section>
                    <h5>Pengaturan Peserta Quiz</h5>
                    <hr>
                    {!! $form_peserta !!}
                </section>
            @endif

            <h3>Setting Lain - Lain</h3>
            <section>
                <h5>Pengaturan Soal dan Jawaban</h5>
                <hr>
                <div class="form-layout">
                    <div class="row mg-b-25">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="ckbox">
                                    <input type="checkbox" id_shuffle='{{ isset($id_shuffle) ? $id_shuffle : '' }}'  {{ isset($shuffle_question) ? $shuffle_question == '1' ? 'checked': '' : '' }} id="check_acak_soal"><span>Soal Acak</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="ckbox">
                                    <input type="checkbox" id_shuffle='{{ isset($id_shuffle) ? $id_shuffle : '' }}' {{ isset($shuffle_answer) ? $shuffle_answer == '1' ? 'checked': '': '' }} id="check_acak_jawaban"><span>Jawaban Acak</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label">Soal Per Halaman : <span class="tx-danger">*</span></label>
                                <input field="question_per_page" onkeyup="PostQuiz.changeSoalPerPage(this)" id_shuffle='{{ isset($id_shuffle) ? $id_shuffle : '' }}' {{ isset($disabled) ? $disabled : '' }} id="question_per_page" class="form-control text-right required col-sm-3" error="Soal Per Halaman" placeholder="Soal Per Halaman" type="number" min="0" max="{{ isset($data_ujian_has_soal) ? count($data_ujian_has_soal) : '0' }}" value="{{ isset($question_per_page) ? $question_per_page : '0' }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label">Remidial : <span class="tx-danger">*</span></label>
                                <input field="remidial" id_shuffle='{{ isset($id_shuffle) ? $id_shuffle : '' }}' {{ isset($disabled) ? $disabled : '' }} id="remidial" class="form-control text-right required col-sm-3" error="Soal Per Halaman" placeholder="Remidial" type="number" min="0" value="{{ isset($remidial) ? $remidial : '' }}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="form-control-label">Penilaian : <span class="tx-danger">*</span></label>
                                <select {{ isset($disabled) ? 'disabled' : '' }} name="" id="grading_method" class="form-control required" error="Penilaian">
                                    @if (!empty($list_grade))
                                        <option value="">Penilaian</option>
                                        @foreach ($list_grade as $item)
                                            @php
                                                $selected = isset($grading_method) ? $item['id'] == $grading_method ? 'selected' : '' : ''
                                            @endphp
                                            <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['penilaian'] }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                @if (!isset($disabled))
                    <div class="text-right">
                        <a href="" onclick="PostQuiz.submitSetting(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-check"></i></div></a>
                    </div>
                @endif
            </section>
        </div>
    </div><!-- card-body -->
</div><!-- card -->
<script>
    $(function(){
        PostQuiz.setFormQizard();
        PostQuiz.setDatePicker();
        PostQuiz.finish();
    });
</script>
