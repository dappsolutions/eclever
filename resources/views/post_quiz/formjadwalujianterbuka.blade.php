
<input type="hidden" id='ujian_time_open' value="{{ !empty($ujian_open) ? $ujian_open['id'] : '' }}">

@if (isset($kategori_ujian))
    <!--Hanya untuk kategori ujian yang terbuka-->
    @if ($kategori_ujian == '1')
        <div class="form-group wd-xs-300">
            <label class="form-control-label">Batas Waktu : <span class="tx-danger">*</span></label>
            @if (isset($data_time_limit))
                <select {{ isset($disabled) ? $disabled : '' }} name="" id="time_limit" class="form-control {{ !isset($disabled) ? 'required' : '' }}" error="Batas Waktu">
                    @if (!empty($data_time_limit))
                        <option value="">Pilih Batas Waktu</option>
                        @foreach ($data_time_limit as $item)
                            @php
                                $selected = !empty($ujian_open) ? $ujian_open['time_limit'] == $item['id'] ? 'selected' : '' : ''
                            @endphp
                            <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['time_limit'].' Menit' }}</option>
                        @endforeach
                    @endif
                </select>
            @endif
        </div>
    @endif
@endif

<div class="form-group wd-xs-300">
    <label class="form-control-label">Dibuka Tanggal : <span class="tx-danger">*</span></label>
    <div class="input-group">
        @php
            $tanggal = !empty($ujian_open) ? date('m/d/Y', strtotime($ujian_open['tanggal_ujian'])).' - '.date('m/d/Y', strtotime($ujian_open['tanggal_akhir'])) : '';
        @endphp
        <input {{ isset($disabled) ? $disabled : '' }} readonly id="{{ !isset($disabled) ? 'tanggal' : '' }}" class="form-control {{ !isset($disabled) ? 'required' : '' }}" error="Tanggal" name="tanggal" placeholder="Tanggal" type="text" value="{{ $tanggal }}">
        <span class="input-group-btn">
            <button class="btn"><i class="fa fa-calendar"></i></button>
        </span><!-- input-group-btn -->
    </div>
</div>


@if (!isset($disabled))
    <div class="btn-action text-right">
        <a href="" onclick="PostQuiz.submitJadwalOpenQuiz(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-check"></i></div></a>
    </div>
@endif
