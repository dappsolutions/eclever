<div class="table-responsive">
    <table class="table" id="tb-soal-ujian">
        <thead>
            <tr>
                <td>No</td>
                <td>Kategori Soal</td>
                <td>Soal</td>
                <td>Action</td>
            </tr>
            <tr>
                <td colspan="4"><i>Total Soal : {{ count($data) }}</i></td>
            </tr>
        </thead>
        <tbody>
            @if (isset($data))
                @if (!empty($data))
                    @php
                        $no =1;
                    @endphp
                    @foreach ($data as $item)
                        <tr data_id="{{ $item['id'] }}">
                            <td>{{ $no++ }}</td>
                            <td>{{ $item['kategori'] }}</td>
                            <td>
                                <a href="" soal="{{ $item['soal'] }}" onclick="PostQuiz.detailSoal(this)">{!! $item['soal_term'] !!}</a>
                            </td>
                            <td>
                                <a href="" onclick="PostQuiz.deleteSoal(this, event)" class="btn btn-outline-danger btn-icon rounded-circle"><div><i class="fa fa-trash"></i></div></a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">Belum ada soal yang dipilih</td>
                    </tr>
                @endif
            @endif
        </tbody>
    </table>
</div>
