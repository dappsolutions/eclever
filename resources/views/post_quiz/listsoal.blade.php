<div class="table-responsive">
    <table class="table" id="tb-list-soal">
        <thead>
            <tr>
                <th>No</th>
                <th>Kategori Soal</th>
                <th>Soal</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if (!empty($data))
                @php
                    $no = 1;
                @endphp
                @foreach ($data as $item)
                    <tr data_id="{{ $item['id'] }}">
                        <td>{{ $no++ }}</td>
                        <td>{{ $item['kategori'] }}</td>
                        <td>
                            <a href="" soal="{{ $item['id'] }}" onclick="PostQuiz.detailSoal(this, event)">{!! $item['soal'] !!}</a>
                        </td>
                        <td>
                            @if ($item['soal_id'] == '')
                                <button class="btn btn-outline-primary btn-block mg-b-10" onclick="PostQuiz.chooseSoal(this)">Pilih</button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="4">Tidak ada soal ditemukan</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
