<table class="table" id="tb_siswa_ujian">
    <thead>
        <tr>
            <td>No</td>
            <td>Kelas</td>
            <td>Nama</td>
            @if (count($data_peserta_ujian) > 0)
                @if ($data_peserta_ujian[0]['jam_awal'] != '')
                    <td>Jadwal</td>
                @endif
            @endif
            @if (!isset($disabled))
                <td>Action</td>
            @endif
        </tr>
        <tr>
            <td colspan="{{ !isset($disabled) ? '5' : '4' }}"> Total Peserta : {{ count($data_peserta_ujian) }}</td>
        </tr>
    </thead>
    <tbody>
        @if (!empty($data_peserta_ujian))
            @php
                $no = 1
            @endphp
            @foreach ($data_peserta_ujian as $item)
                <tr data_id="{{ $item['id'] }}">
                    <td>{{ $no++ }}</td>
                    <td>
                        <u>{{ $item['nama_kelas'] }}</u>
                        <br>
                        {{ $item['nama_jurusan'] }}
                    </td>
                    <td>{{ $item['nama'] }}</td>
                    @if ($item['jam_awal'] != '')
                        <td>{{ $item['jam_awal'].' - '.$item['jam_akhir'] }}</td>
                    @endif
                    @if (!isset($disabled))
                        <td>
                            <a href="" onclick="PostQuiz.deleteSiswa(this, event)" class="btn btn-outline-danger btn-icon rounded-circle"><div><i class="fa fa-trash"></i></div></a>
                        </td>
                    @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4">Tidak ada data ditemukan</td>
            </tr>
        @endif
    </tbody>
</table>
