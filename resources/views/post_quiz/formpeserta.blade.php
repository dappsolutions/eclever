@if (!isset($disabled))
    <div class="form-group wd-xs-300">
        <label class="form-control-label">Kelas : <span class="tx-primary">*</span></label>
        <select id='kelas' class="form-control select2" tabindex="-1" aria-hidden="true" onchange="PostQuiz.getListSiswa(this)">
            @if (isset($data_kelas))
                @if (!empty($data_kelas))
                    <option label="Pilih Kelas"></option>
                        @foreach ($data_kelas as $item)
                            <option value="{{ $item['id'] }}">{{ $item['nama_kelas'].' - ['.$item['nama_jurusan'].']' }}</option>
                        @endforeach
                @else
                    <option value=""></option>
                @endif
            @endif
        </select>
    </div>
@endif

<div class="form-group">
    <div class="row">
        @if (!isset($disabled))
            <div class="col-md-6">
                <label class="form-control-label">Daftar Siswa : <span class="tx-danger">*</span></label>
                <br>
                <div class="input-group">
                    <input type="search" class="form-control" placeholder="Cari Siswa" onkeyup="PostQuiz.searchInTableSiswa(this, event)">
                    <span class="input-group-btn">
                        <button class="btn"><i class="fa fa-search"></i></button>
                    </span><!-- input-group-btn -->
                </div>
                <br>

                <div class="table-responsive">
                    <table class="table" id="tb_list_siswa">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Kelas</td>
                                <td>Nama Siswa</td>
                                <td class="text-center">
                                    <label class="ckbox">
                                        <input type="checkbox" onchange="PostQuiz.checkAllSiswa(this)" id="check_head_siswa"><span>&nbsp;</span>
                                    </label>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="4">Tidak ada siswa ditemukan</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="text-right hide" id="action-submit-siswa">
                    <a href="" onclick="PostQuiz.submitChooseSiswa(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-check"></i></div></a>
                </div>
            </div>
        @endif

        <div class="{{ !isset($disabled) ? 'col-md-6' : 'col-md-12' }}">
            <label class="form-control-label">Daftar Siswa Terpilih : <span class="tx-danger">*</span></label>
            <br>
            <div class="siswa-choosed" style="border: 1px solid #ccc;padding: 16px !important;">
                <div class="table-responsive" id="content-data-siswa-ujian">
                    @if (isset($data_peserta_ujian))
                        {!! $list_siswa_ujian !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
