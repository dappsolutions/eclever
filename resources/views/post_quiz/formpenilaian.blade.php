
<div class="row">
    <div class="col-md-12">
        <div class="content-action" style="border: 1px solid #ccc;padding: 16px !important;">
            <div class="row">
                <div class="col-md-6">
                    <u><i><h6 class="tx-inverse mg-b-0">Penilaian</h6></i></u>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-md-4">
                    <label class="form-control-label">Dasar Penilaian : <span class="tx-danger">*</span></label>
                    <select data_id="{{ isset($penilaian_id) ? $penilaian_id : '' }}" name="" onchange="PostQuiz.setDasarPenliaian(this)" class="form-control required" error="Penilaian" id="penilaian">
                        <option value="1" {{ isset($penilaian) ? $penilaian == '1' ? 'selected' : '' : '' }}>SOAL</option>
                        <option value="2" {{ isset($penilaian) ? $penilaian == '2' ? 'selected' : '' : '' }}>JAWABAN</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<br>

@php
    $hide_soal = $penilaian != '' ? $penilaian == '1' ? '' : 'hide' : '';
    $hide_jawaban = $penilaian != '' ? $penilaian == '2' ? '' : 'hide' : '';
@endphp

<div class="row">
    <div class="col-md-12 text-right">
        <h5>Total Soal : {{ count($list_soal_and_answer) }}</h5>
        @if ($hide_soal != 'hide')
            <h5 class="total-poin">Total Poin : <label for="" id="total-poin">0</label></h5>
        @endif
    </div>
</div>
<hr>

<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table" id="tb_data_soal">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Soal</th>
                        <th class="">Poin</th>
                    </tr>
                </thead>
                <tbody>
                    @if (!empty($list_soal_and_answer))
                        @php
                            $no = 1;
                        @endphp
                        @foreach ($list_soal_and_answer as $item)
                            <tr ujian_soal_nilai_id="{{ $item['ujian_soal_nilai_id'] }}"
                            ujian_soal_id="{{ $item['id'] }}"
                            class="data_soal"
                            class_jawaban="data_jawaban_{{ $item['id'] }}">
                                <td rowspan="{{ count($item['detail_jawaban'])+1 }}">{{ $no++ }}</td>
                                <td class="text-primary">{!! $item['soal_term'] !!}</td>
                                <td class="text-right">
                                    <input type="number" value="{{ $item['poin_soal'] == '' ? 0 : $item['poin_soal'] }}" style="width:100px;" onkeyup="PostQuiz.hitungTotalPoinSoal(this, event)" onchange="PostQuiz.hitungTotalPoinSoal(this, event)" class="form-control text-right poin-soal {{ $hide_soal }}">
                                </td>
                            </tr>
                            @if (!empty($item['detail_jawaban']))
                                @foreach ($item['detail_jawaban'] as $item_ans)
                                    @php
                                        $is_true = $item_ans['true_or_false'] == 1 ? 'text-success' : '';
                                    @endphp
                                    <tr ujian_soal_nilai_id="{{ $item['ujian_soal_nilai_id'] }}"
                                    ujian_soal_id="{{ $item['id'] }}"
                                    ujian_jawaban_id="{{ $item_ans['ujian_jawaban_id'] }}"
                                    jawaban_id="{{ $item_ans['jawaban_id'] }}"
                                    class="data_jawaban_{{ $item['id'] }}"
                                    >
                                        <td class="{{ $is_true }}">{!! $item_ans['jawaban'] !!}</td>
                                        <td class="text-right">
                                            <input style="width:100px;" value="{{ $item_ans['poin_jawaban'] == '' ? 0 : $item_ans['poin_jawaban'] }}" type="number" class="form-control text-right poin-jawaban {{ $hide_jawaban }}">
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3">Tidak ada jawaban</td>
                                </tr>
                            @endif
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">Tidak ada data ditemukan</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="text-right">
            <a href="" onclick="PostQuiz.submitPenilaian(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-check"></i></div></a>
        </div>
    </div>
</div>


