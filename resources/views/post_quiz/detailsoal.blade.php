<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">
                Jawaban Soal
            </div>
            <br>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table  table-bordered" id="tb-list-soal">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Jawaban</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($data))
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($data as $item)
                                    <tr data_id="{{ $item['id'] }}">
                                        <td>{{ $no++ }}</td>
                                        @php
                                            $tx_color = $item['true_or_false'] == '1' ? 'text-success' : ''
                                        @endphp
                                        <td class="{{ $tx_color }}">{!! $item['jawaban'] !!}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">Tidak ada soal ditemukan</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
