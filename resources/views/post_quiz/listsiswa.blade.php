@if (!empty($data))
    @php
        $no = 1;
    @endphp
    @foreach ($data as $item)
        <tr data_id="{{ $item['id'] }}">
            <td>{{ $no++ }}</td>
            <td>
                <u>{{ $item['nama_kelas'] }}</u>
                <br>
                {{ $item['nama_jurusan'] }}
            </td>
            <td>{{ $item['nama'] }}</td>
            <td class="text-center">
                @if ($item['siswa_ujian_id'] == '')
                    <label class="ckbox not-checked">
                        <input type="checkbox" onchange="PostQuiz.checkedSiswa(this)" class="check_siswa"><span>&nbsp;</span>
                    </label>
                @else
                    <label class="ckbox checked">
                        <input type="checkbox" checked disabled class="check_siswa"><span>&nbsp;</span>
                    </label>
                @endif
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="4">Tidak ada siswa ditemukan</td>
    </tr>
@endif
