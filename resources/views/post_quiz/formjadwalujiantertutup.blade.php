<div class="form-group wd-xs-300">
    <select {{ !isset($disabled) ? '' : 'disabled' }} name="" id="jam_pelajaran" class="form-control" onchange="PostQuiz.getListDataJamPelajaran(this)">
        @if (!empty($list_jam_pelajaran))
            <option value="">Pilih Jam Pelajaran</option>
            @foreach ($list_jam_pelajaran as $item)
                <option value="{{ $item['id'] }}">{{ $item['judul'] }}</option>
            @endforeach
        @endif
    </select>
</div>

<div class="form-group">
    <div class="table-responsive">
        <table class="table" id="tb_list_jam_pelajaran">
            <thead>
                <tr>
                    <th>Jam Ke</th>
                    <th>Jam Awal</th>
                    <th>Jam Akhir</th>
                    <th class="text-center">
                        <label class="ckbox">
                            <input type="checkbox" onchange="PostQuiz.checkedAllJamPelajaran(this)" id="check-head-jam"><span>&nbsp;</span>
                        </label>
                    </th>
                </tr>
            </thead>
            <tbody class="tdata-jadwal-jam">
                {!! $view_list_jam_pelajaran !!}
            </tbody>
        </table>
    </div>
</div>

<div id="action-submit-jam" class="hide text-right">
    <a href="" onclick="PostQuiz.submitJamQuiz(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-check"></i></div></a>
</div>


