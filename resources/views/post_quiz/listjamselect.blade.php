

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <select name="" id="jam_select" class="form-control" onchange="PostQuiz.setJamUjianSiswa(this)">
                @if (!empty($data))
                <option value="">Pilih Jam Ujian Siswa</option>
                    @foreach ($data as $item)
                        <option value="{{ $item['ujian_jadwal_id'] }}">{{ $item['jam_awal'].' - '.$item['jam_akhir'] }}</option>
                    @endforeach
                @endif
            </select>
        </div>

    </div>
</div>
