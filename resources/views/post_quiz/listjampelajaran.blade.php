@if (!empty($data))
@php
$no = 1;
@endphp
@foreach ($data as $item)
<tr data_id="{{ $item['id'] }}">
    <td>{{ $no++ }}</td>
    <td>
        <u>{{ $item['jam_awal'] }}</u>
    </td>
    <td>
        <u>{{ $item['jam_akhir'] }}</u>
    </td>
    <td class="text-center">
        @if ($item['ujian_jadwal_id'] == '')
        <label class="ckbox not-checked">
            <input type="checkbox" onchange="PostQuiz.checkedJamPelajaran(this)" class="check_jam"><span>&nbsp;</span>
        </label>
        @else
        <label class="ckbox checked">
            <input type="checkbox" checked disabled class="check_jam"><span>&nbsp;</span>
        </label>
        @endif
    </td>
</tr>
@endforeach
@else
<tr>
    <td colspan="4">Tidak ada siswa ditemukan</td>
</tr>
@endif
