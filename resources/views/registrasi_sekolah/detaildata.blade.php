<input type="hidden" id='id' value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">{{ 'DETAIL '.strtoupper($module) }}</div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-layout">
                            <div class="row mg-b-25">

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">Sekolah : <span class="tx-danger">*</span></label>
                                        <input readonly id='sekolah' class="form-control required" error="Sekolah" value="{{ isset($nama_sekolah) ? $nama_sekolah : '' }}" type="text" placeholder="Sekolah">
                                    </div>
                                </div><!-- col-4 -->

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">Email : <span class="tx-danger">*</span></label>
                                        <input readonly id='email' class="form-control required" error="Email" value="{{ isset($email) ? $email : '' }}" type="text" placeholder="Email">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">No Telp/Hp : <span class="tx-danger">*</span></label>
                                        <input readonly id='no_telp' class="form-control required" error="No Telp/Hp" value="{{ isset($no_telp) ? $no_telp : '' }}" type="text" placeholder="No Telp/Hp">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">Alamat : <span class="tx-danger">*</span></label>
                                        <textarea readonly name="" id="alamat" class='form-control required' error="Alamat">{{ isset($alamat) ? $alamat : '' }}</textarea>
                                    </div>
                                </div><!-- col-4 -->

                                @if (!empty($data_user))
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">Username : <span class="tx-danger">*</span></label>
                                        <input readonly class="form-control required" value="{{ isset($data_user[0]['username']) ? $data_user[0]['username'] : '' }}" type="text">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">Password : <span class="tx-danger">*</span></label>
                                        <input readonly class="form-control required" value="{{ isset($data_user[0]['password']) ? $data_user[0]['password'] : '' }}" type="text">
                                    </div>
                                </div><!-- col-4 -->
                                @endif
                            </div><!-- row -->
                        </div>
                    </div>
                </div>
                <hr>
            </div>

            <div class="card-footer">
                <div class="text-right">
                    @if (empty($data_user))
                        <button class="btn btn-success" onclick="RegistrasiSekolah.approve()">Approve</button>
                    @endif
                    <button class="btn btn-secondary" onclick="RegistrasiSekolah.cancel()">Kembali</button>
                </div>
            </div><!-- form-layout-footer -->
        </div>
    </div>
</div>
<br>
