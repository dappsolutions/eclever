<div class="card bd-primary mg-t-20">
    <div class="card-header bg-primary tx-white">
        Histori
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Jurusan</th>
                        <th>Kelas</th>
                    </tr>
                </thead>
                <tbody>
                    @if (!empty($data))
                        @foreach ($data as $item)
                            @php
                                $change_bg = $item->deleted == '0' ? 'bg-success' : ''
                            @endphp
                            <tr class="{{ $change_bg }}">
                                <td>{{ $item->nama_jurusan }}</td>
                                <td>{{ $item->nama_kelas }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
