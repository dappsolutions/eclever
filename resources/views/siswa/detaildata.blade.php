<input type="hidden" id='id' value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">{{ 'DETAIL '.strtoupper($module) }}</div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-layout">
                            <div class="row mg-b-25">
                                @php
                                    $hide = session('access') == 'superadmin' ? '' : 'hide';
                                @endphp
                                <div class="col-lg-4 {{ $hide }}">
                                    <div class="form-group mg-b-10-force">
                                        <label class="form-control-label">Sekolah : <span class="tx-danger">*</span></label>
                                        <select id='sekolah' class="form-control select2  required" error='Mata Pelajaran' data-placeholder="Choose country" tabindex="-1" aria-hidden="true">
                                            @if (!empty($data_sekolah))
                                                <option label="Pilih Sekolah"></option>
                                                    @foreach ($data_sekolah as $item)
                                                        @php
                                                            $selected = isset($sekolah) ? $item->id == $sekolah ? 'selected' : '' : '';
                                                        @endphp
                                                        <option {{ $selected }} value="{{ $item->id }}">{{ $item->nama_sekolah }}</option>
                                                    @endforeach
                                            @else
                                                <option value=""></option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">Nama : <span class="tx-danger">*</span></label>
                                        <input readonly id='nama' class="form-control required" error="Nama" value="{{ isset($nama) ? $nama : '' }}" type="text" placeholder="Nama">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">Nis : <span class="tx-danger">*</span></label>
                                        <input id='nis' readonly value="{{ isset($nis) ? $nis : '' }}" class="form-control required" error='NIS' type="text" placeholder="NIS">
                                    </div>
                                </div><!-- col-4 -->

                            </div><!-- row -->
                        </div>
                    </div>
                </div>
                <hr>

                <div class="content-action" style="border: 1px solid #ccc;padding: 16px !important;">
                    <div class="row">
                        <div class="col-md-6">
                            <u><i><h6 class="tx-inverse mg-b-0">Siswa Jurusan dan Kelas</h6></i></u>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="" onclick="Siswa.historyKelas(this, event)" class="btn btn-outline-warning btn-icon rounded-circle"><div><i class="fa fa-file-text"></i></div></a>
                            <a href="" onclick="Siswa.editKelas(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-pencil"></i></div></a>
                            {{--  <i class="fa fa-pencil hover" data-toggle="tooltip" data-placement="top" title="Ubah"></i>  --}}
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Kelas : <span class="tx-danger">*</span></label>
                                <select disabled id='kelas' error='Kelas' class="form-control required action select2 " tabindex="-1" aria-hidden="true">
                                    @if (!empty($data_kelas))
                                        <option label="Pilih Kelas"></option>
                                            @foreach ($data_kelas as $item)
                                                @php
                                                    $selected = isset($kelas_id) ? $item->id == $kelas_id ? 'selected' : '' : '';
                                                @endphp
                                                <option {{ $selected }} value="{{ $item->id }}">{{ $item->nama_kelas.' - ['.$item->nama_jurusan.']' }}</option>
                                            @endforeach
                                    @else
                                        <option value=""></option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="btn-action hide">
                                <a href="" onclick="Siswa.changeKelas(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-check"></i></div></a>
                                <a href="" onclick="Siswa.cancelKelas(this, event)" class="btn btn-outline-danger btn-icon rounded-circle mg-r-5"><div><i class="fa fa-close"></i></div></a>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

                <div class="content-action-angkatan" style="border: 1px solid #ccc;padding: 16px !important;">
                    <div class="row">
                        <div class="col-md-6">
                            <u><i><h6 class="tx-inverse mg-b-0">Siswa Angkatan</h6></i></u>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="" onclick="Siswa.historyAngkatan(this, event)" class="btn btn-outline-warning btn-icon rounded-circle"><div><i class="fa fa-file-text"></i></div></a>
                            <a href="" onclick="Siswa.editAngkatan(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-pencil"></i></div></a>
                            {{--  <i class="fa fa-pencil hover" data-toggle="tooltip" data-placement="top" title="Ubah"></i>  --}}
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Angkatan : <span class="tx-danger">*</span></label>
                                <select disabled id='angkatan' error='Angkatan' class="form-control required action select2 " tabindex="-1" aria-hidden="true">
                                    @if (!empty($data_angkatan))
                                        <option label="Pilih Angkatan"></option>
                                            @foreach ($data_angkatan as $item)
                                                @php
                                                    $selected = isset($angkatan_id) ? $item['id'] == $angkatan_id ? 'selected' : '' : '';
                                                @endphp
                                                <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['nama_angkatan'] }}</option>
                                            @endforeach
                                    @else
                                        <option value=""></option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="btn-action hide">
                                <a href="" onclick="Siswa.changeAngkatan(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-check"></i></div></a>
                                <a href="" onclick="Siswa.cancelAngkatan(this, event)" class="btn btn-outline-danger btn-icon rounded-circle mg-r-5"><div><i class="fa fa-close"></i></div></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="card-footer">
                <div class="text-right">
                    <button class="btn btn-secondary" onclick="Siswa.cancel()">Kembali</button>
                </div>
            </div><!-- form-layout-footer -->
        </div>
    </div>
</div>
<br>
