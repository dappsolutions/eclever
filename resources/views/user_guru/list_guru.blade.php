<label class="form-control-label">Guru : <span class="tx-danger">*</span></label>
<select id='guru' class="form-control select2-show-search required" error='Guru' data-placeholder="Pilih Guru" tabindex="-1" aria-hidden="true">
    @if (!empty($data_guru))
        <option label="Pilih Guru"></option>
            @foreach ($data_guru as $item)
                @php
                    $selected = isset($guru) ? $item['id'] == $guru ? 'selected' : '' : '';
                @endphp
                <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['nama'] }}</option>
            @endforeach
    @else
        <option value=""></option>
    @endif
</select>
