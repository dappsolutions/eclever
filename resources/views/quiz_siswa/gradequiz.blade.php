<div class="card pd-20 pd-sm-30 bd-primary">
    <h5 class="tx-inverse mg-b-20">Congratulations!! Hasil Quiz anda adalah : {{ $nilai }}</h5>
    <p>Tingkatkan belajar anda, supaya dapat nilai yang selau baik</p>
    <p>
        <div class="col-md-4">
            <table>
                <tr>
                    <td>Benar</td>
                    <td>:</td>
                    <td><b>{{ $benar }}</b></td>
                </tr>
                <tr>
                    <td>Salah </td>
                    <td>:</td>
                    <td><b>{{ $salah }}</b></td>
                </tr>
                <tr>
                    <td>Total Soal </td>
                    <td>:</td>
                    <td><b>{{ $total }}</b></td>
                </tr>
            </table>
        </div>
    </p>
    <p class="mg-b-0"><i>SMK Negeri 2 Blitar Selau Jaya</i></p>

    <hr>

    <div class="media align-items-center">
    @php
        $image_profile = $data['foto'] == '' ? asset('assets/img/no_picture.jpg') : $data['foto'];
    @endphp
      <img src="{{ $image_profile }}" class="wd-40 rounded-circle" alt="">
      <div class="media-body mg-l-15">
        <h6 class="tx-inverse tx-14 mg-b-5">{{ ucfirst($data['nama']) }}</h6>
        <p class="tx-12 mg-b-0"> {{ date('M d, Y H:i:s') }}</p>
      </div><!-- media-body -->

      <div class="text-right">
          <button class="btn" onclick="QuizSiswa.cancel()">Kembali</button>
      </div>
    </div><!-- media -->
</div>
