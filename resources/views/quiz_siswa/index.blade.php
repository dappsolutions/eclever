
<div class='row'>
    <div class="col-md-3">
        &nbsp;
    </div>
    <div class="col-md-9">
        <div class="input-group">
            <input type="search" class="form-control" placeholder="Search" onkeyup="QuizSiswa.cari(this, event)">
            <span class="input-group-btn">
                <button class="btn"><i class="fa fa-search"></i></button>
            </span><!-- input-group-btn -->
        </div>
    </div>
</div>


@if (isset($keyword))
    <div class="row">
        <div class="col-md-12">
            Hasil Pencarian : <b>{{ $keyword }}</b>
        </div>
    </div>
    <br>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">{{ 'DAFTAR '.strtoupper($module) }}</div>
            <div class="card-body">
                <div class='table-responsive'>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Ujian</th>
                                <th>Token Ujian</th>
                                <th>Judul</th>
                                <th>Kategori Ujian</th>
                                <th>Tanggal Ujian</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($data))
                                @php
                                    $no = ($data->currentpage()-1)* $data->perpage();
                                    $no +=1;
                                @endphp

                                @foreach ($data as $value)
                                    <tr id='{{ $value->id }}'>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $value->kode_ujian }}</td>
                                        <td>{{ $value->token }}</td>
                                        <td>{{ $value->judul }}</td>
                                        <td class="{{ $value->kat_ujian == 'TERBUKA' ? 'text-success' : 'text-warning' }}"><i>{{ $value->kat_ujian }}</i></td>
                                        <td><b>{{ $value->tanggal_ujian == '' ? '' : date('d F Y', strtotime($value->tanggal_ujian)).' s/d '.date('d F Y', strtotime($value->tanggal_akhir)) }}</b></td>
                                        <td class='text-center'>
                                            <a href=""
                                            shuffle_question='{{ $value->shuffle_question }}'
                                            shuffle_answer='{{ $value->shuffle_answer }}'
                                            question_per_page='{{ $value->question_per_page }}'
                                            remidial='{{ $value->remidial }}'
                                            grading_method='{{ $value->grading_method }}'
                                            siswa='{{ $value->siswa }}'
                                            ujian='{{ $value->ujian }}'
                                            onclick="QuizSiswa.kerjakan(this, event)"
                                            class="btn btn-outline-warning btn-icon rounded-circle mg-r-5">
                                                <div><i class="fa fa-pencil"></i></div>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">Tidak ada data ditemukan</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="card-footer">
                <div class="text-right">
                    @php
                        $no_hal = ($data->currentpage()-1)* $data->perpage();
                    @endphp
                    <span>Menampilkan data {{ $no_hal >= 0 ? $data->total() > 0 ? $no_hal+ 1 : 0 : 0 }} - {{ $no-1 }} dari <b>{{ $data->total() }}</b> data.</span>
                </div>
            </div>
        </div>
    </div>
</div>
<br>

<div class="row ">
    <div class="col-md-12">
        <div class="text-right">
            {{ $data->links() }}
        </div>
    </div>
</div>
