<input type="hidden" id='total_soal' value="{{ count($data_soal) }}">
<input type="hidden" id='siswa_has_ujian' value="{{ $siswa_has_ujian }}">

<div class="row">
    <div class="col-md-8">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">Form Quiz Siswa</div>
            <div class="card-body">
                <div id="form-quiz">
                    @php
                        $counter = 0;
                    @endphp
                    @for ($i = 0; $i < count($data_soal); $i++)
                        @if ($i % $question_per_page == 0)
                            <h3>&nbsp;</h3>
                            <section>
                                <div class="media-list">
                                    @php
                                        $batas = $i+$question_per_page;
                                        $batas = $batas > count($data_soal) ? $batas - 1 : $batas;
                                    @endphp
                                    @for($j = $i; $j < $batas; $j++)
                                        <div class="media">
                                            @php
                                                $number = $j+1;
                                            @endphp
                                            <span class="number-soal hover-number">{{ $number }}</span>
                                            <div class="media-body mg-l-20">
                                                @foreach ($data_soal[$j] as $key => $item)
                                                    <h6 class="tx-15 mg-b-5" style="margin-top: 12px;">
                                                        <b soal_id="{{ $key }}" ujian_soal_id='{{ $item['ujian_soal_id'] }}' siswa_answer_id="{{ $item['siswa_answer'] }}">{!! $item['soal'] !!}</b>
                                                    </h6>
                                                    @php
                                                        $answer_siswa = $item['answer_id'];
                                                    @endphp
                                                    <p>
                                                        @if (!empty($item['jawaban']))
                                                            <div class="row">
                                                                @foreach ($item['jawaban'] as $item_ans)
                                                                    <div class="col-md-12">
                                                                        <div class="table-responsive">
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label class="rdiobox hover">
                                                                                            @php
                                                                                                $checked = $answer_siswa == $item_ans['jawaban_id'] ? 'checked' : '';
                                                                                            @endphp
                                                                                            <input onchange="QuizSiswa.answer(this)" name="rdio_{{ $number }}" {{ $checked }} data_id="{{ $item_ans['jawaban_id'] }}"
                                                                                            class="radio_answer hover" type="radio">
                                                                                            <span></span>
                                                                                        </label>
                                                                                    </td>
                                                                                    <td style="padding-top: 3px;">{!! $item_ans['jawaban'] !!}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </p>
                                                @endforeach
                                            </div>
                                        </div>

                                        <hr class="mg-y-20">
                                    @endfor
                                </div>
                            </section>
                        @endif
                    @endfor
                </div>
                <br>
            </div><!-- card-body -->
            <div class="card-footer">
                <div class="text-right">
                    {{-- <a href="" class="btn btn-success pd-sm-x-20 mg-sm-r-5">Submit</a> --}}
                    {{-- <a href="" class="btn btn-secondary pd-sm-x-20">Kembali</a> --}}
                </div>
            </div>
          </div>
    </div>

    <div class="col-md-4">
        <div class="row">
            <div class="col-md-12">
                <div class="card bd-primary mg-t-20">
                    <div class="card-header bg-primary tx-white">Waktu Quiz</div>
                    <div class="card-body">
                        <div id="flotRealtime"></div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    Soal Dikerjakan
                                </div>
                                <br>
                                <div class="row">
                                    @if (!empty($data_soal))
                                        @for ($i = 0; $i < count($data_soal); $i++)
                                            <div class="col-md-1">
                                                @php
                                                    $no = $i+1;
                                                @endphp
                                                <span class="number no_soal_{{ $no }}">{{ $no }}</span>
                                            </div>
                                        @endfor
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div><!-- card-body -->
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    .number{
        font-size: 14px;
        display: flex;
        justify-content: center;
        align-items: center;
        width: 25px;
        height: 25px;
        border: 1px solid #adb5bd;
        border-radius: 50px;
    }
    .number-soal{
        font-size: 14px;
        display: flex;
        justify-content: center;
        align-items: center;
        width: 45px;
        height: 45px;
        border: 1px solid #adb5bd;
        border-radius: 50px;
    }

    .hover-number:hover{
        background-color: #0866C6;
        cursor: pointer;
        color:white;
    }

    .answered{
        background-color: #23BF08;
        color:white;
    }

    .hover:hover{
        cursor: pointer;
    }
</style>


<script>
    $(function(){
        QuizSiswa.setFormQizard();
        QuizSiswa.soalAnswered();
        QuizSiswa.finish();
    });
</script>
