<input type="hidden" id='id' value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">{{ 'DETAIL '.strtoupper($module) }}</div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-layout">
                            <div class="row mg-b-25">
                                @php
                                    $hide = session('access') == 'superadmin' ? '' : 'hide';
                                @endphp
                                <div class="col-lg-4 {{ $hide }}">
                                    <div class="form-group mg-b-10-force">
                                        <label class="form-control-label">Sekolah : <span class="tx-danger">*</span></label>
                                        <select readonly id='sekolah' class="form-control select2  required" error='Mata Pelajaran' data-placeholder="Choose country" tabindex="-1" aria-hidden="true">
                                            @if (!empty($data_sekolah))
                                                <option label="Pilih Sekolah"></option>
                                                    @foreach ($data_sekolah as $item)
                                                        @php
                                                            $selected = isset($sekolah) ? $item->id == $sekolah ? 'selected' : '' : '';
                                                        @endphp
                                                        <option {{ $selected }} value="{{ $item->id }}">{{ $item->nama_sekolah }}</option>
                                                    @endforeach
                                            @else
                                                <option value=""></option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group mg-b-10-force">
                                        <label class="form-control-label">Mata Pelajaran : <span class="tx-danger">*</span></label>
                                        <select readonly id='mapel' class="form-control select2  required" error='Mata Pelajaran' data-placeholder="Choose country" tabindex="-1" aria-hidden="true">
                                            @if (!empty($data_mapel))
                                                <option label="Pilih Mata Pelajaran"></option>
                                                    @foreach ($data_mapel as $item)
                                                        @php
                                                            $selected = isset($mata_pelajaran) ? $item['id'] == $mata_pelajaran ? 'selected' : '' : '';
                                                        @endphp
                                                        <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['mata_pelajaran'] }}</option>
                                                    @endforeach
                                            @else
                                                <option value=""></option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">Kategori : <span class="tx-danger">*</span></label>
                                        <input readonly id='kategori' class="form-control required" error="Kategori" value="{{ isset($kategori) ? $kategori : '' }}" type="text" placeholder="Kategori">
                                    </div>
                                </div><!-- col-4 -->

                            </div><!-- row -->
                        </div>
                    </div>
                </div>
                <hr>
            </div>

            <div class="card-footer">
                <div class="text-right">
                    <button class="btn btn-secondary" onclick="KategoriSoal.cancel()">Kembali</button>
                </div>
            </div><!-- form-layout-footer -->
        </div>
    </div>
</div>
<br>
