<input type="hidden" id='id' value="{{ isset($id) ? $id : '' }}">
<input type="hidden" id='disabled' value="{{ isset($disabled) ? $disabled : '' }}">

<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">{{ 'TAMBAH '.strtoupper($module) }}</div>

            <div class="card-body">
                <div id="form-jam">
                    <h3>Jam</h3>
                    <section>
                        <div class="form-layout">
                            <div class="row mg-b-25">
                                @php
                                    $hide = session('access') == 'superadmin' ? '' : 'hide';
                                @endphp
                                <div class="col-lg-4 {{ $hide }}">
                                    <div class="form-group mg-b-10-force">
                                        <label class="form-control-label">Sekolah : <span class="tx-danger">*</span></label>
                                        <select {{ isset($disabled) ? 'disabled' : '' }} id='sekolah' class="form-control select2  required" error='Sekolah' data-placeholder="Choose country" tabindex="-1" aria-hidden="true">
                                            @if (!empty($data_sekolah))
                                                <option label="Pilih Sekolah"></option>
                                                    @foreach ($data_sekolah as $item)
                                                        @php
                                                            $selected = isset($sekolah) ? $item->id == $sekolah ? 'selected' : '' : '';
                                                        @endphp
                                                        <option {{ $selected }} value="{{ $item->id }}">{{ $item->nama_sekolah }}</option>
                                                    @endforeach
                                            @else
                                                <option value=""></option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">Judul : <span class="tx-danger">*</span></label>
                                        <input {{ isset($disabled) ? 'disabled' : '' }} id='judul' class="form-control required text-right" error="Judul" value="{{ isset($judul) ? $judul : '' }}" type="text" placeholder="Judul">
                                    </div>
                                </div><!-- col-4 -->

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label">Jam Pelajaran (Menit) : <span class="tx-danger">*</span></label>
                                        <input {{ isset($disabled) ? 'disabled' : '' }} id='lama' class="form-control required text-right" error="Jam Pelajaran" value="{{ isset($lama) ? $lama : '' }}" type="number" placeholder="Jam Pelajaran">
                                    </div>
                                </div><!-- col-4 -->
                            </div><!-- row -->

                            @if (!isset($disabled))
                                <div class="text-right">
                                    <a href="" onclick="JamPelajaran.submit(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-check"></i></div></a>
                                </div>
                            @endif
                        </div>
                    </section>

                    <h3>Generate Jam</h3>
                    <section>
                        <div class="form-layout">
                            <div class="row mg-b-25">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="bootstrap-timepicker">
                                            <div class="form-group">
                                              <label>Jam :</label>

                                              <div class="input-group">
                                                <input {{ isset($disabled) ? 'disabled' : '' }} id="jam" type="text" class="form-control timepicker" value="{{ isset($jam_set) ? $jam_set : '' }}">

                                                <div class="input-group-addon">
                                                  <i class="fa fa-clock-o"></i>
                                                </div>
                                              </div>

                                              <div style="margin-top:8px;">
                                                <input {{ isset($disabled) ? 'disabled' : '' }} id="jumlah_jam" type="number" class="form-control" value="{{ isset($jumlah_jam) ? $jumlah_jam : '' }}" placeholder="Jumlah Jam Pelajaran">
                                              </div>

                                              @if (!isset($disabled))
                                                <div style="margin-top:12px;">
                                                    <button class="btn btn-block btn-success" onclick="JamPelajaran.generateJam(this)">Generate</button>
                                                </div>
                                              @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-8">
                                    <label>Hasil Generate Jam Mata Pelajaran : <span class="text-primary">*</span></label>
                                    <div style="border: 1px solid #ccc;padding: 16px !important;">
                                        <div class="table-responsive" id="data-generate-jam">
                                            @if (isset($list_genarate_jam))
                                                {!! $list_genarate_jam !!}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>

                            @if (!isset($disabled))
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="text-right">
                                            <a href="" onclick="JamPelajaran.submitKlasifikasi(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-check"></i></div></a>
                                        </div>
                                    </div>
                                </div>
                            @endif


                        </div>
                    </section>

                    <h3>Setting Hari</h3>
                    <section>
                        <div class="form-layout">
                            @if (isset($data_day))
                                <div class="row">
                                    @foreach ($data_day as $item)
                                    <div class="col-lg-6">
                                        <label class="ckbox">
                                            <input {{ isset($disabled) ? 'disabled' : '' }} {{ $item['choose'] == '1' ? 'checked' : '' }} type="checkbox" day='{{ $item['id'] }}' data_id="{{ $item['mapel_day_id'] }}" onchange="JamPelajaran.setMapelDay(this)"><span>{{ $item['hari'] }}</span>
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </section>
                </div>
            </div>

        </div>
    </div>
</div>
<br>


<script>
    $(function(){
        JamPelajaran.setFormWizard();
        JamPelajaran.setTimePicker();
        JamPelajaran.finish();
    });
</script>

