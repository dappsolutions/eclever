<table id="tb-list-generate-jam">
    @php
        $no = 1;
    @endphp
    @foreach ($data as $item)
        <tr class="tr_input" data_id="{{ isset($item['id']) ? $item['id'] : '' }}">
            <td class="text-center"><label for="">{{ $no++ }}&nbsp;&nbsp;&nbsp;&nbsp;</label></td>
            <td>
                <div class="input-group">
                    <input type="text" id="jam_awal" value="{{ $item['jam_awal'] }}" readonly class="form-control">
                    <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                    </div>
                </div>
            </td>
            <td>
                -
            </td>
            <td>
                <div class="input-group">
                    <input type="text" id="jam_akhir" value="{{ $item['jam_akhir'] }}" readonly class="form-control">
                    <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                    </div>
                </div>
            </td>
        </tr>
    <tr>
        <td colspan="4">&nbsp;</td>
    </tr>
    @endforeach
</table>
