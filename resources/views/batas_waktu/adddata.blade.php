<input type="hidden" id='id' value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">{{ 'TAMBAH '.strtoupper($module) }}</div>

            <div class="card-body">
                <div class="form-layout">
                    <div class="row mg-b-25">
                        @php
                            $hide = session('access') == 'superadmin' ? '' : 'hide';
                        @endphp
                        <div class="col-lg-4 {{ $hide }}">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Sekolah : <span class="tx-danger">*</span></label>
                                <select id='sekolah' class="form-control select2  required" error='Mata Pelajaran' data-placeholder="Choose country" tabindex="-1" aria-hidden="true">
                                    @if (!empty($data_sekolah))
                                        <option label="Pilih Sekolah"></option>
                                            @foreach ($data_sekolah as $item)
                                                @php
                                                    $selected = isset($sekolah) ? $item->id == $sekolah ? 'selected' : '' : '';
                                                @endphp
                                                <option {{ $selected }} value="{{ $item->id }}">{{ $item->nama_sekolah }}</option>
                                            @endforeach
                                    @else
                                        <option value=""></option>
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Batas Waktu : <span class="tx-danger">*</span></label>
                                <input id='time_limit' class="form-control required text-right" error="Batas Waktu" value="{{ isset($time_limit) ? $time_limit : '' }}" type="number" placeholder="Batas Waktu">
                            </div>
                        </div><!-- col-4 -->

                    </div><!-- row -->

                    <div class="form-layout-footer">
                        <div class="text-right">
                            <button class="btn btn-success mg-r-5" onclick="BatasWaktu.submit()">Submit Form</button>
                            <button class="btn btn-secondary" onclick="BatasWaktu.cancel()">Batal</button>
                        </div>
                    </div><!-- form-layout-footer -->
                </div>
            </div>

        </div>
    </div>
</div>
<br>

<div class="row">
    <div class="col-md-12">

    </div>
</div>
