<input type="hidden" id='id' value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">{{ 'TAMBAH '.strtoupper($module) }}</div>

            <div class="card-body">
                <div class="form-layout">
                    <div class="row mg-b-25">
                        @php
                            $hide = session('access') == 'superadmin' ? '' : 'hide';
                        @endphp
                        <div class="col-lg-4 {{ $hide }}">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Sekolah : <span class="tx-danger">*</span></label>
                                <select id='sekolah' class="form-control select2  required" error='Sekolah' data-placeholder="Choose country" tabindex="-1" aria-hidden="true">
                                    @if (!empty($data_sekolah))
                                        <option label="Pilih Sekolah"></option>
                                            @foreach ($data_sekolah as $item)
                                                @php
                                                    $selected = isset($sekolah) ? $item->id == $sekolah ? 'selected' : '' : '';
                                                @endphp
                                                <option {{ $selected }} value="{{ $item->id }}">{{ $item->nama_sekolah }}</option>
                                            @endforeach
                                    @else
                                        <option value=""></option>
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Mata Pelajaran : <span class="tx-danger">*</span></label>
                                <select onchange="Soal.getListKategoriSoal(this)" id='mapel' class="form-control select2  required" error='Mata Pelajaran' data-placeholder="Choose country" tabindex="-1" aria-hidden="true">
                                    @if (!empty($data_mapel))
                                        <option label="Pilih Mata Pelajaran"></option>
                                            @foreach ($data_mapel as $item)
                                                @php
                                                    $selected = isset($mata_pelajaran) ? $item['id'] == $mata_pelajaran ? 'selected' : '' : '';
                                                @endphp
                                                <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['mata_pelajaran'] }}</option>
                                            @endforeach
                                    @else
                                        <option value=""></option>
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force" id="list_kategori">
                                <label class="form-control-label">Kategori Soal : <span class="tx-danger">*</span></label>
                                <select id='kategori_soal' class="form-control select2  required" error='Kategori Soal' data-placeholder="Choose country" tabindex="-1" aria-hidden="true">
                                    @if (!empty($data_kategori))
                                        <option label="Pilih Kategori Soal"></option>
                                            @foreach ($data_kategori as $item)
                                                @php
                                                    $selected = isset($kategori_soal) ? $item['id'] == $kategori_soal ? 'selected' : '' : '';
                                                @endphp
                                                <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['kategori'] }}</option>
                                            @endforeach
                                    @else
                                        <option value="">Pilih Kategori Soal</option>
                                    @endif
                                </select>
                            </div>
                        </div>

                    </div><!-- row -->

                    <div class="row mg-b-25">
                        <div class="col-md-12">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Soal : <span class="tx-danger">*</span></label>
                                <textarea class="required form-control" id="soal" error="Soal"><?php echo isset($soal) ? $soal : '' ?></textarea>
                            </div>
                        </div>
                    </div>

                    {{--  <div class="row mg-b-25">
                        <div class="col-md-12">
                            <div class="content-action" style="border: 1px solid #ccc;padding: 16px !important;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <u><i><h6 class="tx-inverse mg-b-0">Penilaian</h6></i></u>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="form-control-label">Dasar Penilaian : <span class="tx-danger">*</span></label>
                                        <select name="" onchange="Soal.setDasarPenliaian(this)" class="form-control required" error="Penilaian" id="penilaian">
                                            <option value="1">SOAL</option>
                                            <option value="2">JAWABAN</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  --}}

                    <div class="row mg-b-25">
                        <div class="col-md-12">
                            <div class="content-action-jawaban" style="border: 1px solid #ccc;padding: 16px !important;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <u><i><h6 class="tx-inverse mg-b-0">Jawaban</h6></i></u>
                                    </div>
                                </div>
                                <br>

                                {!! $table_jawaban !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-layout-footer">
                        <div class="text-right">
                            <button class="btn btn-success mg-r-5" onclick="Soal.submit()">Submit Form</button>
                            <button class="btn btn-secondary" onclick="Soal.cancel()">Batal</button>
                        </div>
                    </div><!-- form-layout-footer -->
                </div>
            </div>

        </div>
    </div>
</div>
<br>

<div class="row">
    <div class="col-md-12">

    </div>
</div>
