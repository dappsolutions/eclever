<div class="row">
 <div class="col-md-12">
  <label class="form-control-label">Membuat Jawaban : <span class="tx-danger">*</span></label>
  <div class="table-responsive">
   <table class='table' id='tb_jawaban'>
    <thead>
     <tr>
      <th>Jawaban</th>
      <th class="text-center ">Benar</th>
      @if (isset($detail))
      @else
        <th class="text-center">Action</th>
      @endif
     </tr>
    </thead>
    <tbody>
        @if (isset($data_jawaban))
            @if (!empty($data_jawaban))
                @php
                    $no = 1;
                    $disabled = isset($detail) ? 'disabled' : '';
                @endphp
                @foreach ($data_jawaban as $key => $value)
                <tr>
                    <td>
                        <textarea disabled {{ $disabled }} jawaban_id='<?php echo $value['id'] ?>' class="form-control focused required jawaban" error="Jawaban"
                                name=""><?php echo $value['jawaban'] ?></textarea>
                    </td>
                    <td class="text-center">
                        <label>
                        <input {{ $disabled }} name="benar" type="radio" id='rd_benar' <?php echo $value['true_or_false'] == '1' ? 'checked' : '' ?>/>
                        <span>Benar</span>
                        </label>
                    </td>
                    @if (!isset($detail))
                        <td class="text-center">
                            @if (isset($detail))
                                &nbsp;
                            @else
                                @if ($no == 1)
                                    <a href="" onclick="Soal.addJawaban(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-plus"></i></div></a>
                                    <a href="" data_id='{{ $value['id'] }}' onclick="Soal.deleteJawaban(this, event)" class="btn btn-outline-danger btn-icon rounded-circle"><div><i class="fa fa-trash"></i></div></a>
                                @else
                                    <a href="" data_id='{{ $value['id'] }}' onclick="Soal.deleteJawaban(this, event)" class="btn btn-outline-danger btn-icon rounded-circle"><div><i class="fa fa-trash"></i></div></a>
                                @endif
                            @endif
                        @php
                            $no++
                        @endphp
                        </td>
                    @endif
                </tr>
                @endforeach
            @else
                <tr>
                    <td>
                        <textarea jawaban_id='0' class="form-control focused required jawaban" error="Jawaban"
                                name=""><?php echo isset($membuat_soal) ? $membuat_soal : '' ?></textarea>
                    </td>
                    <td class="text-center">
                        <label>
                        <input name="benar" type="radio" id='rd_benar'/>
                        <span>Benar</span>
                        </label>
                    </td>
                    <td class="text-center">
                        <a href="" onclick="Soal.addJawaban(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-plus"></i></div></a>
                    </td>
               </tr>
            @endif
        @else
            <tr>
                <td>
                    <textarea jawaban_id='0' class="form-control focused required jawaban" error="Jawaban"
                            name=""><?php echo isset($membuat_soal) ? $membuat_soal : '' ?></textarea>
                </td>
                <td class="text-center">
                    <label>
                    <input name="benar" type="radio" id='rd_benar'/>
                    <span>Benar</span>
                    </label>
                </td>
                <td class="text-center">
                    <a href="" onclick="Soal.addJawaban(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-plus"></i></div></a>
                </td>
           </tr>
        @endif
    </tbody>
   </table>
  </div>
 </div>
</div>
