<label class="form-control-label">Kategori Soal : <span class="tx-danger">*</span></label>
<select id='kategori_soal' class="form-control required" error='Kategori Soal' data-placeholder="Choose country" tabindex="-1" aria-hidden="true">
    @if (!empty($data))
        <option label="Pilih Kategori Soal"></option>
            @foreach ($data as $item)
                @php
                    $selected = isset($kategori_soal) ? $item['id'] == $kategori_soal ? 'selected' : '' : '';
                @endphp
                <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['kategori'] }}</option>
            @endforeach
    @else
        <option value="">Pilih Kategori Soal</option>
    @endif
</select>
