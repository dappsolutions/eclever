<?php

use Illuminate\View\View;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//root
Route::get('/', function () {
    return view("login.login");
});
//root

//guru
Route::get('master/guru', 'Master\GuruController@index')->middleware('session');
Route::get('master/guru/add', 'Master\GuruController@add')->middleware('session');
Route::get('master/guru/edit/{id}', 'Master\GuruController@edit')->middleware('session');
Route::get('master/guru/cari', 'Master\GuruController@cari')->middleware('session');
Route::get('master/guru/detail/{id}', 'Master\GuruController@detail')->middleware('session');

//ajax guru
Route::post('master/guru/submit', 'Master\GuruController@submit');
Route::post('master/guru/delete', 'Master\GuruController@delete');
Route::post('master/guru/changeMapel', 'Master\GuruController@changeMapel');
Route::post('master/guru/showDetailUser', 'Master\GuruController@showDetailUser');

//mata pelajaran
Route::get('master/mapel', 'Master\MataPelajaranController@index')->middleware('session');
Route::get('master/mapel/cari', 'Master\MataPelajaranController@cari')->middleware('session');
Route::get('master/mapel/detail/{id}', 'Master\MataPelajaranController@detail')->middleware('session');
Route::get('master/mapel/add', 'Master\MataPelajaranController@add')->middleware('session');
Route::get('master/mapel/edit/{id}', 'Master\MataPelajaranController@edit')->middleware('session');

//ajax mata pelajaran
Route::post('master/mapel/submit', 'Master\MataPelajaranController@submit');
Route::post('master/mapel/delete', 'Master\MataPelajaranController@delete');


//siswa
Route::get('master/siswa', 'Master\SiswaController@index')->middleware('session');
Route::get('master/siswa/cari', 'Master\SiswaController@cari')->middleware('session');
Route::get('master/siswa/detail/{id}', 'Master\SiswaController@detail')->middleware('session');
Route::get('master/siswa/add', 'Master\SiswaController@add')->middleware('session');
Route::get('master/siswa/edit/{id}', 'Master\SiswaController@edit')->middleware('session');

//ajax siswa
Route::post('master/siswa/submit', 'Master\SiswaController@submit');
Route::post('master/siswa/delete', 'Master\SiswaController@delete');
Route::post('master/siswa/changeKelas', 'Master\SiswaController@changeKelas');
Route::post('master/siswa/changeAngkatan', 'Master\SiswaController@changeAngkatan');
Route::post('master/siswa/historyKelas', 'Master\SiswaController@historyKelas');
Route::post('master/siswa/historyAngkatan', 'Master\SiswaController@historyAngkatan');
Route::post('master/siswa/showDetailUser', 'Master\SiswaController@showDetailUser');

//kelas
Route::get('master/kelas', 'Master\KelasController@index')->middleware('session');
Route::get('master/kelas/cari', 'Master\KelasController@cari')->middleware('session');
Route::get('master/kelas/detail/{id}', 'Master\KelasController@detail')->middleware('session');
Route::get('master/kelas/add', 'Master\KelasController@add')->middleware('session');
Route::get('master/kelas/edit/{id}', 'Master\KelasController@edit')->middleware('session');

//ajax kelas
Route::post('master/kelas/submit', 'Master\KelasController@submit');
Route::post('master/kelas/delete', 'Master\KelasController@delete');

//jurusan
Route::get('master/jurusan', 'Master\JurusanController@index')->middleware('session');
Route::get('master/jurusan/cari', 'Master\JurusanController@cari')->middleware('session');
Route::get('master/jurusan/detail/{id}', 'Master\JurusanController@detail')->middleware('session');
Route::get('master/jurusan/add', 'Master\JurusanController@add')->middleware('session');
Route::get('master/jurusan/edit/{id}', 'Master\JurusanController@edit')->middleware('session');

//ajax jurusan
Route::post('master/jurusan/submit', 'Master\JurusanController@submit');
Route::post('master/jurusan/delete', 'Master\JurusanController@delete');

//sekolah
Route::get('master/sekolah', 'Master\SekolahController@index')->middleware('session');
Route::get('master/sekolah/cari', 'Master\SekolahController@cari')->middleware('session');
Route::get('master/sekolah/detail/{id}', 'Master\SekolahController@detail')->middleware('session');
Route::get('master/sekolah/add', 'Master\SekolahController@add')->middleware('session');
Route::get('master/sekolah/edit/{id}', 'Master\SekolahController@edit')->middleware('session');

//ajax sekolah
Route::post('master/sekolah/submit', 'Master\SekolahController@submit');
Route::post('master/sekolah/delete', 'Master\SekolahController@delete');


//angkatan
Route::get('master/angkatan', 'Master\AngkatanController@index')->middleware('session');
Route::get('master/angkatan/cari', 'Master\AngkatanController@cari')->middleware('session');
Route::get('master/angkatan/detail/{id}', 'Master\AngkatanController@detail')->middleware('session');
Route::get('master/angkatan/add', 'Master\AngkatanController@add')->middleware('session');
Route::get('master/angkatan/edit/{id}', 'Master\AngkatanController@edit')->middleware('session');

//ajax angkatan
Route::post('master/angkatan/submit', 'Master\AngkatanController@submit');
Route::post('master/angkatan/delete', 'Master\AngkatanController@delete');


//MENTORING

//kategori soal
Route::get('mentoring/kategori_soal', 'Mentoring\KategoriSoalController@index')->middleware('session');
Route::get('mentoring/kategori_soal/cari', 'Mentoring\KategoriSoalController@cari')->middleware('session');
Route::get('mentoring/kategori_soal/detail/{id}', 'Mentoring\KategoriSoalController@detail')->middleware('session');
Route::get('mentoring/kategori_soal/add', 'Mentoring\KategoriSoalController@add')->middleware('session');
Route::get('mentoring/kategori_soal/edit/{id}', 'Mentoring\KategoriSoalController@edit')->middleware('session');

//ajax kategori soal
Route::post('mentoring/kategori_soal/submit', 'Mentoring\KategoriSoalController@submit');
Route::post('mentoring/kategori_soal/delete', 'Mentoring\KategoriSoalController@delete');

//soal
Route::get('mentoring/bank_soal', 'Mentoring\SoalController@index')->middleware('session');
Route::get('mentoring/bank_soal/cari', 'Mentoring\SoalController@cari')->middleware('session');
Route::get('mentoring/bank_soal/detail/{id}', 'Mentoring\SoalController@detail')->middleware('session');
Route::get('mentoring/bank_soal/add', 'Mentoring\SoalController@add')->middleware('session');
Route::get('mentoring/bank_soal/edit/{id}', 'Mentoring\SoalController@edit')->middleware('session');

//ajax soal
Route::post('mentoring/bank_soal/submit', 'Mentoring\SoalController@submit');
Route::post('mentoring/bank_soal/delete', 'Mentoring\SoalController@delete');
Route::post('mentoring/bank_soal/getListKategoriSoalSelect', 'Mentoring\SoalController@getListKategoriSoalSelect');
Route::post('mentoring/bank_soal/addJawaban', 'Mentoring\SoalController@addJawaban');


//profile
Route::get('user/profile/edit', 'Users\ProfileController@edit')->middleware('session');

//ajax profile
Route::post('user/profile/submit', 'Users\ProfileController@submit');
Route::post('user/profile/gantiPassword', 'Users\ProfileController@changePassword');
Route::post('user/profile/submitPassword', 'Users\ProfileController@submitPassword');
Route::post('user/profile/submitFoto', 'Users\ProfileController@submitFoto');


//post quiz
Route::get('quiz/posting_quiz', 'Quiz\PostQuizController@index')->middleware('session');
Route::get('quiz/posting_quiz/add/{kategori_ujian_id}', 'Quiz\PostQuizController@add')->middleware('session');
Route::get('quiz/posting_quiz/edit/{id}', 'Quiz\PostQuizController@edit')->middleware('session');
Route::get('quiz/posting_quiz/detail/{id}', 'Quiz\PostQuizController@detail')->middleware('session');
Route::get('quiz/posting_quiz/cari', 'Quiz\PostQuizController@cari')->middleware('session');

//ajax quiz
Route::post('quiz/posting_quiz/submitQuiz', 'Quiz\PostQuizController@submitQuiz');
Route::post('quiz/posting_quiz/getListSoal', 'Quiz\PostQuizController@getListSoal');
Route::post('quiz/posting_quiz/chooseSoal', 'Quiz\PostQuizController@chooseSoal');
Route::post('quiz/posting_quiz/getListUjianSoal', 'Quiz\PostQuizController@getListUjianSoal');
Route::post('quiz/posting_quiz/deleteSoal', 'Quiz\PostQuizController@deleteSoal');
Route::post('quiz/posting_quiz/submitJadwalOpenQuiz', 'Quiz\PostQuizController@submitJadwalOpenQuiz');
Route::post('quiz/posting_quiz/getListSiswa', 'Quiz\PostQuizController@getListSiswa');
Route::post('quiz/posting_quiz/submitChooseSiswa', 'Quiz\PostQuizController@submitChooseSiswa');
Route::post('quiz/posting_quiz/deleteSiswa', 'Quiz\PostQuizController@deleteSiswa');
Route::post('quiz/posting_quiz/getListSiswaUjian', 'Quiz\PostQuizController@getListSiswaUjian');
Route::post('quiz/posting_quiz/detailSoal', 'Quiz\PostQuizController@detailSoal');
Route::post('quiz/posting_quiz/getListDataJamPelajaran', 'Quiz\PostQuizController@getListDataJamPelajaran');
Route::post('quiz/posting_quiz/submitJamQuiz', 'Quiz\PostQuizController@submitJamQuiz');
Route::post('quiz/posting_quiz/showListJadwalJamPelajaran', 'Quiz\PostQuizController@showListJadwalJamPelajaran');
Route::post('quiz/posting_quiz/setJamUjianSiswa', 'Quiz\PostQuizController@setJamUjianSiswa');
Route::post('quiz/posting_quiz/setSettingUjian', 'Quiz\PostQuizController@setSettingUjian');
Route::post('quiz/posting_quiz/publish', 'Quiz\PostQuizController@publish');
Route::post('quiz/posting_quiz/submitPenilaian', 'Quiz\PostQuizController@submitPenilaian');


//batas waktu
Route::get('quiz/batas_waktu', 'Quiz\BatasWaktuController@index')->middleware('session');
Route::get('quiz/batas_waktu/cari', 'Quiz\BatasWaktuController@cari')->middleware('session');
Route::get('quiz/batas_waktu/detail/{id}', 'Quiz\BatasWaktuController@detail')->middleware('session');
Route::get('quiz/batas_waktu/add', 'Quiz\BatasWaktuController@add')->middleware('session');
Route::get('quiz/batas_waktu/edit/{id}', 'Quiz\BatasWaktuController@edit')->middleware('session');

//ajax batas waktu
Route::post('quiz/batas_waktu/submit', 'Quiz\BatasWaktuController@submit');
Route::post('quiz/batas_waktu/delete', 'Quiz\BatasWaktuController@delete');

//jam pelajaran
Route::get('lesson/jam_pelajaran', 'Lesson\JamPelajaranController@index')->middleware('session');
Route::get('lesson/jam_pelajaran/cari', 'Lesson\JamPelajaranController@cari')->middleware('session');
Route::get('lesson/jam_pelajaran/detail/{id}', 'Lesson\JamPelajaranController@detail')->middleware('session');
Route::get('lesson/jam_pelajaran/add', 'Lesson\JamPelajaranController@add')->middleware('session');
Route::get('lesson/jam_pelajaran/edit/{id}', 'Lesson\JamPelajaranController@edit')->middleware('session');

//ajax jam pelajaran
Route::post('lesson/jam_pelajaran/submit', 'Lesson\JamPelajaranController@submit');
Route::post('lesson/jam_pelajaran/delete', 'Lesson\JamPelajaranController@delete');
Route::post('lesson/jam_pelajaran/generateJam', 'Lesson\JamPelajaranController@generateJam');
Route::post('lesson/jam_pelajaran/submitKlasifikasi', 'Lesson\JamPelajaranController@submitKlasifikasi');
Route::post('lesson/jam_pelajaran/setMapelDay', 'Lesson\JamPelajaranController@setMapelDay');

//dashboard
Route::get('dashboard', 'Dashboard\DashboardController@index')->middleware('session');
Route::get('dashboard/redirectlogin', 'Dashboard\DashboardController@redirectLogin');
Route::get('dashboard/jsonreturn', 'Dashboard\DashboardController@jsonReturn');
Route::get('dashboard/tes', 'Dashboard\DashboardController@tesCoba');
Route::get('dashboard/session', 'Dashboard\DashboardController@tesSession');
Route::get('dashboard/db', 'Dashboard\DashboardController@tesDb');
//dashboard

//login
Route::get('login', 'Login\LoginController@index');
Route::get('login/sign_out', 'Login\LoginController@sign_out');
Route::post('login/sign_in', 'Login\LoginController@sign_in');

//registrasi
Route::get('registrasi', 'Registrasi\RegistrasiController@index');

//ajax registrasi
Route::post('registrasi/signUp', 'Registrasi\RegistrasiController@signUp');

// registrasi sekolah
Route::get('notifikasi/registrasi_sekolah', 'Notifikasi\RegistrasiSekolahController@index')->middleware('session');
Route::get('notifikasi/registrasi_sekolah/cari', 'Notifikasi\RegistrasiSekolahController@cari')->middleware('session');
Route::get('notifikasi/registrasi_sekolah/detail/{id}', 'Notifikasi\RegistrasiSekolahController@detail')->middleware('session');

//ajax registrasi sekolah
Route::post('notifikasi/registrasi_sekolah/approve', 'Notifikasi\RegistrasiSekolahController@approve');

//user sekolah
Route::get('setting/user_sekolah', 'Setting\UserSekolahController@index')->middleware('session');
Route::get('setting/user_sekolah/cari', 'Setting\UserSekolahController@cari')->middleware('session');
Route::get('setting/user_sekolah/detail/{id}', 'Setting\UserSekolahController@detail')->middleware('session');
Route::get('setting/user_sekolah/add', 'Setting\UserSekolahController@add')->middleware('session');
Route::get('setting/user_sekolah/edit/{id}', 'Setting\UserSekolahController@edit')->middleware('session');

//ajax user sekolah
Route::post('setting/user_sekolah/submit', 'Setting\UserSekolahController@submit');
Route::post('setting/user_sekolah/delete', 'Setting\UserSekolahController@delete');

//user guru
Route::get('setting/user_guru', 'Setting\UserGuruController@index')->middleware('session');
Route::get('setting/user_guru/cari', 'Setting\UserGuruController@cari')->middleware('session');
Route::get('setting/user_guru/detail/{id}', 'Setting\UserGuruController@detail')->middleware('session');
Route::get('setting/user_guru/add', 'Setting\UserGuruController@add')->middleware('session');
Route::get('setting/user_guru/edit/{id}', 'Setting\UserGuruController@edit')->middleware('session');

//ajax user guru
Route::post('setting/user_guru/submit', 'Setting\UserGuruController@submit');
Route::post('setting/user_guru/delete', 'Setting\UserGuruController@delete');
Route::post('setting/user_guru/getListGuru', 'Setting\UserGuruController@getListGuru');

//user siswa
Route::get('setting/user_siswa', 'Setting\UserSiswaController@index')->middleware('session');
Route::get('setting/user_siswa/cari', 'Setting\UserSiswaController@cari')->middleware('session');
Route::get('setting/user_siswa/detail/{id}', 'Setting\UserSiswaController@detail')->middleware('session');
Route::get('setting/user_siswa/add', 'Setting\UserSiswaController@add')->middleware('session');
Route::get('setting/user_siswa/edit/{id}', 'Setting\UserSiswaController@edit')->middleware('session');

//ajax user siswa
Route::post('setting/user_siswa/submit', 'Setting\UserSiswaController@submit');
Route::post('setting/user_siswa/delete', 'Setting\UserSiswaController@delete');
Route::post('setting/user_siswa/getListSiswa', 'Setting\UserSiswaController@getListSiswa');

//quiz siswa
Route::get('quiz/quiz_siswa', 'Quiz\QuizSiswaController@index')->middleware('session');
Route::get('quiz/quiz_siswa/cari', 'Quiz\QuizSiswaController@cari')->middleware('session');
Route::get('quiz/quiz_siswa/detail/{id}', 'Quiz\QuizSiswaController@detail')->middleware('session');
Route::get('quiz/quiz_siswa/add', 'Quiz\QuizSiswaController@add')->middleware('session');
Route::get('quiz/quiz_siswa/getNilaiQuiz/{siswa_ujian_id}', 'Quiz\QuizSiswaController@getNilaiQuiz')->middleware('session');
Route::get('quiz/quiz_siswa/kerjakan/{id}', 'Quiz\QuizSiswaController@kerjakan')->middleware('session');

//ajax quiz siswa
Route::post('quiz/quiz_siswa/answer', 'Quiz\QuizSiswaController@answer');
Route::post('quiz/quiz_siswa/finish', 'Quiz\QuizSiswaController@finish');

//testing
Route::get('testing', 'TestingController@show');
Route::get('testing/get', 'TestingController@gettest');
Route::get('testing/getelo', 'TestingController@testelo');
Route::get('testing/debug', 'TestingController@tesdebug');
