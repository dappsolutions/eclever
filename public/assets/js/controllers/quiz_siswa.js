var QuizSiswa = {
    module: function () {
        return "quiz/quiz_siswa";
    },

    link_get_module_url: function () {
        return "menu=quiz_siswa&child=quiz";
    },

    ajaxSetup: function () {
        console.log('tes ', $('meta[name="csrf_token"]').attr('content'));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
    },

    kerjakan: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        var shuffle_question = $(elm).attr('shuffle_question');
        var shuffle_answer = $(elm).attr('shuffle_answer');
        var question_per_page = $(elm).attr('question_per_page');
        var remidial = $(elm).attr('remidial');
        var grading_method = $(elm).attr('grading_method');
        var siswa = $(elm).attr('siswa');
        var ujian = $(elm).attr('ujian');
        window.location.href = url.base_url_with_slash(QuizSiswa.module()) + "kerjakan/" + id + "?" + QuizSiswa.link_get_module_url() + "&shuffle_question=" + shuffle_question + "&shuffle_answer=" + shuffle_answer + "&question_per_page=" + question_per_page + "&remidial=" + remidial + "&grading_method=" + grading_method + "&siswa=" + siswa + "&ujian=" + ujian;
    },

    cancel: function () {
        window.location.href = url.base_url(QuizSiswa.module()) + QuizSiswa.link_get_module_url();
    },

    cari: function (elm, e) {
        if (e.keyCode == 13) {
            var keyword = $(elm).val();
            if (keyword == '') {
                window.location.href = url.base_url(QuizSiswa.module()) + QuizSiswa.link_get_module_url();
            } else {
                window.location.href = url.base_url_with_slash(QuizSiswa.module()) + "cari?keyword=" + $.trim(keyword) + "&" + QuizSiswa.link_get_module_url();
            }
        }
    },

    getPostInput: function () {
        var data = {
            'id': $('#id').val(),
            'time_limit': $('#time_limit').val(),
            'sekolah': $('#sekolah').val(),
        };

        return data;
    },

    submit: function () {
        if (validation.run()) {
            var data = QuizSiswa.getPostInput();
            QuizSiswa.ajaxSetup();

            var formData = new FormData();
            formData.append('data', JSON.stringify(data));

            $.ajax({
                type: 'POST',
                dataType: 'json',
                // async: false,
                data: formData,
                processData: false,
                contentType: false,
                url: url.base_url_with_slash(QuizSiswa.module()) + 'submit',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                        var reload = function () {
                            window.location.href = url.base_url_with_slash(QuizSiswa.module()) + "detail/" + resp.id + "?" + QuizSiswa.link_get_module_url();
                        };

                        setTimeout(reload(), 1000);
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        }
    },

    setFormQizard: function (elm) {
        $('#form-quiz').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            onStepChanging: function (event, currentIndex, newIndex) {
                return true;
            }
        });
    },

    answer: function (elm) {
        var number = $.trim($(elm).closest('div.media').find('.number-soal').text());
        var no_soal_posisi = $('.no_soal_' + number);
        var media_body = $(elm).closest('.media-body');

        QuizSiswa.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {
                siswa_answer_id: media_body.find('h6').find('b').attr('siswa_answer_id'),
                jawaban: $(elm).attr('data_id'),
            },
            url: url.base_url_with_slash(QuizSiswa.module()) + 'answer',
            error: function () {
                toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                // message.loading("Sedang Proses Simpan");
            },

            success: function (resp) {
                // message.hideLoading();
                if (resp.is_valid) {
                    // toastr.success('Data Berhasil Disimpan', 'Pesan');
                    no_soal_posisi.addClass('answered');
                } else {
                    toastr.error('Data Gagal Disimpan', 'Pesan');
                }
            }
        });
    },

    soalAnswered: function () {
        var soal = $('span.number-soal');
        $.each(soal, function () {
            var number = $.trim($(this).closest('div.media').find('.number-soal').text());
            var radio_answer = $(this).closest('div.media').find('input.radio_answer');
            var answer = false;
            $.each(radio_answer, function () {
                if ($(this).is(':checked')) {
                    answer = true;
                }
            });
            if (answer) {
                var no_soal_posisi = $('.no_soal_' + number);
                no_soal_posisi.addClass('answered');
            }
        });
    },

    finish: function () {
        $('a[href=#finish]').on('click', function (e) {
            e.preventDefault();
            var data = helper.getParameter();
            data.siswa_has_ujian = $('#siswa_has_ujian').val();

            QuizSiswa.ajaxSetup();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: data,
                url: url.base_url_with_slash(QuizSiswa.module()) + 'finish',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    // message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    // message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                        QuizSiswa.getNilaiQuiz(data.siswa_has_ujian);
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        });
    },

    getNilaiQuiz: function(siswa_ujian){
        window.location.href = url.base_url_with_slash(QuizSiswa.module()) + "getNilaiQuiz/" + siswa_ujian + "?" + QuizSiswa.link_get_module_url();
    }
};


$(function () {

});
