var Siswa = {
    module: function () {
        return "master/siswa";
    },

    link_get_module_url: function () {
        return "menu=siswa&child=master";
    },

    ajaxSetup: function () {
        console.log('tes ', $('meta[name="csrf_token"]').attr('content'));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
    },

    add: function (elm, e) {
        e.preventDefault();
        window.location.href = url.base_url_with_slash(Siswa.module()) + "add?"+Siswa.link_get_module_url();
    },

    edit: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        window.location.href = url.base_url_with_slash(Siswa.module()) + "edit/" + id+"?"+Siswa.link_get_module_url();
    },

    detail: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        window.location.href = url.base_url_with_slash(Siswa.module()) + "detail/" + id+"?"+Siswa.link_get_module_url();
    },

    cancel: function () {
        window.location.href = url.base_url(Siswa.module()) + Siswa.link_get_module_url();
    },

    cari: function (elm, e) {
        if (e.keyCode == 13) {
            var keyword = $(elm).val();
            if (keyword == '') {
                window.location.href = url.base_url(Siswa.module()) + Siswa.link_get_module_url();
            } else {
                window.location.href = url.base_url_with_slash(Siswa.module()) + "cari?keyword=" + $.trim(keyword)+"&"+Siswa.link_get_module_url();
            }
        }
    },

    hapus: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        var html = '<div class="modal-content bd-0 tx-14">';
        // html += '<div class="modal-header pd-y-20 pd-x-25">';
        // html += '<h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message Preview</h6>';
        // html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
        // html += '<span aria-hidden="true">×</span>';
        // html += '</button>';
        // html += '</div>';
        html += '<div class="modal-body pd-25">';
        html += '<h4 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary">Informasi</a></h4>';
        html += '<p class="mg-b-5">Apa anda yakin akan menghapus data ini ?</p>';
        html += '</div>';
        html += '<div class="text-right">';
        html += '<button type="button" data_id="'+id+'" onclick="Siswa.delete(this)" class="btn btn-success pd-x-20">Ya</button>&nbsp;';
        html += '<button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Tidak</button>';
        html += '</div>';
        html += '</div>';

        bootbox.dialog({
            message: html,
            size: 'large'
        });
    },

    delete: function(elm){
        var id = $(elm).attr('data_id');
        Siswa.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {
                id: id
            },
            url: url.base_url_with_slash(Siswa.module()) + 'delete',
            error: function () {
                toastr.error('Data Gagal Hapus, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Hapus..");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Hapus', 'Pesan');
                    var reload = function () {
                        window.location.reload();
                    };

                    setTimeout(reload(), 1000);
                } else {
                    toastr.error('Data Gagal Hapus', 'Pesan');
                }
            }
        });
    },

    getPostInput: function () {
        var data = {
            'id': $('#id').val(),
            'nama': $('#nama').val(),
            'nis': $('#nis').val(),
            'sekolah': $('#sekolah').val(),
        };

        return data;
    },

    submit: function () {
        if (validation.run()) {
            var data = Siswa.getPostInput();
            Siswa.ajaxSetup();

            var formData = new FormData();
            formData.append('data', JSON.stringify(data));

            $.ajax({
                type: 'POST',
                dataType: 'json',
                // async: false,
                data: formData,
                processData: false,
                contentType: false,
                url: url.base_url_with_slash(Siswa.module()) + 'submit',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                        var reload = function () {
                            window.location.href = url.base_url_with_slash(Siswa.module()) + "detail/"+resp.id+"?"+Siswa.link_get_module_url();
                        };

                        setTimeout(reload(), 1000);
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        }
    },

    editKelas: function(elm, e){
        e.preventDefault();
        var action = $(elm).closest('div.content-action').find('.action');
        $.each(action, function(){
            $(this).removeAttr('disabled');
        });
        $(elm).closest('div.content-action').find('div.btn-action').removeClass('hide');
    },

    editAngkatan: function(elm, e){
        e.preventDefault();
        var action = $(elm).closest('div.content-action-angkatan').find('.action');
        $.each(action, function(){
            $(this).removeAttr('disabled');
        });
        $(elm).closest('div.content-action-angkatan').find('div.btn-action').removeClass('hide');
    },

    changeKelas: function(elm, e){
        e.preventDefault();


        var kelas = $('#kelas').val();
        var siswa = $('#id').val();
        Siswa.ajaxSetup();

        if(validation.run()){
            $.ajax({
                type: 'POST',
                dataType: 'json',
                // async: false,
                data: {
                    kelas :kelas,
                    siswa: siswa
                },
                url: url.base_url_with_slash(Siswa.module()) + 'changeKelas',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        }
    },

    changeAngkatan: function(elm, e){
        e.preventDefault();


        var angkatan = $('#angkatan').val();
        var siswa = $('#id').val();
        Siswa.ajaxSetup();

        if(validation.run()){
            $.ajax({
                type: 'POST',
                dataType: 'json',
                // async: false,
                data: {
                    angkatan :angkatan,
                    siswa: siswa
                },
                url: url.base_url_with_slash(Siswa.module()) + 'changeAngkatan',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        }
    },

    cancelKelas: function(elm, e){
        e.preventDefault();
        var action = $(elm).closest('div.content-action').find('.action');
        $.each(action, function(){
            $(this).attr('disabled', 'disabled');
        });

        $(elm).closest('div.content-action').find('div.btn-action').addClass('hide');
        $('p.data-error').remove();
    },

    cancelAngkatan: function(elm, e){
        e.preventDefault();
        var action = $(elm).closest('div.content-action-angkatan').find('.action');
        $.each(action, function(){
            $(this).attr('disabled', 'disabled');
        });

        $(elm).closest('div.content-action-angkatan').find('div.btn-action').addClass('hide');
        $('p.data-error').remove();
    },

    historyKelas: function(elm, e){
        e.preventDefault();

        var siswa = $('#id').val();
        Siswa.ajaxSetup();

        $.ajax({
            type: 'POST',
            dataType: 'html',
            // async: false,
            data: {
                siswa: siswa
            },
            url: url.base_url_with_slash(Siswa.module()) + 'historyKelas',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data...");
            },

            success: function (resp) {
                message.hideLoading();

                bootbox.dialog({
                    message: resp,
                    size: 'large'
                });
            }
        });
    },

    historyAngkatan: function(elm, e){
        e.preventDefault();

        var siswa = $('#id').val();
        Siswa.ajaxSetup();

        $.ajax({
            type: 'POST',
            dataType: 'html',
            // async: false,
            data: {
                siswa: siswa
            },
            url: url.base_url_with_slash(Siswa.module()) + 'historyAngkatan',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data...");
            },

            success: function (resp) {
                message.hideLoading();

                bootbox.dialog({
                    message: resp,
                    size: 'large'
                });
            }
        });
    },

    showDetailUser: function(elm){
        var siswa_user_id = $(elm).attr('siswa_user_id');
        Siswa.ajaxSetup();

        $.ajax({
            type: 'POST',
            dataType: 'html',
            // async: false,
            data: {
                siswa_user: siswa_user_id
            },
            url: url.base_url_with_slash(Siswa.module()) + 'showDetailUser',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data...");
            },

            success: function (resp) {
                message.hideLoading();

                bootbox.dialog({
                    message: resp,
                    size: 'large'
                });
            }
        });
    }
};


$(function () {

});
