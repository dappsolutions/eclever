var PostQuiz = {
    module: function () {
        return "quiz/posting_quiz";
    },

    link_get_module_url: function () {
        return "menu=post_quiz&child=quiz";
    },

    ajaxSetup: function () {
        console.log('tes ', $('meta[name="csrf_token"]').attr('content'));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
    },

    add: function (elm, e) {
        e.preventDefault();

        var id = $(elm).closest('tr').attr('id');
        var html = '<div class="modal-content bd-0 tx-14">';
        html += '<div class="modal-body pd-25">';
        html += '<h4 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary">Tambah Ujian</a></h4>';
        html += '<select class="form-control" id="kat_ujian_dipilih">';
        html += '<option value="1">TERBUKA</option>';
        html += '<option value="2">TERUTUP</option>';
        html += '</select>';
        html += '</div>';
        html += '<div class="text-right">';
        html += '<button type="button" onclick="PostQuiz.addUjian()" class="btn btn-success pd-x-20">Proses</button>&nbsp;';
        html += '<button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Tidak</button>';
        html += '</div>';
        html += '</div>';

        bootbox.dialog({
            message: html,
            size: 'large'
        });
    },

    addUjian: function () {
        var kat_ujian = $('#kat_ujian_dipilih').val();
        window.location.href = url.base_url_with_slash(PostQuiz.module()) + "add/" + kat_ujian + "?" + PostQuiz.link_get_module_url();
    },

    edit: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        window.location.href = url.base_url_with_slash(PostQuiz.module()) + "edit/" + id + "?" + PostQuiz.link_get_module_url();
    },

    detail: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        window.location.href = url.base_url_with_slash(PostQuiz.module()) + "detail/" + id + "?" + PostQuiz.link_get_module_url();
    },

    cancel: function () {
        window.location.href = url.base_url(PostQuiz.module()) + PostQuiz.link_get_module_url();
    },

    cari: function (elm, e) {
        if (e.keyCode == 13) {
            var keyword = $(elm).val();
            if (keyword == '') {
                PostQuiz.cancel();
            } else {
                window.location.href = url.base_url_with_slash(PostQuiz.module()) + "cari?keyword=" + $.trim(keyword) + "&" + PostQuiz.link_get_module_url();
            }
        }
    },

    hapus: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        var html = '<div class="modal-content bd-0 tx-14">';
        html += '<div class="modal-body pd-25">';
        html += '<h4 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary">Informasi</a></h4>';
        html += '<p class="mg-b-5">Apa anda yakin akan menghapus data ini ?</p>';
        html += '</div>';
        html += '<div class="text-right">';
        html += '<button type="button" data_id="' + id + '" onclick="PostQuiz.delete(this)" class="btn btn-success pd-x-20">Ya</button>&nbsp;';
        html += '<button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Tidak</button>';
        html += '</div>';
        html += '</div>';

        bootbox.dialog({
            message: html,
            size: 'large'
        });
    },

    delete: function (elm) {
        var id = $(elm).attr('data_id');
        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {
                id: id
            },
            url: url.base_url_with_slash(PostQuiz.module()) + 'delete',
            error: function () {
                toastr.error('Data Gagal Hapus, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Hapus..");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Hapus', 'Pesan');
                    var reload = function () {
                        window.location.reload();
                    };

                    setTimeout(reload(), 1000);
                } else {
                    toastr.error('Data Gagal Hapus', 'Pesan');
                }
            }
        });
    },

    getPostInputQuiz: function () {
        var data = {
            'id': $('#id').val(),
            'judul': $('#judul').val(),
            'kategori_ujian': $('#kategori_ujian').val(),
            'mapel': $('#mapel').val(),
            'sekolah': $('#sekolah').val(),
        };

        return data;
    },

    validationQuiz: function () {
        var error = 0;
        var form = $('section#form-quiz-p-0');
        $.each(form.find('.required'), function () {
            if ($(this).val() == '') {
                error += 1;
            }
        });

        if (error > 0) {
            $('.data-error').remove();
            $.each(form.find('.required'), function () {
                if ($(this).val() == '') {
                    $(this).after('<p style="color:red" class="data-error">* ' + $(this).attr('error') + ' Harus Diisi</p>');
                }
            });
        }

        return error;
    },

    submitQuiz: function (elm, e) {
        e.preventDefault();

        if (PostQuiz.validationQuiz() == 0) {
            var data = PostQuiz.getPostInputQuiz();
            PostQuiz.ajaxSetup();

            var formData = new FormData();
            formData.append('data', JSON.stringify(data));

            $.ajax({
                type: 'POST',
                dataType: 'json',
                // async: false,
                data: formData,
                processData: false,
                contentType: false,
                url: url.base_url_with_slash(PostQuiz.module()) + 'submitQuiz',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                        var reload = function () {
                            window.location.href = url.base_url_with_slash(PostQuiz.module()) + "edit/" + resp.id + "?" + PostQuiz.link_get_module_url();
                        };

                        setTimeout(reload(), 1000);
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        }
    },

    setFormQizard: function (elm) {
        $('#form-quiz').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            onStepChanging: function (event, currentIndex, newIndex) {
                // console.log(currentIndex+" dan "+newIndex);

                if(currentIndex < newIndex) {
                    console.log('currentIndex', currentIndex);
                    var form = $('section#form-quiz-p-' + currentIndex);
                    var error = 0;
                    $.each(form.find('.required'), function () {
                        if ($(this).val() == '') {
                            error += 1;
                        }
                    });

                    if (error > 0) {
                        $('.data-error').remove();
                        $.each(form.find('.required'), function () {
                            if ($(this).val() == '') {
                                $(this).after('<p style="color:red" class="data-error">* ' + $(this).attr('error') + ' Harus Diisi</p>');
                            }
                        });
                        return false;
                    } else {
                        var ujian = $('#id').val();
                        if(ujian != ''){
                            return true;
                        }
                        toastr.error('Quiz Belum Disimpan', 'Pesan');
                        return false;
                    }
                }else{
                    //finish
                    return true;
                }
            }
        });
    },

    cancelQuiz: function (elm, e) {
        e.preventDefault();
    },

    deleteSoal: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('data_id');

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            // async: false,
            data: {
                id: id
            },
            url: url.base_url_with_slash(PostQuiz.module()) + 'deleteSoal',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Hapus Data");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Dihapus', 'Pesan');
                    $('#kategori_soal').trigger('change');
                    PostQuiz.getListUjianSoal();
                } else {
                    toastr.error('Data Gagal Dihapus', 'Pesan');
                }
            }
        });
    },

    getListSoal: function (elm) {
        var kategori_soal = $(elm).val();
        var ujian = $('#id').val();

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'html',
            // async: false,
            data: {
                kategori_soal: kategori_soal,
                ujian: ujian
            },
            url: url.base_url_with_slash(PostQuiz.module()) + 'getListSoal',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data");
            },

            success: function (resp) {
                message.hideLoading();
                $('div.content-list-soal').html(resp);
            }
        });
    },

    chooseSoal: function (elm) {
        var id_soal = $(elm).closest('tr').attr('data_id');
        var id_ujian = $('#id').val();

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            // async: false,
            data: {
                id_soal: id_soal,
                id_ujian: id_ujian
            },
            url: url.base_url_with_slash(PostQuiz.module()) + 'chooseSoal',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Simpan Data");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Disimpan', 'Pesan');
                    $(elm).remove();
                    PostQuiz.getListUjianSoal();
                } else {
                    toastr.error('Data Gagal Disimpan', 'Pesan');
                }
            }
        });
    },

    getListUjianSoal: function () {
        var id_ujian = $('#id').val();

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'html',
            // async: false,
            data: {
                id_ujian: id_ujian
            },
            url: url.base_url_with_slash(PostQuiz.module()) + 'getListUjianSoal',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data");
            },

            success: function (resp) {
                message.hideLoading();
                $('div.content-soal-choosed').html(resp);
            }
        });
    },

    searchInTableSoal: function (elm, e) {
        var value = $(elm).val().toLowerCase();
        PostQuiz.searchInTable(value, "table#tb-list-soal");
    },

    searchInTable: function (value, elm) {
        $("" + elm + " tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
    },

    setDatePicker: function () {
        $('#tanggal').daterangepicker();
    },

    validationJadwalOpenQuiz: function () {
        var error = 0;
        var form = $('section#form-quiz-p-2');
        $.each(form.find('.required'), function () {
            if ($(this).val() == '') {
                error += 1;
            }
        });

        if (error > 0) {
            $('.data-error').remove();
            $.each(form.find('.required'), function () {
                if ($(this).val() == '') {
                    $(this).after('<p style="color:red" class="data-error">* ' + $(this).attr('error') + ' Harus Diisi</p>');
                }
            });
        }

        return error;
    },

    submitJadwalOpenQuiz: function (elm, e) {
        e.preventDefault();
        var ujian = $('#id').val();
        var time_limit = $('#time_limit').length > 0 ? $('#time_limit').val() : '';
        var tanggal = $('#tanggal').val();
        var id = $('#ujian_time_open').val();

        if (PostQuiz.validationJadwalOpenQuiz() == 0) {
            PostQuiz.ajaxSetup();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: {
                    ujian: ujian,
                    id: id,
                    time_limit: time_limit,
                    tanggal: tanggal
                },
                url: url.base_url_with_slash(PostQuiz.module()) + 'submitJadwalOpenQuiz',
                error: function () {
                    toastr.error('Data Gagal, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan Data");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        var reload = function () {
                            window.location.reload();
                        };

                        toastr.success("Simpan Data Berhasil", "Pesan");
                        setTimeout(reload(), 3000);
                    } else {
                        toastr.error("Simpan Data Gagal", "Pesan");
                    }
                }
            });
        }
    },

    getListSiswa: function (elm) {
        var kelas = $(elm).val();
        var ujian = $('#id').val();

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'html',
            // async: false,
            data: {
                kelas: kelas,
                ujian: ujian
            },
            url: url.base_url_with_slash(PostQuiz.module()) + 'getListSiswa',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Prsses Retrieving Data");
            },

            success: function (resp) {
                message.hideLoading();
                $('table#tb_list_siswa').find('tbody').html(resp);
            }
        });
    },

    checkAllSiswa: function (elm) {
        var checked = $(elm).is(':checked');
        var data_siswa = $(elm).closest('table').find('tbody').find('.not-checked');
        $.each(data_siswa, function () {
            var checkedbox = $(this).find('input');
            if (checked) {
                checkedbox.prop('checked', true);
            } else {
                checkedbox.prop('checked', false);
            }
        });

        if(checked){
            $('#action-submit-siswa').removeClass('hide');
        }else{
            $('#action-submit-siswa').addClass('hide');
        }
    },

    checkedSiswa: function (elm) {
        var total_checked = 0;
        var data_siswa = $(elm).closest('table').find('tbody').find('.not-checked');

        if($(elm).is(':checked')){
            $('#action-submit-siswa').removeClass('hide');
        }

        $.each(data_siswa, function () {
            var checkedbox = $(this).find('input');
            if(checkedbox.is(':checked')){
                total_checked +=1;
            }
        });

        if(data_siswa.length == total_checked){
            $('#check_head_siswa').prop('checked', true);
        }else{
            $('#check_head_siswa').prop('checked', false);
            if(total_checked == 0){
                $('#action-submit-siswa').addClass('hide');
            }
        }
    },

    getInputPostDataSiswa: function(){
        var data_siswa = $('table#tb_list_siswa').find('tbody').find('.not-checked');
        var data = [];

        $.each(data_siswa, function(){
            var checkedbox = $(this).find('input');
            if(checkedbox.is(':checked')){
                data.push({
                    'siswa': $(this).closest('tr').attr('data_id')
                });
            }
        });

        return data;
    },

    showListJadwalJamPelajaran: function(){
        var ujian = $('#id').val();

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'html',
            data:{
                ujian: ujian
            },
            url: url.base_url_with_slash(PostQuiz.module()) + 'showListJadwalJamPelajaran',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data");
            },

            success: function (resp) {
                message.hideLoading();
                bootbox.dialog({
                    message: resp
                });
            }
        });
    },

    submitChooseSiswa: function(elm, e){
        e.preventDefault();

        var kategori_ujian = $('#kategori_ujian').val();
        if(kategori_ujian == '2'){
            PostQuiz.showListJadwalJamPelajaran();
            return;
        }
        var data = PostQuiz.getInputPostDataSiswa();
        var ujian = $('#id').val();

        var formData = new FormData();
        formData.append('data', JSON.stringify(data));
        formData.append('ujian', ujian);

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            // async: false,
            data: formData,
            processData: false,
            contentType: false,
            url: url.base_url_with_slash(PostQuiz.module()) + 'submitChooseSiswa',
            error: function () {
                toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Simpan");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Disimpan', 'Pesan');
                    var reload = function () {
                        window.location.reload();
                    };

                    setTimeout(reload(), 3000);
                } else {
                    toastr.error('Data Gagal Disimpan', 'Pesan');
                }
            }
        });
    },

    deleteSiswa: function(elm, e){
        e.preventDefault();
        var id = $(elm).closest('tr').attr('data_id');

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            // async: false,
            data: {
                id: id
            },
            url: url.base_url_with_slash(PostQuiz.module()) + 'deleteSiswa',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Hapus Data");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Dihapus', 'Pesan');
                    $('#kelas').trigger('change');
                    PostQuiz.getListSiswaUjian();
                } else {
                    toastr.error('Data Gagal Dihapus', 'Pesan');
                }
            }
        });
    },

    searchInTableSiswa: function(elm, e){
        var value = $(elm).val().toLowerCase();
        PostQuiz.searchInTable(value, "table#tb_list_siswa");
    },

    getListSiswaUjian: function(){
        var id_ujian = $('#id').val();

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'html',
            // async: false,
            data: {
                id_ujian: id_ujian
            },
            url: url.base_url_with_slash(PostQuiz.module()) + 'getListSiswaUjian',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data");
            },

            success: function (resp) {
                message.hideLoading();
                $('div#content-data-siswa-ujian').html(resp);
            }
        });
    },

    finish: function(){
        $('a[href=#finish]').on('click', function(e){
            e.preventDefault();
            var ujian = $('#id').val();
            var disabled = $('#disabled').val();
            if(disabled == ''){
                window.location.href = url.base_url_with_slash(PostQuiz.module())+"detail/"+ujian+"?"+PostQuiz.link_get_module_url();
            }else{
                PostQuiz.cancel();
            }
        });
    },

    detailSoal: function(elm ,e){
        e.preventDefault();
        var soal = $(elm).attr('soal');

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'html',
            data: {
                soal: soal
            },
            url: url.base_url_with_slash(PostQuiz.module()) + 'detailSoal',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data");
            },

            success: function (resp) {
                message.hideLoading();
                bootbox.dialog({
                    message: resp
                });
            }
        });
    },

    getListDataJamPelajaran: function(elm){
        var jam_mapel = $(elm).val();

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'html',
            data: {
                jam_mapel: jam_mapel
            },
            url: url.base_url_with_slash(PostQuiz.module()) + 'getListDataJamPelajaran',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data");
            },

            success: function (resp) {
                message.hideLoading();
                $('tbody.tdata-jadwal-jam').html(resp);
            }
        });
    },

    checkedAllJamPelajaran: function(elm){
        var checked = $(elm).is(':checked');
        var data_jam = $(elm).closest('table').find('tbody').find('.not-checked');
        $.each(data_jam, function () {
            var checkedbox = $(this).find('input');
            if (checked) {
                checkedbox.prop('checked', true);
            } else {
                checkedbox.prop('checked', false);
            }
        });
    },

    checkedJamPelajaran: function(elm){
        var total_checked = 0;
        var data_jam = $(elm).closest('table').find('tbody').find('.not-checked');

        if($(elm).is(':checked')){
            $('#action-submit-jam').removeClass('hide');
        }

        $.each(data_jam, function () {
            var checkedbox = $(this).find('input');
            if(checkedbox.is(':checked')){
                total_checked +=1;
            }
        });

        if(data_jam.length == total_checked){
            $('#check-head-jam').prop('checked', true);
        }else{
            $('#check-head-jam').prop('checked', false);
        }
    },

    getInputPostDataJamPelajaran: function(){
        var data_jam = $('table#tb_list_jam_pelajaran').find('tbody').find('.not-checked');
        var data = [];

        $.each(data_jam, function(){
            var checkedbox = $(this).find('input');
            if(checkedbox.is(':checked')){
                data.push({
                    'klasifikasi_jam': $(this).closest('tr').attr('data_id')
                });
            }
        });

        return data;
    },

    submitJamQuiz: function(elm, e){
        e.preventDefault();
        var data = PostQuiz.getInputPostDataJamPelajaran();
        var ujian = $('#id').val();

        var formData = new FormData();
        formData.append('data', JSON.stringify(data));
        formData.append('ujian', ujian);
        formData.append('jam_pelajaran', $('#jam_pelajaran').val());

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            // async: false,
            data: formData,
            processData: false,
            contentType: false,
            url: url.base_url_with_slash(PostQuiz.module()) + 'submitJamQuiz',
            error: function () {
                toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Simpan");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Disimpan', 'Pesan');
                    var reload = function () {
                        window.location.reload();
                    };

                    setTimeout(reload(), 3000);
                } else {
                    toastr.error('Data Gagal Disimpan', 'Pesan');
                }
            }
        });
    },

    setJamUjianSiswa: function(elm){
        var jam_ujian = $(elm).val();
        var ujian = $('#id').val();


        var data = PostQuiz.getInputPostDataSiswa();
        var ujian = $('#id').val();

        var formData = new FormData();
        formData.append('data', JSON.stringify(data));
        formData.append('ujian', ujian);
        formData.append('jam_ujian', jam_ujian);

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            // async: false,
            data: formData,
            processData: false,
            contentType: false,
            url: url.base_url_with_slash(PostQuiz.module()) + 'setJamUjianSiswa',
            error: function () {
                toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Simpan");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Disimpan', 'Pesan');
                    var reload = function () {
                        window.location.reload();
                    };

                    setTimeout(reload(), 3000);
                } else {
                    toastr.error('Data Gagal Disimpan', 'Pesan');
                }
            }
        });
    },

    setSettingUjian: function(elm){
        var field = $(elm).attr('field');
        var id_shuffle = $(elm).attr('id_shuffle');
        var nilai = $(elm).is(':checked') ? 1 : 0;
        var ujian = $('#id').val();
        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            // async: false,
            data: {
                field: field,
                nilai: nilai,
                ujian: ujian,
                id_shuffle: id_shuffle
            },
            url: url.base_url_with_slash(PostQuiz.module()) + 'setSettingUjian',
            error: function () {
                toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Simpan");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Disimpan', 'Pesan');
                } else {
                    toastr.error('Data Gagal Disimpan', 'Pesan');
                }
            }
        });
    },

    getPostInputSetting: function(){
        var data = {
            'ujian':$('#id').val(),
            'id_shuffle': $('#question_per_page').attr('id_shuffle'),
            'shuffle_question': $('#check_acak_soal').is(':checked') ? 1 : 0,
            'shuffle_answer': $('#check_acak_jawaban').is(':checked') ? 1 : 0,
            'question_per_page': $('#question_per_page').val(),
            'remidial': $('#remidial').val(),
            'grading_method': $('#grading_method').val()
        };
        return data;
    },

    submitSetting: function(elm, e){
        e.preventDefault();
        var data = PostQuiz.getPostInputSetting();
        var formData = new FormData();
        formData.append('data', JSON.stringify(data));

        PostQuiz.ajaxSetup();
        if(validation.run()){
            $.ajax({
                type: 'POST',
                dataType: 'json',
                // async: false,
                data: formData,
                processData: false,
                contentType: false,
                url: url.base_url_with_slash(PostQuiz.module()) + 'setSettingUjian',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        }
    },

    changeSoalPerPage: function(elm){
        var total_soal_ujian = parseInt($('#total_soal_ujian').val());
        var input_per_page = parseInt($(elm).val());
        if(input_per_page > total_soal_ujian){
            $(elm).val(0);
            toastr.error('Soal per halaman melebihi jumlah total soal ujian', 'Pesan');
        }
    },

    publish: function(elm){
        var ujian = $(elm).closest('tr').attr('id');
        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {
                ujian: ujian
            },
            url: url.base_url_with_slash(PostQuiz.module()) + 'publish',
            error: function () {
                toastr.error('Data Gagal Publish, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Publish");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    $(elm).closest('td').html('<i class="text-success">PUBLISH</i>');
                    toastr.success('Data Berhasil Publish', 'Pesan');
                } else {
                    toastr.error('Data Gagal Publish', 'Pesan');
                }
            }
        });
    },

    setDasarPenliaian: function(elm){
        var penilaian = $('#penilaian').val();
        if(penilaian == '1'){
            $('h5.total-poin').removeClass('hide');
            $('#total-poin').text(0);
            var poin_soal = $('input.poin-soal');
            $.each(poin_soal, function(){
                $(this).val(0);
                $(this).removeClass('hide');
            });

            var poin_jawaban = $('input.poin-jawaban');
            $.each(poin_jawaban, function(){
                $(this).addClass('hide');
                $(this).val(0);
            });
        }else{
            $('h5.total-poin').addClass('hide');
            $('#total-poin').text(0);
            var poin_soal = $('input.poin-soal');
            $.each(poin_soal, function(){
                $(this).val(0);
                $(this).addClass('hide');
            });

            var poin_jawaban = $('input.poin-jawaban');
            $.each(poin_jawaban, function(){
                $(this).removeClass('hide');
                $(this).val(0);
            });
        }
    },

    hitungTotalPoinSoal: function(elm, e){
        var total = 0;

        var all_soal_poin = $('.poin-soal');
        $.each(all_soal_poin, function(){
            if(!isNaN(parseInt($(this).val()))){
                total += parseInt($(this).val());
            }
        });

        var poin_soal = $('#total-poin');
        poin_soal.text(total);
    },

    getInputPostItemPenilaian: function(){
        var data = [];
        var tb_data_soal = $('table#tb_data_soal').find('tbody').find('tr.data_soal');
        $.each(tb_data_soal, function(){
            var data_jawaban = [];
            var class_jawaban = $(this).attr('class_jawaban');
            var tr_jawaban = $('tr.'+class_jawaban);
            $.each(tr_jawaban, function(){
                data_jawaban.push({
                    'ujian_jawaban_id': $(this).attr('ujian_jawaban_id'),
                    'jawaban_id': $(this).attr('jawaban_id'),
                    'poin': $(this).find('.poin-jawaban').val()
                });
            });

            data.push({
                'ujian_soal_nilai_id': $(this).attr('ujian_soal_nilai_id'),
                'ujian_soal_id': $(this).attr('ujian_soal_id'),
                'poin': $(this).find('.poin-soal').val(),
                'data_jawaban': data_jawaban
            });
        });

        return data;
    },

    getPostInputPenilaian: function(){
        var data = {
            'ujian': $('#id').val(),
            'header':{
                'penilaian_id': $('#penilaian').attr('data_id'),
                'penilaian': $('#penilaian').val()
            },

            'item': PostQuiz.getInputPostItemPenilaian()
        };

        return data;
    },

    submitPenilaian: function(elm, e){
        e.preventDefault();
        var data = PostQuiz.getPostInputPenilaian();
        var formData = new FormData();
        formData.append('data', JSON.stringify(data));

        PostQuiz.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            // async: false,
            data: formData,
            processData: false,
            contentType: false,
            url: url.base_url_with_slash(PostQuiz.module()) + 'submitPenilaian',
            error: function () {
                toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Simpan");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Disimpan', 'Pesan');
                    var reload = function () {
                        window.location.reload();
                    };

                    setTimeout(reload(), 3000);
                } else {
                    toastr.error('Data Gagal Disimpan', 'Pesan');
                }
            }
        });
    }
};


$(function () {

});
