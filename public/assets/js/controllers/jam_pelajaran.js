var JamPelajaran = {
    module: function () {
        return "lesson/jam_pelajaran";
    },

    link_get_module_url: function () {
        return "menu=jam_pelajaran&child=lesson";
    },

    ajaxSetup: function () {
        console.log('tes ', $('meta[name="csrf_token"]').attr('content'));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
    },

    add: function (elm, e) {
        e.preventDefault();
        window.location.href = url.base_url_with_slash(JamPelajaran.module()) + "add?" + JamPelajaran.link_get_module_url();
    },

    edit: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        window.location.href = url.base_url_with_slash(JamPelajaran.module()) + "edit/" + id + "?" + JamPelajaran.link_get_module_url();
    },

    detail: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        window.location.href = url.base_url_with_slash(JamPelajaran.module()) + "detail/" + id + "?" + JamPelajaran.link_get_module_url();
    },

    cancel: function () {
        window.location.href = url.base_url(JamPelajaran.module()) + JamPelajaran.link_get_module_url();
    },

    cari: function (elm, e) {
        if (e.keyCode == 13) {
            var keyword = $(elm).val();
            if (keyword == '') {
                window.location.href = url.base_url(JamPelajaran.module()) + JamPelajaran.link_get_module_url();
            } else {
                window.location.href = url.base_url_with_slash(JamPelajaran.module()) + "cari?keyword=" + $.trim(keyword) + "&" + JamPelajaran.link_get_module_url();
            }
        }
    },

    hapus: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        var html = '<div class="modal-content bd-0 tx-14">';
        // html += '<div class="modal-header pd-y-20 pd-x-25">';
        // html += '<h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message Preview</h6>';
        // html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
        // html += '<span aria-hidden="true">×</span>';
        // html += '</button>';
        // html += '</div>';
        html += '<div class="modal-body pd-25">';
        html += '<h4 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary">Informasi</a></h4>';
        html += '<p class="mg-b-5">Apa anda yakin akan menghapus data ini ?</p>';
        html += '</div>';
        html += '<div class="text-right">';
        html += '<button type="button" data_id="' + id + '" onclick="JamPelajaran.delete(this)" class="btn btn-success pd-x-20">Ya</button>&nbsp;';
        html += '<button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Tidak</button>';
        html += '</div>';
        html += '</div>';

        bootbox.dialog({
            message: html,
            size: 'large'
        });
    },

    delete: function (elm) {
        var id = $(elm).attr('data_id');
        JamPelajaran.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {
                id: id
            },
            url: url.base_url_with_slash(JamPelajaran.module()) + 'delete',
            error: function () {
                toastr.error('Data Gagal Hapus, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Hapus..");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Hapus', 'Pesan');
                    var reload = function () {
                        window.location.reload();
                    };

                    setTimeout(reload(), 1000);
                } else {
                    toastr.error('Data Gagal Hapus', 'Pesan');
                }
            }
        });
    },

    getPostInput: function () {
        var data = {
            'id': $('#id').val(),
            'lama': $('#lama').val(),
            'judul': $('#judul').val(),
            'sekolah': $('#sekolah').val(),
        };

        return data;
    },

    submit: function (elm, e) {
        e.preventDefault();
        if (validation.run()) {
            var data = JamPelajaran.getPostInput();
            JamPelajaran.ajaxSetup();

            var formData = new FormData();
            formData.append('data', JSON.stringify(data));

            $.ajax({
                type: 'POST',
                dataType: 'json',
                // async: false,
                data: formData,
                processData: false,
                contentType: false,
                url: url.base_url_with_slash(JamPelajaran.module()) + 'submit',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                        var reload = function () {
                            window.location.href = url.base_url_with_slash(JamPelajaran.module()) + "edit/" + resp.id + "?" + JamPelajaran.link_get_module_url();
                        };

                        setTimeout(reload(), 1000);
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        }
    },

    setFormWizard: function () {
        $('#form-jam').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            onStepChanging: function (event, currentIndex, newIndex) {
                // console.log(currentIndex+" dan "+newIndex);

                if (currentIndex < newIndex) {
                    console.log('currentIndex', currentIndex);
                    var form = $('section#form-jam-p-' + currentIndex);
                    var error = 0;
                    $.each(form.find('.required'), function () {
                        if ($(this).val() == '') {
                            error += 1;
                        }
                    });

                    if (error > 0) {
                        $('.data-error').remove();
                        $.each(form.find('.required'), function () {
                            if ($(this).val() == '') {
                                $(this).after('<p style="color:red" class="data-error">* ' + $(this).attr('error') + ' Harus Diisi</p>');
                            }
                        });
                        return false;
                    } else {
                        var ujian = $('#id').val();
                        if (ujian != '') {
                            return true;
                        }
                        toastr.error('Jam Belum Disimpan', 'Pesan');
                        return false;
                    }
                } else {
                    //finish
                    return true;
                }
            }
        });
    },

    setTimePicker: function () {
        $('.timepicker').timepicker({
            showInputs: false,
            showMeridian: false
        });

        // glyphicon glyphicon-chevron-up
        var icon = $('i.glyphicon');
        $.each(icon, function(){
            $(this).addClass('icon');
            if($(this).hasClass('glyphicon-chevron-up')){
                $(this).addClass('ion-arrow-up-a');
            }

            if($(this).hasClass('glyphicon-chevron-down')){
                $(this).addClass('ion-arrow-down-a');
            }
        });
    },

    generateJam: function(elm){
        JamPelajaran.ajaxSetup();

        var durasi = $('#lama').val();
        var jumlah_jam = $('#jumlah_jam').val();
        var jam = $('#jam').val();
        if(jumlah_jam == ''){
            toastr.error('Jumlah Jam Belum Diisi', 'Pesan');
            return;
        }
        $.ajax({
            type: 'POST',
            dataType: 'html',
            data:{
                jam : jam,
                durasi : durasi,
                jumlah_jam: jumlah_jam
            },
            url: url.base_url_with_slash(JamPelajaran.module()) + 'generateJam',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Generate Data...");
            },

            success: function (resp) {
                message.hideLoading();
                $('div#data-generate-jam').html(resp);
            }
        });
    },

    getPostDataInputGenerate: function(){
        var data = [];
        var table = $('#tb-list-generate-jam').find('tbody').find('tr.tr_input');
        $.each(table, function(){
            var id = $(this).attr('data_id');
            if(id == ''){
                var jam_awal = $(this).find('#jam_awal').val();
                var jam_akhir = $(this).find('#jam_akhir').val();
                data.push({
                    'jam_awal': jam_awal,
                    'jam_akhir': jam_akhir
                });
            }
        });

        return data;
    },

    getPostInputKlasifikasi: function(){
        var data = [];
        data = {
            'jam_mapel_id': $('#id').val(),
            'jam_set': $('#jam').val(),
            'jumlah_jam': $('#jumlah_jam').val(),
            'data_generate': JamPelajaran.getPostDataInputGenerate()
        };

        return data;
    },

    submitKlasifikasi: function(elm, e){
        e.preventDefault();
        var table = $('#data-generate-jam').find('table');
        JamPelajaran.ajaxSetup();
        if(table.length > 0){
            var data = JamPelajaran.getPostInputKlasifikasi();

            var formData = new FormData();
            formData.append('data', JSON.stringify(data));

            $.ajax({
                type: 'POST',
                dataType: 'json',
                // async: false,
                data: formData,
                processData: false,
                contentType: false,
                url: url.base_url_with_slash(JamPelajaran.module()) + 'submitKlasifikasi',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                        var reload = function () {
                            window.location.reload();
                        };

                        setTimeout(reload(), 1000);
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        }else{
            toastr.error('Jam Belum di Generate', 'Pesan');
        }
    },

    setMapelDay: function(elm){
        var mapel_day_id = $(elm).attr('data_id');
        var day = $(elm).attr('day');
        var jam_id = $('#id').val();
        var choose = $(elm).is(':checked') ? 1 : 0;

        JamPelajaran.ajaxSetup();

        $.ajax({
            type: 'POST',
            dataType: 'json',
            data:{
                mapel_day_id : mapel_day_id,
                day : day,
                jam_id: jam_id,
                choose: choose
            },
            url: url.base_url_with_slash(JamPelajaran.module()) + 'setMapelDay',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Simpan Data...");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Disimpan', 'Pesan');
                } else {
                    toastr.error('Data Gagal Disimpan', 'Pesan');
                }
            }
        });
    },

    finish: function(){
        $('a[href=#finish]').on('click', function(e){
            e.preventDefault();
            var jam_id = $('#id').val();
            var disabled = $('#disabled').val();
            if(disabled == ''){
                window.location.href = url.base_url_with_slash(JamPelajaran.module())+"detail/"+jam_id+"?"+JamPelajaran.link_get_module_url();
            }else{
                JamPelajaran.cancel();
            }
        });
    },
};


$(function () {

});
