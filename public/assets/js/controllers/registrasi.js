var Registrasi = {
    module: function () {
        return 'registrasi';
    },

    ajaxSetup: function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
    },

    signUp: function (elm, e) {
        e.preventDefault();

        var nama_sekolah = $('#nama_sekolah').val();
        var email = $('#email').val();
        var no_telp = $('#no_telp').val();
        var alamat = $('#alamat').val();

        //   message.loading("Proses Authentifikasi Data...");
        //   return;
        if (validation.run()) {
            Registrasi.ajaxSetup();

            $.ajax({
                type: 'POST',
                data: {
                    nama_sekolah: nama_sekolah,
                    email: email,
                    no_telp:no_telp,
                    alamat:alamat
                },
                dataType: 'json',
                // async: false,
                url: url.base_url_with_slash(Registrasi.module()) + 'signUp',
                error: function () {
                    toastr.error("Gagal", 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Proses Registrasi Data...");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success("Registrasi Berhasil", 'Pesan');
                        toastr.info("Username dan Password akan dikirimkan melalui email terkait apabila disetujui oleh superadmin", 'Informasi', { timeOut: 10000 });
                        $.each($('.required'), function(){
                            $(this).val('');
                        });
                    } else {
                        toastr.error("Username atau Password Tidak Valid", 'Pesan');
                    }
                }
            });
        }
    },

    goto_dashboard: function () {
        // var url = "dashboard/dashboard";
        window.location.href = url.base_url('dashboard')+"?menu=dashboard&child=no";
    },
};

$(function () {

});
