var Soal = {
    module: function () {
        return "mentoring/bank_soal";
    },

    link_get_module_url: function () {
        return "menu=bank_soal&child=mentoring";
    },

    ajaxSetup: function () {
        console.log('tes ', $('meta[name="csrf_token"]').attr('content'));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
    },

    add: function (elm, e) {
        e.preventDefault();
        window.location.href = url.base_url_with_slash(Soal.module()) + "add?"+Soal.link_get_module_url();
    },

    edit: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        window.location.href = url.base_url_with_slash(Soal.module()) + "edit/" + id+"?"+Soal.link_get_module_url();
    },

    detail: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        window.location.href = url.base_url_with_slash(Soal.module()) + "detail/" + id+"?"+Soal.link_get_module_url();
    },

    cancel: function () {
        window.location.href = url.base_url(Soal.module()) + Soal.link_get_module_url();
    },

    cari: function (elm, e) {
        if (e.keyCode == 13) {
            var keyword = $(elm).val();
            if (keyword == '') {
                Soal.cancel();
            } else {
                window.location.href = url.base_url_with_slash(Soal.module()) + "cari?keyword=" + $.trim(keyword)+"&"+Soal.link_get_module_url();
            }
        }
    },

    hapus: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        var html = '<div class="modal-content bd-0 tx-14">';
        // html += '<div class="modal-header pd-y-20 pd-x-25">';
        // html += '<h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message Preview</h6>';
        // html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
        // html += '<span aria-hidden="true">×</span>';
        // html += '</button>';
        // html += '</div>';
        html += '<div class="modal-body pd-25">';
        html += '<h4 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary">Informasi</a></h4>';
        html += '<p class="mg-b-5">Apa anda yakin akan menghapus data ini ?</p>';
        html += '</div>';
        html += '<div class="text-right">';
        html += '<button type="button" data_id="'+id+'" onclick="Soal.delete(this)" class="btn btn-success pd-x-20">Ya</button>&nbsp;';
        html += '<button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Tidak</button>';
        html += '</div>';
        html += '</div>';

        bootbox.dialog({
            message: html,
            size: 'large'
        });
    },

    delete: function(elm){
        var id = $(elm).attr('data_id');
        Soal.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {
                id: id
            },
            url: url.base_url_with_slash(Soal.module()) + 'delete',
            error: function () {
                toastr.error('Data Gagal Hapus, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Hapus..");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Hapus', 'Pesan');
                    var reload = function () {
                        window.location.reload();
                    };

                    setTimeout(reload(), 1000);
                } else {
                    toastr.error('Data Gagal Hapus', 'Pesan');
                }
            }
        });
    },

    getPostInputJawaban: function(){
        var jawaban = [];
        var tr = $('table#tb_jawaban').find('tbody').find('tr');
        $.each(tr, function () {
         jawaban.push({
          'jawaban': $(this).find('textarea').val(),
          'jawaban_id': $(this).find('textarea').attr('jawaban_id'),
          'benar': $(this).find('#rd_benar').is(':checked') ? 1 : 0,
          'deleted': $(this).hasClass('deleted') ? 1 : 0
         });
        });

        return jawaban;
    },

    getPostInput: function () {
        var data = {
            'id': $('#id').val(),
            'kategori': $('#kategori_soal').val(),
            'mapel': $('#mapel').val(),
            'sekolah': $('#sekolah').val(),
            'soal': $('#soal').val(),
            'jawaban': Soal.getPostInputJawaban(),
        };

        return data;
    },

    submit: function () {
        tinymce.triggerSave();
        if (validation.run()) {
            var data = Soal.getPostInput();
            Soal.ajaxSetup();

            var formData = new FormData();
            formData.append('data', JSON.stringify(data));

            $.ajax({
                type: 'POST',
                dataType: 'json',
                // async: false,
                data: formData,
                processData: false,
                contentType: false,
                url: url.base_url_with_slash(Soal.module()) + 'submit',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                        var reload = function () {
                            window.location.href = url.base_url_with_slash(Soal.module()) + "detail/"+resp.id+"?"+Soal.link_get_module_url();
                        };

                        setTimeout(reload(), 1000);
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        }
    },

    editMapel: function(elm){
        var action = $(elm).closest('div.content-action').find('.action');
        $.each(action, function(){
            $(this).removeAttr('disabled');
        });
        $(elm).closest('div.content-action').find('div.btn-action').removeClass('hide');
    },

    changeMapel: function(elm, e){
        e.preventDefault();
        var action = $(elm).closest('div.content-action').find('.action');
        var checked_id = [];

        $.each(action, function(){
            checked_id.push({
                'guru': $('input#id').val(),
                'mapel_id': $(this).attr('data_id'),
                'guru_mapel_id': $(this).attr('guru_mapel_id'),
                'checked': $(this).is(':checked') ? 1 : 0
            });
        });


        Soal.ajaxSetup();
        var formData = new FormData();
        formData.append('data', JSON.stringify(checked_id));

        $.ajax({
            type: 'POST',
            dataType: 'json',
            // async: false,
            data: formData,
            processData: false,
            contentType: false,
            url: url.base_url(Soal.module()) + 'changeMapel',
            error: function () {
                toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Simpan");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Disimpan', 'Pesan');
                    // var reload = function () {
                    //     window.location.href = url.base_url(Soal.module()) + "";
                    // };

                    // setTimeout(reload(), 1000);
                } else {
                    toastr.error('Data Gagal Disimpan', 'Pesan');
                }
            }
        });

    },

    cancelMapel: function(elm, e){
        e.preventDefault();
        var action = $(elm).closest('div.content-action').find('.action');
        $.each(action, function(){
            $(this).attr('disabled', 'disabled');
        });

        $(elm).closest('div.content-action').find('div.btn-action').addClass('hide');
    },

    getListKategoriSoal: function(elm){
        var mapel = $(elm).val();
        Soal.ajaxSetup();

        $.ajax({
            type: 'POST',
            dataType: 'html',
            // async: false,
            data: {
                mapel: mapel
            },
            url: url.base_url_with_slash(Soal.module()) + 'getListKategoriSoalSelect',
            error: function () {
                toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data...");
            },

            success: function (resp) {
                message.hideLoading();
                $('div#list_kategori').html(resp);
            }
        });
    },

    setTinyMCE: function () {
        tinymce.init({
         paste_data_images: true,
         selector: 'textarea',
         menubar: 'insert',
         theme: 'modern',
         plugins: 'table charmap tiny_mce_wiris image',
         file_picker_types: 'image',
         image_title: true,
         automatic_uploads: true,
         file_picker_callback: function (cb, value, meta) {
          var input = document.createElement('input');
          input.setAttribute('type', 'file');
          input.setAttribute('accept', 'image/*');
          input.onchange = function () {
           var file = this.files[0];
      //     $(this).attr('ondrag', 'membuatl_soal_data.dragImage()');

           var reader = new FileReader();
           reader.onload = function () {
            var id = 'blobid' + (new Date()).getTime();
            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
            var base64 = reader.result.split(',')[1];
            var blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);

            // call the callback and populate the Title field with the file name
            cb(blobInfo.blobUri(), {title: file.name});
           };
           reader.readAsDataURL(file);
          };

          input.click();
         },
         toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry tiny_mce_wiris_CAS",
        });
    },

    setDasarPenliaian: function(elm){
        var jenis = $(elm).val();
        //soal
        var tb_jawaban = $('table#tb_jawaban').find('.nilai_bobot');
        if(jenis == '1'){
            $.each(tb_jawaban, function(){
                $(this).val(0);
                $(this).attr('disabled', '');
            });
        }else{
            //jawaban
            $.each(tb_jawaban, function(){
                $(this).removeAttr('disabled');
            });
        }
    },

    addJawaban: function(elm, e){
        e.preventDefault();

        var tr_index = $(elm).closest('tbody').find('tr:last');
        var jenis = $('#peniliaian').val();

        var index = tr_index.index();

        Soal.ajaxSetup();
        $.ajax({
            type: 'POST',
            data: {
                index: index,
                jenis: jenis
            },
            dataType: 'html',
            async: false,
            url: url.base_url_with_slash(Soal.module()) + "addJawaban",
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data...");
            },
            success: function (resp) {
                message.hideLoading();
                var new_tr = tr_index.clone();
                new_tr.html(resp);
                new_tr.removeClass('hide');
                new_tr.removeClass('deleted');
                tr_index.after(new_tr);

                Soal.setTinyMCECustom('#jawaban_'+index);
            }
        });
    },

    setTinyMCECustom: function(elm){
        // console.log($(elm));
        // return;

        tinymce.remove(elm);
        tinymce.init({
            paste_data_images: true,
            selector: ''+elm+'',
            menubar: 'insert',
            theme: 'modern',
            plugins: 'table charmap tiny_mce_wiris image',
            file_picker_types: 'image',
            image_title: true,
            automatic_uploads: true,
            file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            input.onchange = function () {
            var file = this.files[0];
        //     $(this).attr('ondrag', 'membuatl_soal_data.dragImage()');

            var reader = new FileReader();
            reader.onload = function () {
            var id = 'blobid' + (new Date()).getTime();
            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
            var base64 = reader.result.split(',')[1];
            var blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);

            // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), {title: file.name});
            };
                reader.readAsDataURL(file);
            };

                input.click();
            },
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry tiny_mce_wiris_CAS",
        });
    },

    deleteJawaban: function(elm, e){
        e.preventDefault();
        var data_id = $(elm).attr('data_id');

        if (data_id != '') {
         $(elm).closest('tr').addClass('hide');
         $(elm).closest('tr').addClass('deleted');
        } else {
         $(elm).closest('tr').remove();
        }
    }
};


$(function () {
    Soal.setTinyMCE();
});
