var Profile = {
    module: function () {
        return "user/profile";
    },

    ajaxSetup: function () {
        console.log('tes ', $('meta[name="csrf_token"]').attr('content'));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
    },

    cancel: function () {
        window.location.href = url.base_url_with_slash("dashboard") +"?menu=dashboard&child=no";
    },

    getPostInput: function () {
        var data = {
            'profile_id': $('#profile_id').val(),
            'firstname': $('#firstname').val(),
            'lastname': $('#lastname').val(),
            'tentang': $('#tentang').val(),
            'media_id': $('#media_id').val(),
            'facebook': $('#facebook').val(),
            'twitter': $('#twitter').val(),
            'instagram': $('#instagram').val(),
            'wa': $('#wa').val(),
        };

        return data;
    },

    submit: function () {
        if (validation.run()) {
            var data = Profile.getPostInput();
            Profile.ajaxSetup();

            var formData = new FormData();
            formData.append('data', JSON.stringify(data));

            $.ajax({
                type: 'POST',
                dataType: 'json',
                // async: false,
                data: formData,
                processData: false,
                contentType: false,
                url: url.base_url_with_slash(Profile.module()) + 'submit',
                error: function () {
                    toastr.error('Data Gagal Diperbaharui, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Diperbaharui', 'Pesan');
                        var reload = function () {
                            window.location.reload();
                        };

                        setTimeout(reload(), 1000);
                    } else {
                        toastr.error('Data Gagal Diperbaharui', 'Pesan');
                    }
                }
            });
        }
    },

    submitPassword: function () {
        if (validation.run()) {
            Profile.ajaxSetup();

            $.ajax({
                type: 'POST',
                dataType: 'json',
                // async: false,
                data: {
                    password_lama: $('#password_lama').val(),
                    password_baru: $('#password_baru').val(),
                },
                url: url.base_url_with_slash(Profile.module()) + 'submitPassword',
                error: function () {
                    toastr.error('Data Gagal Diperbaharui, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Diperbaharui', 'Pesan');
                        var reload = function () {
                            window.location.reload();
                        };

                        setTimeout(reload(), 1000);
                    } else {
                        toastr.error(resp.message, 'Pesan');
                    }
                }
            });
        }
    },

    gantiPassword: function(elm, e){
        e.preventDefault();
        Profile.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'html',
            // async: false,
            url: url.base_url_with_slash(Profile.module()) + 'gantiPassword',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data...");
            },

            success: function (resp) {
                message.hideLoading();
                bootbox.dialog({
                    message:resp
                });
            }
        });
    },

    gantiFoto: function(elm, e){
        e.preventDefault();
        var uploader = $('<input type="file" accept="image/*" />');
        uploader.click();
        var images = $('#img_profile');

        images.removeClass('change');
        uploader.on('change', function () {
         var reader = new FileReader();
         reader.onload = function (event) {
            //  console.log(images);
             var base64image = event.target.result;
             images.attr('src', base64image);
             images.addClass('change');

             Profile.submitFoto(base64image);
         };

         reader.readAsDataURL(uploader[0].files[0]);
        });
    },

    submitFoto: function(image_src){
        var profile_id = $('#profile_id').val();
        Profile.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data:{
                profile_id: profile_id,
                image_src:image_src
            },
            // async: false,
            url: url.base_url_with_slash(Profile.module()) + 'submitFoto',
            error: function () {
                toastr.error('Data Gagal, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data...");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Diperbaharui', 'Pesan');
                    var reload = function () {
                        window.location.reload();
                    };
                    setTimeout(reload(), 1000);
                } else {
                    toastr.error(resp.message, 'Pesan');
                }
            }
        });

    }
};


$(function () {

});
