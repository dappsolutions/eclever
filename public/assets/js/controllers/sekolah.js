var Sekolah = {
    module: function () {
        return "master/sekolah";
    },

    link_get_module_url: function () {
        return "menu=sekolah&child=master";
    },

    ajaxSetup: function () {
        console.log('tes ', $('meta[name="csrf_token"]').attr('content'));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
    },

    add: function (elm, e) {
        e.preventDefault();
        window.location.href = url.base_url_with_slash(Sekolah.module()) + "add?"+Sekolah.link_get_module_url();
    },

    edit: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        window.location.href = url.base_url_with_slash(Sekolah.module()) + "edit/" + id+"?"+Sekolah.link_get_module_url();
    },

    detail: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        window.location.href = url.base_url_with_slash(Sekolah.module()) + "detail/" + id+"?"+Sekolah.link_get_module_url();
    },

    cancel: function () {
        window.location.href = url.base_url(Sekolah.module()) + Sekolah.link_get_module_url();
    },

    cari: function (elm, e) {
        if (e.keyCode == 13) {
            var keyword = $(elm).val();
            if (keyword == '') {
                window.location.href = url.base_url(Sekolah.module()) + Sekolah.link_get_module_url();
            } else {
                window.location.href = url.base_url_with_slash(Sekolah.module()) + "cari?keyword=" + $.trim(keyword)+"&"+ Sekolah.link_get_module_url();
            }
        }
    },

    hapus: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        var html = '<div class="modal-content bd-0 tx-14">';
        // html += '<div class="modal-header pd-y-20 pd-x-25">';
        // html += '<h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message Preview</h6>';
        // html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
        // html += '<span aria-hidden="true">×</span>';
        // html += '</button>';
        // html += '</div>';
        html += '<div class="modal-body pd-25">';
        html += '<h4 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary">Informasi</a></h4>';
        html += '<p class="mg-b-5">Apa anda yakin akan menghapus data ini ?</p>';
        html += '</div>';
        html += '<div class="text-right">';
        html += '<button type="button" data_id="'+id+'" onclick="Sekolah.delete(this)" class="btn btn-success pd-x-20">Ya</button>&nbsp;';
        html += '<button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Tidak</button>';
        html += '</div>';
        html += '</div>';

        bootbox.dialog({
            message: html,
            size: 'large'
        });
    },

    delete: function(elm){
        var id = $(elm).attr('data_id');
        Sekolah.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {
                id: id
            },
            url: url.base_url_with_slash(Sekolah.module()) + 'delete',
            error: function () {
                toastr.error('Data Gagal Hapus, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Hapus..");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Hapus', 'Pesan');
                    var reload = function () {
                        window.location.reload();
                    };

                    setTimeout(reload(), 1000);
                } else {
                    toastr.error('Data Gagal Hapus', 'Pesan');
                }
            }
        });
    },

    getPostInput: function () {
        var data = {
            'id': $('#id').val(),
            'sekolah': $('#sekolah').val(),
            'email': $('#email').val(),
            'no_telp': $('#no_telp').val(),
            'alamat': $('#alamat').val(),
        };

        return data;
    },

    submit: function () {
        if (validation.run()) {
            var data = Sekolah.getPostInput();
            Sekolah.ajaxSetup();

            var formData = new FormData();
            formData.append('data', JSON.stringify(data));

            $.ajax({
                type: 'POST',
                dataType: 'json',
                // async: false,
                data: formData,
                processData: false,
                contentType: false,
                url: url.base_url_with_slash(Sekolah.module()) + 'submit',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                        var reload = function () {
                            window.location.href = url.base_url_with_slash(Sekolah.module()) + "detail/"+resp.id+"?"+Sekolah.link_get_module_url();
                        };

                        setTimeout(reload(), 1000);
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        }
    },

};


$(function () {

});
