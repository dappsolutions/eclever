-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 18, 2020 at 06:39 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `examination`
--

-- --------------------------------------------------------

--
-- Table structure for table `angkatan`
--

CREATE TABLE `angkatan` (
  `id` int(11) NOT NULL,
  `sekolah` int(11) NOT NULL,
  `nama_angkatan` varchar(255) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `angkatan`
--

INSERT INTO `angkatan` (`id`, `sekolah`, `nama_angkatan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, '2019/2020', '2020-04-10 04:29:59', 1, '2020-04-10 04:31:07', 1, 0),
(2, 1, '2020/2021', '2020-04-10 04:36:23', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bobot_nilai_kategori`
--

CREATE TABLE `bobot_nilai_kategori` (
  `id` int(11) NOT NULL,
  `kategori_soal` int(11) NOT NULL,
  `nilai_benar` int(11) DEFAULT NULL,
  `nilai_salah` int(11) DEFAULT NULL,
  `nilai_kosong` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `day`
--

CREATE TABLE `day` (
  `id` int(11) NOT NULL,
  `hari` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `day`
--

INSERT INTO `day` (`id`, `hari`) VALUES
(1, 'Senin'),
(2, 'selasa'),
(3, 'Rabu'),
(4, 'Kamis'),
(5, 'Jumat'),
(6, 'Sabtu');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id` int(11) NOT NULL,
  `sekolah` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `nip` varchar(150) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id`, `sekolah`, `nama`, `nip`, `alamat`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(10, 1, 'Asrori S.pd', '131803333111', '-', NULL, NULL, '2020-04-10 03:56:31', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `guru_has_mapel`
--

CREATE TABLE `guru_has_mapel` (
  `id` int(11) NOT NULL,
  `guru` int(11) NOT NULL,
  `mata_pelajaran` int(11) NOT NULL,
  `handled` int(11) DEFAULT '0' COMMENT '0 : Tidak Handle\n1 : Handle',
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru_has_mapel`
--

INSERT INTO `guru_has_mapel` (`id`, `guru`, `mata_pelajaran`, `handled`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 10, 6, 1, NULL, NULL, NULL, NULL, 0),
(2, 10, 7, 1, NULL, NULL, NULL, NULL, 0),
(3, 10, 8, 1, NULL, NULL, NULL, NULL, 0),
(4, 10, 9, 0, NULL, NULL, NULL, NULL, 0),
(5, 10, 10, 1, NULL, NULL, NULL, NULL, 0),
(6, 10, 11, 0, NULL, NULL, NULL, NULL, 0),
(7, 10, 12, 0, NULL, NULL, NULL, NULL, 0),
(8, 10, 13, 0, NULL, NULL, NULL, NULL, 0),
(9, 10, 12, 0, NULL, NULL, NULL, NULL, 0),
(10, 10, 13, 0, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `guru_has_user`
--

CREATE TABLE `guru_has_user` (
  `id` int(11) NOT NULL,
  `guru` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hak_akses`
--

CREATE TABLE `hak_akses` (
  `id` int(11) NOT NULL,
  `hak_akses` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hak_akses`
--

INSERT INTO `hak_akses` (`id`, `hak_akses`) VALUES
(1, 'Superadmin'),
(2, 'Administrator'),
(3, 'Sekolah'),
(4, 'Guru'),
(5, 'Pengawas'),
(6, 'Siswa'),
(7, 'Guest');

-- --------------------------------------------------------

--
-- Table structure for table `jam_pelajaran`
--

CREATE TABLE `jam_pelajaran` (
  `id` int(11) NOT NULL,
  `sekolah` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `lama` int(11) DEFAULT NULL,
  `jam_set` varchar(45) DEFAULT NULL,
  `jumlah_jam` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jam_pelajaran`
--

INSERT INTO `jam_pelajaran` (`id`, `sekolah`, `judul`, `lama`, `jam_set`, `jumlah_jam`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'Reguler', 45, '07:00', 4, '2020-04-17 01:39:32', 1, '2020-04-17 07:55:37', 1, 0),
(2, 1, 'Ujian Reguler', 60, '09:45', 3, '2020-04-18 02:52:30', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jam_pelajaran_has_day`
--

CREATE TABLE `jam_pelajaran_has_day` (
  `id` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `jam_pelajaran` int(11) NOT NULL,
  `choose` int(11) DEFAULT '0',
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jam_pelajaran_has_day`
--

INSERT INTO `jam_pelajaran_has_day` (`id`, `day`, `jam_pelajaran`, `choose`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(7, 1, 1, 1, NULL, NULL, NULL, NULL, 0),
(8, 2, 1, 1, NULL, NULL, NULL, NULL, 0),
(9, 3, 1, 1, NULL, NULL, NULL, NULL, 0),
(10, 4, 1, 1, NULL, NULL, NULL, NULL, 0),
(11, 1, 2, 1, NULL, NULL, NULL, NULL, 0),
(12, 2, 2, 1, NULL, NULL, NULL, NULL, 0),
(13, 3, 2, 1, NULL, NULL, NULL, NULL, 0),
(14, 4, 2, 1, NULL, NULL, NULL, NULL, 0),
(15, 6, 2, 1, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id` int(11) NOT NULL,
  `sekolah` int(11) NOT NULL,
  `jurusan` varchar(150) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id`, `sekolah`, `jurusan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'Teknik Komputer dan Jaringan', NULL, NULL, NULL, NULL, 0),
(2, 1, 'Multimedia', NULL, NULL, NULL, NULL, 0),
(3, 1, 'Usaha Perjalanan Wisata', NULL, NULL, NULL, NULL, 0),
(4, 1, 'Pemasaran', NULL, NULL, '2020-04-10 04:11:57', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_soal`
--

CREATE TABLE `kategori_soal` (
  `id` int(11) NOT NULL,
  `sekolah` int(11) NOT NULL,
  `mata_pelajaran` int(11) NOT NULL,
  `kategori` varchar(45) DEFAULT NULL,
  `poin_by` varchar(100) NOT NULL DEFAULT 'SOAL',
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_soal`
--

INSERT INTO `kategori_soal` (`id`, `sekolah`, `mata_pelajaran`, `kategori`, `poin_by`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(22, 1, 10, 'BAB I', 'SOAL', '2020-04-11 01:55:13', 1, '2020-04-11 01:59:22', 1, 0),
(23, 1, 6, 'UMUM', 'SOAL', '2020-04-11 02:21:47', 1, NULL, NULL, 0),
(24, 1, 7, 'UMUM', 'SOAL', '2020-04-11 05:15:33', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_ujian`
--

CREATE TABLE `kategori_ujian` (
  `id` int(11) NOT NULL,
  `kategori_ujian` varchar(150) DEFAULT NULL COMMENT 'Pilihan Ganda\nIsian',
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_ujian`
--

INSERT INTO `kategori_ujian` (`id`, `kategori_ujian`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 'TERBUKA', NULL, NULL, NULL, NULL),
(2, 'TERTUTUP', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id` int(11) NOT NULL,
  `sekolah` int(11) NOT NULL,
  `jurusan` int(11) NOT NULL,
  `nama_kelas` varchar(150) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id`, `sekolah`, `jurusan`, `nama_kelas`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 1, 'X TKJ 1', NULL, NULL, NULL, NULL, 0),
(2, 1, 1, 'X TKJ 2', NULL, NULL, NULL, NULL, 0),
(3, 1, 2, 'X MM 1', NULL, NULL, NULL, NULL, 0),
(4, 1, 2, 'X MM 2', NULL, NULL, '2020-04-09 08:18:44', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `klasifikasi_jam_mapel`
--

CREATE TABLE `klasifikasi_jam_mapel` (
  `id` int(11) NOT NULL,
  `jam_pelajaran` int(11) NOT NULL,
  `jam_awal` varchar(45) DEFAULT NULL,
  `jam_akhir` varchar(45) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `klasifikasi_jam_mapel`
--

INSERT INTO `klasifikasi_jam_mapel` (`id`, `jam_pelajaran`, `jam_awal`, `jam_akhir`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, '08:45', '09:30', NULL, NULL, NULL, NULL, 1),
(2, 1, '09:30', '10:15', NULL, NULL, NULL, NULL, 1),
(3, 1, '10:15', '11:00', NULL, NULL, NULL, NULL, 1),
(4, 1, '11:00', '11:45', NULL, NULL, NULL, NULL, 1),
(5, 1, '07:00', '07:45', NULL, NULL, NULL, NULL, 0),
(6, 1, '07:45', '08:30', NULL, NULL, NULL, NULL, 0),
(7, 1, '08:30', '09:15', NULL, NULL, NULL, NULL, 0),
(8, 1, '09:15', '10:00', NULL, NULL, NULL, NULL, 0),
(9, 2, '09:45', '10:45', NULL, NULL, NULL, NULL, 0),
(10, 2, '10:45', '11:45', NULL, NULL, NULL, NULL, 0),
(11, 2, '11:45', '12:45', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE `mata_pelajaran` (
  `id` int(11) NOT NULL,
  `sekolah` int(11) NOT NULL,
  `mata_pelajaran` varchar(150) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`id`, `sekolah`, `mata_pelajaran`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(6, 1, 'Bahasa Inggris', NULL, NULL, NULL, NULL, 0),
(7, 1, 'Bahasa Indonesia', NULL, NULL, NULL, NULL, 0),
(8, 1, 'Fisika', NULL, NULL, NULL, NULL, 0),
(9, 1, 'Kimia', NULL, NULL, NULL, NULL, 0),
(10, 1, 'Matematika', NULL, NULL, NULL, NULL, 0),
(11, 1, 'IPS', NULL, NULL, NULL, NULL, 0),
(12, 1, 'Bahasa Jepang', NULL, NULL, NULL, NULL, 0),
(13, 1, 'Bahasa Jawa', NULL, NULL, '2020-04-10 04:19:45', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nama_menu` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `module` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan_nilai`
--

CREATE TABLE `pengaturan_nilai` (
  `id` int(11) NOT NULL,
  `show` int(11) DEFAULT '0',
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengawas_ujian`
--

CREATE TABLE `pengawas_ujian` (
  `id` int(11) NOT NULL,
  `guru` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penilaian`
--

CREATE TABLE `penilaian` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penilaian`
--

INSERT INTO `penilaian` (`id`, `keterangan`) VALUES
(1, 'SOAL'),
(2, 'JAWABAN');

-- --------------------------------------------------------

--
-- Table structure for table `peserta_ujian`
--

CREATE TABLE `peserta_ujian` (
  `id` int(11) NOT NULL,
  `ujian` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `nilai` double DEFAULT '0',
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `nama_role` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `nama_role`) VALUES
(1, 'tambah'),
(2, 'ubah'),
(3, 'hapus');

-- --------------------------------------------------------

--
-- Table structure for table `role_access`
--

CREATE TABLE `role_access` (
  `id` int(11) NOT NULL,
  `guru` int(11) NOT NULL DEFAULT '0',
  `module` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role_has_menu`
--

CREATE TABLE `role_has_menu` (
  `id` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `menu` int(11) NOT NULL,
  `hak_akses` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sekolah`
--

CREATE TABLE `sekolah` (
  `id` int(11) NOT NULL,
  `nama_sekolah` varchar(150) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sekolah`
--

INSERT INTO `sekolah` (`id`, `nama_sekolah`, `alamat`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'SMK NEGERI 2 KOTA BLITAR', 'Jl. Tanjung', '2020-04-08 00:00:00', 1, '2020-04-09 08:16:23', 1, 0),
(2, 'SMK NEGERI 1 KOTA BLITAR', 'Jl. Bali', NULL, NULL, NULL, NULL, 0),
(3, 'SMK NEGERI 3 KOTA BLITAR', 'Jl. TMP', '2020-04-10 03:49:56', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sekolah_has_user`
--

CREATE TABLE `sekolah_has_user` (
  `id` int(11) NOT NULL,
  `sekolah` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `sekolah` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `nis` varchar(45) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `sekolah`, `nama`, `nis`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'Dodik Rismawan Affrudin', '1318033', NULL, NULL, NULL, NULL, 0),
(2, 1, 'Eko Kurniawan', '1318032', NULL, NULL, NULL, NULL, 0),
(3, 1, 'Erwandianto', '1318014', NULL, NULL, NULL, NULL, 0),
(5, 1, 'Beni Maha', '121881', '2020-04-09 08:14:47', 1, '2020-04-10 04:01:35', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `siswa_has_angkatan`
--

CREATE TABLE `siswa_has_angkatan` (
  `id` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `angkatan` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa_has_angkatan`
--

INSERT INTO `siswa_has_angkatan` (`id`, `siswa`, `angkatan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 5, 1, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `siswa_has_jawaban`
--

CREATE TABLE `siswa_has_jawaban` (
  `id` int(11) NOT NULL,
  `ujian` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `soal` int(11) NOT NULL,
  `soal_has_jawaban` int(11) NOT NULL,
  `status_jawaban` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `siswa_has_kategori_random_soal`
--

CREATE TABLE `siswa_has_kategori_random_soal` (
  `id` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `ujian` int(11) NOT NULL,
  `kategori_soal` int(11) NOT NULL,
  `soal` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `siswa_has_kelas`
--

CREATE TABLE `siswa_has_kelas` (
  `id` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `kelas` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa_has_kelas`
--

INSERT INTO `siswa_has_kelas` (`id`, `siswa`, `kelas`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(12, 5, 3, NULL, NULL, NULL, NULL, 1),
(13, 5, 1, NULL, NULL, NULL, NULL, 0),
(14, 1, 1, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `siswa_has_ujian`
--

CREATE TABLE `siswa_has_ujian` (
  `id` int(11) NOT NULL,
  `ujian` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `nilai` double DEFAULT NULL,
  `jam_ujian` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa_has_ujian`
--

INSERT INTO `siswa_has_ujian` (`id`, `ujian`, `siswa`, `nilai`, `jam_ujian`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(124, 28, 1, NULL, NULL, '2020-04-14 04:55:34', 1, NULL, NULL, 0),
(125, 28, 5, NULL, NULL, '2020-04-14 06:06:53', 1, NULL, NULL, 1),
(126, 28, 5, NULL, NULL, '2020-04-14 06:18:06', 1, NULL, NULL, 1),
(127, 28, 5, NULL, NULL, '2020-04-14 07:12:55', 1, NULL, NULL, 0),
(128, 29, 1, NULL, 1, '2020-04-18 04:14:55', 1, NULL, NULL, 0),
(129, 29, 5, NULL, 1, '2020-04-18 04:14:55', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `siswa_has_user`
--

CREATE TABLE `siswa_has_user` (
  `id` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `siswa_ujian_time`
--

CREATE TABLE `siswa_ujian_time` (
  `id` int(11) NOT NULL,
  `siswa_has_ujian` int(11) NOT NULL,
  `sisa_waktu` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE `soal` (
  `id` int(11) NOT NULL,
  `sekolah` int(11) NOT NULL,
  `mata_pelajaran` int(11) NOT NULL,
  `kategori_soal` int(11) NOT NULL,
  `soal` longtext,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal`
--

INSERT INTO `soal` (`id`, `sekolah`, `mata_pelajaran`, `kategori_soal`, `soal`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(133, 1, 7, 24, '<p>Apa ibu kota jawa timur ?</p>', '2020-04-11 05:18:56', 1, NULL, NULL, 0),
(134, 1, 7, 24, '<p>Apa ibu kota indonesia ?</p>', '2020-04-12 00:02:16', 1, '2020-04-12 00:16:49', 1, 0),
(135, 1, 6, 23, '<p>You... eating noodle</p>', '2020-04-13 12:48:32', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `soal_has_jawaban`
--

CREATE TABLE `soal_has_jawaban` (
  `id` int(11) NOT NULL,
  `soal` int(11) NOT NULL,
  `jawaban` longtext,
  `true_or_false` int(11) DEFAULT NULL COMMENT 'Keterangan Jawaban Benar atau Salah\n0: salah\n1: benar',
  `poin` int(11) NOT NULL DEFAULT '0',
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_has_jawaban`
--

INSERT INTO `soal_has_jawaban` (`id`, `soal`, `jawaban`, `true_or_false`, `poin`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(752, 133, '<p>Surabaya</p>', 1, 0, NULL, NULL, NULL, NULL, 0),
(753, 133, '<p>Malang</p>', 0, 0, NULL, NULL, NULL, NULL, 0),
(754, 133, '<p>Blitar</p>', 0, 0, NULL, NULL, NULL, NULL, 0),
(755, 133, '<p>Mojokerto</p>', 0, 0, NULL, NULL, NULL, NULL, 0),
(756, 134, '<p>Jakarta</p>', 1, 0, '2020-04-12 00:02:16', 1, '2020-04-12 00:16:49', 1, 0),
(757, 134, '<p>Malang</p>', 0, 0, '2020-04-12 00:02:16', 1, '2020-04-12 00:16:49', 1, 0),
(758, 134, '<p>Surabaya</p>', 0, 0, '2020-04-12 00:02:16', 1, '2020-04-12 00:16:49', 1, 0),
(759, 134, '<p>Blitar</p>', 0, 0, '2020-04-12 00:02:16', 1, '2020-04-12 00:16:49', 1, 1),
(760, 134, '<p>Solo</p>', 0, 0, '2020-04-12 00:16:49', 1, NULL, NULL, 0),
(761, 135, '<p>is</p>', 0, 0, '2020-04-13 12:48:32', 1, NULL, NULL, 0),
(762, 135, '<p>are</p>', 1, 0, '2020-04-13 12:48:32', 1, NULL, NULL, 0),
(763, 135, '<p>was</p>', 0, 0, '2020-04-13 12:48:32', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `soal_penilaian`
--

CREATE TABLE `soal_penilaian` (
  `id` int(11) NOT NULL,
  `soal` int(11) NOT NULL,
  `penilaian` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_penilaian`
--

INSERT INTO `soal_penilaian` (`id`, `soal`, `penilaian`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 133, 1, NULL, NULL, NULL, NULL, 0),
(2, 134, 1, '2020-04-12 00:02:16', 1, '2020-04-12 00:16:49', 1, 0),
(3, 135, 1, '2020-04-13 12:48:32', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `start_ujian`
--

CREATE TABLE `start_ujian` (
  `id` int(11) NOT NULL,
  `time_limit` int(11) NOT NULL,
  `ujian` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL COMMENT 'Status Untuk memulai ujian atau menghentikan ujian\n0 : Ujian Stop\n1 : Ujian Dimulai',
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_jawaban`
--

CREATE TABLE `status_jawaban` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'R : Ragu Ragu\nY : Yakin'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `time_limit`
--

CREATE TABLE `time_limit` (
  `id` int(11) NOT NULL,
  `sekolah` int(11) NOT NULL,
  `time_limit` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time_limit`
--

INSERT INTO `time_limit` (`id`, `sekolah`, `time_limit`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(13, 1, 30, '2020-04-12 03:41:57', 1, '2020-04-12 03:43:33', 1, 0),
(14, 1, 90, '2020-04-13 13:11:58', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `type_soal`
--

CREATE TABLE `type_soal` (
  `id` int(11) NOT NULL,
  `type` varchar(45) DEFAULT NULL COMMENT 'L: Listeining\nS: Standard'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ujian`
--

CREATE TABLE `ujian` (
  `id` int(11) NOT NULL,
  `sekolah` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `kode_ujian` varchar(45) DEFAULT NULL,
  `judul` varchar(150) DEFAULT NULL,
  `mata_pelajaran` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `kategori_ujian` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ujian`
--

INSERT INTO `ujian` (`id`, `sekolah`, `users`, `kode_ujian`, `judul`, `mata_pelajaran`, `token`, `kategori_ujian`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(26, 1, 1, 'U13042020001', 'Bahasa Indonesi BAB I', 7, 'om016', 1, '2020-04-13 06:21:10', 1, '2020-04-13 06:27:44', 1, 0),
(27, 1, 1, 'U13042020002', 'Tebak Kata', 6, 'y0d5z', 1, '2020-04-13 12:46:04', 1, '2020-04-13 12:46:27', 1, 0),
(28, 1, 1, 'U14042020001', 'Bahasa Indonesia 2', 7, 'u1bkw', 1, '2020-04-14 02:21:28', 1, NULL, NULL, 0),
(29, 1, 1, 'U15042020001', 'Bahasa Indonesia 3', 7, 'tfmhi', 2, '2020-04-15 03:54:09', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ujian_has_pengawas`
--

CREATE TABLE `ujian_has_pengawas` (
  `id` int(11) NOT NULL,
  `pengawas_ujian` int(11) NOT NULL,
  `ujian` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ujian_has_soal`
--

CREATE TABLE `ujian_has_soal` (
  `id` int(11) NOT NULL,
  `ujian` int(11) NOT NULL,
  `soal` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ujian_has_soal`
--

INSERT INTO `ujian_has_soal` (`id`, `ujian`, `soal`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(116, 26, 133, '2020-04-13 07:17:38', 1, NULL, NULL, 1),
(121, 26, 134, '2020-04-13 07:47:53', 1, NULL, NULL, 1),
(122, 26, 133, '2020-04-13 08:25:59', 1, NULL, NULL, 1),
(123, 26, 134, '2020-04-13 08:26:01', 1, NULL, NULL, 1),
(124, 26, 134, '2020-04-13 08:26:07', 1, NULL, NULL, 0),
(125, 26, 133, '2020-04-13 08:26:09', 1, NULL, NULL, 0),
(126, 27, 135, '2020-04-13 12:49:06', 1, NULL, NULL, 0),
(127, 28, 133, '2020-04-14 02:32:23', 1, NULL, NULL, 0),
(128, 28, 134, '2020-04-14 02:32:26', 1, NULL, NULL, 0),
(131, 29, 133, '2020-04-15 03:57:03', 1, NULL, NULL, 0),
(132, 29, 134, '2020-04-15 03:57:49', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ujian_has_soal_limit_keluar`
--

CREATE TABLE `ujian_has_soal_limit_keluar` (
  `id` int(11) NOT NULL,
  `ujian` int(11) NOT NULL,
  `kategori_soal` int(11) NOT NULL,
  `limit_soal_keluar` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ujian_poin_lulus`
--

CREATE TABLE `ujian_poin_lulus` (
  `id` int(11) NOT NULL,
  `ujian` int(11) DEFAULT NULL,
  `poin` int(11) DEFAULT '0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ujian_status`
--

CREATE TABLE `ujian_status` (
  `id` int(11) NOT NULL,
  `ujian` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'DRAFT\nDONE',
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ujian_status`
--

INSERT INTO `ujian_status` (`id`, `ujian`, `status`, `createddate`, `createdby`) VALUES
(1, 26, 'DRAFT', NULL, NULL),
(2, 27, 'DRAFT', NULL, NULL),
(3, 28, 'DRAFT', NULL, NULL),
(4, 29, 'DRAFT', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ujian_time`
--

CREATE TABLE `ujian_time` (
  `id` int(11) NOT NULL,
  `ujian` int(11) NOT NULL,
  `tanggal_ujian` date DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL,
  `time_limit` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ujian_time`
--

INSERT INTO `ujian_time` (`id`, `ujian`, `tanggal_ujian`, `tanggal_akhir`, `time_limit`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 28, '2020-04-18', '2020-05-01', 14, '2020-04-18 02:48:58', 1, NULL, NULL, 0),
(2, 27, '2020-04-18', '2020-05-05', 14, '2020-04-18 02:49:34', 1, NULL, NULL, 0),
(3, 26, '2020-04-18', '2020-05-06', 13, '2020-04-18 02:49:52', 1, NULL, NULL, 0),
(4, 29, '2020-04-18', '2020-05-14', 14, '2020-04-18 04:29:19', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ujian_time_jadwal`
--

CREATE TABLE `ujian_time_jadwal` (
  `id` int(11) NOT NULL,
  `ujian` int(11) NOT NULL,
  `jam_pelajaran` int(11) NOT NULL,
  `klasifikasi_jam_mapel` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ujian_time_jadwal`
--

INSERT INTO `ujian_time_jadwal` (`id`, `ujian`, `jam_pelajaran`, `klasifikasi_jam_mapel`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 29, 1, 5, NULL, NULL, NULL, NULL, 0),
(2, 29, 1, 6, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `hak_akses` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `hak_akses`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'superadmin', 'superadmin', 1, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_has_menu`
--

CREATE TABLE `user_has_menu` (
  `id` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `menu` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_has_profile`
--

CREATE TABLE `user_has_profile` (
  `id` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `foto` longtext,
  `tentang` varchar(255) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_has_profile`
--

INSERT INTO `user_has_profile` (`id`, `users`, `firstname`, `lastname`, `foto`, `tentang`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(4, 1, 'Dodik', 'Rismawan', 'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAAeAAD/4QMZaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjRENDAyQ0M4OEM0NDExRTc4NDRDODFENDg1OUNEMDEwIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjRENDAyQ0M3OEM0NDExRTc4NDRDODFENDg1OUNEMDEwIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgV2luZG93cyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJDMDE0MEVDRUVBQjE1NzhDQTEwMzAxMzE3NTQ5NkJENCIgc3RSZWY6ZG9jdW1lbnRJRD0iQzAxNDBFQ0VFQUIxNTc4Q0ExMDMwMTMxNzU0OTZCRDQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7QBIUGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAA8cAVoAAxslRxwCAAACAAIAOEJJTQQlAAAAAAAQ/OEfici3yXgvNGI0B1h36//uAA5BZG9iZQBkwAAAAAH/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoXHh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoaJjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/AABEIAfQB9AMBIgACEQEDEQH/xACRAAACAwEBAQAAAAAAAAAAAAACAwABBAUGBwEBAQEBAQAAAAAAAAAAAAAAAAECAwQQAAEEAQMCBQIFAgUDAwQDAAEAEQIDITESBEFRYXEiEwWBMpGhsUIU0QbB4VJiI/DxcjMkFYJDNCWSolMRAQEBAAEEAgICAwEBAAAAAAABEQIhMRIDQVFhcYEykSJCwVL/2gAMAwEAAhEDEQA/AOLYkSWiwJElK1AhEEKILFdIJUrAVsopZQhMkEDKxKIJkSlBGFUNfCpR1EEKXJGUJCgXJAmSCBlYlXFaays0dVogtRmtMCmxKTAp0VpBqgHVo4AMstaXtQmK0CI7Jc4pIWlwxHwTBEnPToVKoHQ6FNjUQcEreMaX7JklTiI+kF5dWWwUGWpPkoaoxGAria5U4WmRLsPNLlWernzW+dTyS5V9CpYsrFtUZPlFilkLNjUqoq+im0+XRQnooqilyTDolyRSzqrgqIP3NjR0UNVBpqC1QjhZqVrjoqBmAyTILRJImtcWeTPZokyKbaVnkV0cgyQIiQgfKzSLQyRoZJAsqkRQrbKwrCoIggsIghCIICCtUFaKooCjKAoBKAoygKASqVlUoC/aoq/aog69iRILRYkTXKu0LRgIQEcVzrcGAr2q4hHtUUmQSyFomEohIUtFEqiFYWkMGiioKILVFWoVAshCyYQhZUoQnQSimQWoxWmCdBIgU6BZaZO6I68jxCAAkKRMhJZaOQSDpgmGzHPcJZ5EYyxH8VYlp0a9sXOnRFAMkxtMi8kyMzqtMz8nwI+quUNwS6yRIFn81qEoEF4sOrafmrKYwyrz4pRqJPlqurtpj6tsm6SOc+cVnsNbnbiXWYKa1jk3Qzoke25YfiulbAv6wJeIbQ9VjlW0m6Dqs2rI1cj4yqFG+m0GYAMon9z/ALon/BZeT8ZyKKo3kxlXMO4K0ym1QiRucgES/cAf29kZJtrFEgRCZJqBP2t4qdDq5DGPmMhXIC+MpxaNkQ8ojAI7hdCfFhMRBO0vtkw+rp9PxHFAlcLpPDUgekS8QVFcS0EV119YgybzVx414pjyDAimRIjM6FtWXUs+PjKZNMo27XaIBfHgVlt90cOFRidlM5E9W3pnc0qorXFxqsUAYyx9PFbRGQkxB9IDhsoqHwSLE+eC2vks1sleLPJmtks0pJtpWaRXWOVQyVxSwmRUpBhVJWFDoopRCFHJCtRlAiCFEFUEFYVBWFFEFapWqKKAo0JQAUBRlCVABVIihQX+1RTooortzhhInArpTrDOs8qlix0lYtpRxCf7KuNfcLnY3KGAwjZFsZRlnF0ucUiQ1WiSRJIFFUFCotAwVFQUQGFaEFEoKIQEIyoIuVSgEJSOAtdHEMmdM43H3dNV2eJwtCy6ceLjy5MNXx27otcPjG6Ls0cQADC0jjDst9GOrhD4/wAFf8EDou6OMOyh4o7KYu156zhyAwsN3GkNB5L1UuKD0WK/hAuGTF15sWH7ZYITq/cZ87e4ytXK4DSLDVLhxK6/VITLasRH8ApZiy6IOBiyMv8AbE5/BULowLy6dCinVx5RcVe02XJyUB9uDGUgWH7tR+qnljcgqrp5YgVkPEyLN3AT2aG+IMwcSjoCPrlZ75QNcfblIScEuwh5BsrNfypVyjISd3EgCcf4LFrTTLZAjcHgR6Rr9EMxxpwJBG85iRhj2ZYLeRIjcAGOoGrhI/kPJwfT18E0aZzsuIhIuYEsNMKzdIxgYE7YyyT3/oskOWZT3RBId4jrIjTHZXE3Tmay0rCcRBdn8tElph911m4CWDJmb9U+PJasEl5SLQBfXrJY76OUbg1U5bBmQD5P6BNFXM2iz2Z+3gRk2D4BNMPpkaZ+85MonIdnJ8uiP3omdplAThYB7kTq3Ug+Czwhyb7YUioxlnbEtEecjJaxZx6JxhSTK4Zss1BPUN2WpWbCf4J3GEXESwBlqxzp5LbKzjcSMZ2j3LX21Vvp4yKVO+QnMn959Uzr5BZpylyeVU8hsrO2vbk7j1HclSFaedxWEZ0DFoEi53SMjqA2iwyplXAG6OHIrh1lL+gXfnbXx4xoq9dkR6y/q3Hp/mufyJ1zEogPOOBZoAT/AKX0ZU+HBvrnu2n7zkxGo8+yxzXS5VQqPtjUvvlp9AsM65AxJDxPZdJXPlCAjitNvx/If/hrNkYR3SnEftOQSs4Daad1KkGFZCgVkKKVIISmEISCtRmhZWFe09lYieyogRKmPZXtl2RUdR1Nk+ysVTQUShJTPYmVY4symoQUJWscKZVjgS6oMJVLoj46R6Ix8aeyg5fRRdb/AONwzKIrrSi8UArBRykwA6lHWAljWlCn8kXsBa4VOE32XWLFlcyVTJMosunfV17arBdFisWNSsk1nlqVpsCzyGVnGySqRSCFUEFFAogIIhogCMIInUVuB4pLFdDi1uAwV4zqzyrocHjAsWXd4tAACw8CtgMLtceGi7fDh8m1Uhk4VhHGIARLNq4X7YVGsJqhTRnlWHWe6oAOfxWyZ25Onfsst18dkjYRtA10j+KsHM5FUZbR0LyOOy89yuRGF8xuBA9THQdGXQ+Q+VjRDZEgylAiDkvF9CW1XmJCcyQ+6X3S8Ssc+Xw6cOPy6nHvF1gEazul2bDdXKvkSjM7RE7tBuP9FmruhGuMtDEMxD5HRkNvIpsBkItINphYbIs5RpkYTDiJcxJ/RDyeRCdQMQxBdwfyKXbZMAvCUh4516R3Mrr4nuVmU4y7CA9L+cmYN+KAfeBgY/XyWWdgjJ5HBzgYCdbXYTtECwxjAf8AUpcONLeBYRVlnkXP+P4K4jXxrePCqcp1yMi3tyMtu0vmRAGcaArUflaqq5V8OoUxkfvybJf7pSPfsuVyrYkiNZ8AfBUTulGMdI4CDqR5szCUImTSLmUpSkH646oK+RChzWN9pwSTJvJtwWQXyhARgcDXuT4qvcj7u6YMtuTAFhgP6j/gqNtvPsBNcZ78NGZwI/6tsfyR8O81SFhnunHOwZ06nx7Ln1G3kWxmREbnMsMI9WPgm2cmmgTFU/cz6pjSR7P1+iDXdzPcmPc1cmYBMjno5TqrL4PZQHk22IDO3+0LhxNomZzIiZFs+PVbIGYFmx9509TEHsoOlXyZUkygAJyeNhB9fi2MK6+RKUCa4RBxqDIgfVZeJ79kRXEyjbLB26+Oequq6XGuj+4gt6tZSHcJOgO7h22yEwB68kDDRGsj2CkOJZYNlUd8tAQPSAO/ittZ5UiZRrnvsyGiz+e5h9FPZ50XkbWj+44ABPRlryTxXdCPHrEYSO+ysCwnDFtq5lXxkPbkCXnFjDbkSj1K0XCwWAzursrkfUYS6AaJ1HJrgYOwi5BjqdMYV8tZ8WAcE/RGOD4Lp0SquO0YkNYrSOMOim4ni4f8HwU/g+C7n8YKfxgr5Hi4X8Edlf8AC8F2/wCMOyn8YJ5ni4o4XgiHC8F2Rxx2VjjhTzXxcgcPwTBw/BdYccdkYoHZPJPFyRwh2TI8Idl1RQOyIVBPI8XMHDHZGOGOy6XtBEKwr5Hi5w4Y7Ihwx2XQEAiEAp5GOf8AxB2UXQ2BRNXHk43uf1K38eRk2MJUOCxGF0uLxwGDLrsY6m1RcJ4h4JsKg2ib7Y7LFqxgvqcea5vIoku7ZW4JWa2kHopcWa8/PjTOEs8OZK7w4w7K/wCMOyz0a2vOy4EnQ/wJdl6Q8UdkB4w7J0NrgR4BHRGOD4Lt/wAYdkQ4w7KLtcWPA8EwcDGi7I447IhQOyDijgB9F0eNxRGIDLV7A7J1cAFZUs07jViLLpU4ZYaiy1VzZb1ixuiUTrNG0MiNoTA0lBORbo6UbUmy98jph06IK3kciBO2AsiNYjEvJcT5H5Xi8icePISoAO67c9cgB0EmZdKfMhCM5yIJgDJh4DBXk+XzJW2ESewSGO+S5ypyuNceOsfPtErjZEvXJ416AxA7t1WOMgBu6hHyIy3x2xMX9THq5SCct2XN1aJHG5gf9uuo1QGW2BLgRl0w4KUJS0B8n7FBOzOrRGPD6INdfOsqeUfvIZ9Zf/y1Sr+TcQbJ2yMtXJOp7DRZ42SOIvGB8clJ5MvSeqqB/kTnIyJaMdAmUWHcZdQD/QLMGiC+Q3TzRiYFfpDSnhhqw8UFuPcMiXbLdkUZASbvgqU0StcjqegdbY8KioGNs99hB3dG8Ac/VBnjMfaMyAP490PuA1lsmWH8AtB4hPqNorq7VsD4Zmn1cb46NZMxKcx9pMjsz0YAOVRi9UKDCJ9Vp3WSHQeaQZWH7RvEMys6RXQt4UeTN6pGFcfSDIiI/A5SbKa6a/ZFzQcloh36GZfuiFUmEgDbLb1OHLLTCdZcSI9OH1JH0ZI/j0Sqxac947T+RUr4xMwISEjXr49glV0qbDVXEQkQTrLQ5yh43PNFkpRltPUslSkIxjAFhA6jqe58kiUIAmI9Uzky8T2U+B3a+fx7Ii2xhKOkpbmH1KA8346UzK2VlktchoD88rkHj8gGJjHc4cl3Yd/BIsmKwASJSL5Yt9FFdXkcnhSO6k2Tl+2EoxjWPJiSs0rJPEsAxyAsQumw246OMI67ABuOW1VR0+HyfavjMyYOHP8AVelrnEkxBd8jyK8bGYMQepOV1ONzpQjCJl6oHBfLFKY9DEAk+avYFzq+a2/P2tL6Fa/eJDvhZ1DjAKtoSfcKr3Cmh+0KCISd5ViRTQ8CKP0sswKIEq6h+6Km+KQShMk0xo92Kr3orKZKAppjV76v31ldXuTTGn3ios7qJpgxQOyfVWyaIIoxCug4RR7UIICLeFdQExhIlB0+UgUskKaF7ArEFZsiOiA3xCKsxCHaEJ5MEP8AIj0CaGbAr2hK/kKe+e6mhu1Rkk8goDyT3TYNLKdVjPIPdDLkS7qaroCxkQ5LdVyjyJd0J5Eu6vkmOwOY2qh5wXG98oTdJXzp4uxLmv1We3mVwAM5N3PgsVVwzu0OH7Lm8u4SkXlvhox/onlScW/n86g1EVScyDA6R/xdcaywnZP0tEs0C0vqCkGyvcDCBA65KA2GW6IwH08FG5MM5MiJOzSgdddchY7LjKe4uS75/NGZHbqx6/0SbADnR9SFSniUTB3fsRqFlsc47dlIzMD3H5KOxd0RcWiASWbCCQEycOGdMJE4u2Rn6JcpHYYjAGWCBJrOpP1Ib8E+mr7X9TaRHXz7BIiPVuPkE0ylGBlEkFm+pQOPKAnti22IYkaP4eCkZSjDfY2T6XyfNZqIgyAPdyP6pvIt3ReJZiIv1ACCzbKRE7cyB/46ycjxZaRGRDH0sPu/VZ6APfMiHZpP00WiFw3Hd9sckdPqgKUxFoAkyEWiB2PX6pcoQE/bs9UgzsRg9vopC7FlpJFlmRIftj4eazSmBZ6cMPNUawOODvOZDLB/JDdcayKI4IzPwJ0isot9t5TyI9O5+iULZEmR1OSfGSg2RlKZYZPcp0Ies5y7v2SePKIgSdSAEyExg6k5I8lRoq5BpI2hox1BOSD3V/yt8pCUBIEuMMR5LNZIwMZRcg/gq3zhaTkmQwO4ZRWz2+LYXjOUdukZ+knyIeKCcaYRIMp5xgB/rofwWeFoMWMQJdw4LozybKww22VkZEtR4ZyExFiNAiI0WiyzxcADzIRUSjDcZESk/TT8Ug8mEyYmuI3ZaX2ptNgjGTQjDDdD+CmK20W7gCdZRMR5BdqskRjuxgary1Fs5XBi5GSvQ18iMxDb1Z1Kla3VBALNzkH0gowoiwjCoBGAggVq2UQUUJREISEAqMiZQBBQCJlFFRFFfRRBpjMlMiSlVhPiEFh0TKAK1UUyCQTEMgistjrNMla7AstgUCCcqxJCdVMhQOiURISQUSKsoCi1VEIFl0JJTSEBCBRJVOiIVMgpQO7AOT0VsrEjH/PKB8zWawIEOBkFh+YXF5kq4EgNuBc9Q3mtHNJAlKVnjtAb8FyrrRIMZEgaEnIWlgLZSlLdEDZ0I9X0SzKUXJz4IDLa+ufBUJg4L+B/qguMzLHU91DIjCH9pAy6VIy03ZHQqoPeIlpRxLqyEkAeBQxFswwcohTb1GqAYnqCxCqYwJR8XCL2pRychGajIeg51bRDCIxLgB+6KT7B56JkISEmOD2OCrNeW07HohhdZIjKX7iggHeJDuQm7Gjox6hCRtB79D5IHVemBGpJyPEIXHtyH1kVb4fTrjyQOYxLfcc99CooZksMa5I/JKd5gnA6lOMJSaf3mYwdWPilSEYEZfqW/qqhU5mR2DQFFH1TjEaDLoD6fPqUyoBok4bTxVRtoAlEAOHdyewyhrk8SRq7HyRWERrJ+2VrARGgg2fxKXXIHcWYCOP0UU73HrlEHAILHr0LIBdIfbJiBjxHVA4BkBoYk+WiXIlxLT+qBhtjZJjiQx4l0ULCSQS5Zonp5FIIBLx+5sIYC4TBId/tOoVQdlZ3Fgx1ER/g6ZWbIxAyPNV6rqjjMP8AD+iOqO3bHUHp4dVKp/CGDMAkk7foV0eHbKJshtf1avp+CxQNldRrgRGL5I1ITaZShIxn6Sz7R0fQKK73HaeGxEv5rS3RY+BCQqjOeAftjr+K2xbzURYRhCiCiLVK1SCKirQlBFFRKp0BK0IKt0FqIXUQaq1ois1ZT4lWBqioFWqLQyVoZlAixZbFpmVnnlQIIQsmkIWUAhEqVqC1FFGRQlCUwhCQqFEIWTTFDtQLZBZMQi51OB5pxC53yF0TMVDWA3S8kgwfIciQltJycMTj8VgMQ+6RaXaKPlWxEiw/DVIFu7OcanGFsGXOAD45/UoH2k+oP2BVSIIeRaI0iNSgOfBA6MvV/qienVOFW/ADv0bL+aXxaDOXU+PRdzi8NhkeQWeXLG+PHXOq+OkRum7dginxyMNjpquyaG6OQjHCMxkHPcYWPNvwjz8uKRliyA1kHOvRl6ev48HDeb5VW/DRlJ4xx/pVnMvB52NbhyT4+H4op8UkuPt/NdqXxUx/6bsdQcS+vQo4/GyLxbbLxGCPEd1fJPF5+fHbUY6nrnR0sVHLjC9Hb8ZOEMxEgRny81jPBBgWfGASOiuni4s4YIH0QSj2BYjotttQEc/dE56JUqjjGSyazYyV7m26dShlCOd410Pb6LZVQTYQ+GIbxS76xEkAgEdBqrqY59g9TdeqbRWZkA6DVEK5WERiHPfo3mik0IejTSJ6n/cqzir7fcsIj9owPEBVE7a5HuwCVEHUo5nMaxqNfNATtu/8A/1VHMQhMnw+ufIaBEXwAWABQSoncOuCQVphCuREx6ZDOHDrNCEhJyNIl+gTY2YYSZvwVQb21SJnFtzgnXBRV7CRrF/+mCo2yMWkTtIcnuqr2l5ylg9NFFbIWATiw2wgHkNdOibxYe49s5E22SWWIgIjdMHcdI5Yea1UGg2bidkGwAc/VQdyNuI1VAAYi/6rTHpjyXN41kKxKc575aDpEDtEro8aMvbEyBEzyBrhQNET5Dup1Uy+S5ViKiIqRCKhigFUSi2qGBQLKFym7FRrKuAA6JWKyi9sphoFEz2yomGihJOjJZwjElBpEkW5ZxNEJqhxmlzkh3IJSQVIpZREoVAJihMUwBUQilsqZGyjIgWVsiEVbIoNqram7VW1AraqMU7aq2oEmK4HycPbumB6pyDk9fJeisIhCUj0HVeT5szO2TFw7urBikGxZr0Eck+aXOYiAIjPQIpnb5nU/wCCTklz9StAoRJeRP1KbVX7ksad0ADs/wCC6nxvG9yyL4Clqya3fH8KOwSbHguvVx9MfRN4/HjCAAC1xrYeei42213nSE1cZzkLVDjAaJtUGDfmU8R0ZMNZxx49k6FI/wC6ZtRxACsiWlmiEtQglxYBpAZfAWlVrqqzrFZCJcEFv9LLDfw4EmcYxAbU/wCC7JiDjVLPHgepie4Ksi68fzeGIkyA9JwSM47LAaYgmIJxkHsOy9bzPixMEQLEl3bP1XKt+OlC5jtO0Z1d/wD6tEtxclcU0yfcI6DJ6F0myqQmfTr2XoJcIxBjtwQT0x5d0uHABOyb7GxuDFTyPF5yyETHZCLj9xz6vDyWScS+Rk9s4XpeR8WCQISLOXYMA3VYeRwNpc+k/itTlGbwrjiBiNxD9ghjGFbzsG4y0jo/n4Lo2cQguHmfJgs06S5Njv8A6o9PotSsXiySlOyZsnkyLlgw8sdFN5CZKqY+3I7v/ggjX1P4qxmxdc2BBJEpsNHxqmBnBaUh44VCADeo/Rj/AEKOIlHR37yDD8VRTTkfU4A1CbWCS4AER37DzQHYADZN/wDbHX+gUF0ZEAA7BnaMOVBorHuEzkQI6RHVu600ezHO0kDwcyPZZRfLSLR8sun1VzlJ529HwdAitlXIIt9/kwBZhCP7IDphd7h8mN1YlP0+J1PkvPVjArqDAl5SJzIrp0G2AEy0px9MJk+keSyWOvCO+bM3mm+2Hwl8SJjWN83fLMXz3WnyCsZpYrU9tNUwriFe2FPbCYomAPbVbEzCmEQvYr2JmFAyoDYomYUQYmKmVp9pQUrGLrPlEAU/2kQqVwIYshMStXtBX7YTBj9sq/bK0+2r2BTBm9tUa1q2hVsCYM3tKe0tO0KbQmDOKlftp+0K9quDP7antrRtCm1MCPbU9pP2q9qYOb8jEw4szEPJsLxvIMIyJ1A0j/uXu/kItw7pAZECx+i+fXuSACkWM0zKWp11ZUGH/WVZDlgW7qCOWiPr0VU/jw3yA2k+RZeq+H40IxxEv4/5Lz3Cq3SDfizr1nxtTRi+OwwufKunGOhGGfAJ8a3I/RBEDCfAZWWxiJBTAg6qxjyVQfiiDNlCGIZEHAQFpn8kDkv0RdB1ZUc40RFDsmRA6oYgaBMZhhaiUEqwSsPJ4olIz6j/AKK6XmkXVmRbus8l43q5Y4sdzx1P1b8VZ45Dl8nw7LeKWPh2VTh26rON+Tk2Uncx/RZbeMD0XYlXu11CVKkEnupWo4dvDJBMfT3wsNvAc6kS7r0kqR1SLuLGQf8ANTysXxleWt4JfbLUddG8mWa3iSAfbvA/d1+q9JfxwCxCxXUNkYPf+q3x5ufLg88YWR0gPrlBL3NSw+i6HJgQSQ8T31CwHk8is/dg4P8AkusuuVmEyJJwC3U6I6vcmdtcTI+COc5yAluBB8AUHuzkGMi35fUKs4fCkE/8l0Ysc1/u/otEZbQ0YwEezsT5rAIy7P4J9W8aGQ8CWCK6VMBNnlrrGsSl+u2K6PC5lPHvhCNG8jSywie0+EcRH5rjVxtmdhsl4xGVspqppG4+uR0A6f1Kh3eqjabGnLMj+4nXyT9/R9Fx/j7pygGgIRAZy4W/3CBoB45TWbGgzVe4sxtKA2FNTGzeq9xY/dl3QmyXdNMbfdU9zxWA2FQTKaY6HuDur9wd1hEkYkmmNnuDuosu5RNMdMRCLaoCrcIK2hRlRkEJsCBmEJQGwITaEBllThKNoVe8EDnUdI94KjeED3VOkG4KvfCaNLqOs3vBX7wTRodR1m99D76arW4UdZP5CL31NCfnLDH424RLGY2v4dV4SyO4lsDv2C9n8xbu4Uwc4Xi5SJHh4KwKmz7Igt+auIL7B01VkbNRk/t8EVe4kAlh0AwqsdL46v1gE+Q6r1XBBjHA+q838bUdw8f+tF6biABh4Lly7us7NtYKdWOpSoJ4xHuSoq+uFYL6qmVjv+KAokd/JHhLiQMv9Ex/zVKp9qoyy4UMumoULHDZUFxOj9U3d+CUGHVX4jPkVdSw1+2qhHVAMf0RBmcnPZ0TAkNk6FJmXl+idJ2SZMVK1AbHyhnEOw1KaMN2Qnx6KNazyglSgGWuQBSJxGqliyufdS50WK6rBC61scP2WSyCy13ef5VDuOq4fIhtJhLQZHdetvpySQvN/I1bbfyXXhfhy9k+XPhMwOfVAp42SjuH1PUeaz7WkQCz6IBuBcYI1XRxagIaZBCMEdIEnxKyxnPQgEdE6Fku/wBJAKrrVWbftHpHUALfx9sdsrN1mMRPp/qVz6ra3acZP4Fdvh1VVwhdEHOCWf8ARQdTgzn7byEYnoI6AeS0EE5QUVBtw0llwniJGFEpJCAhPMUBiohRCFkwhUYoFsrZEyhCCBGEARRQF0UUUVHR3lTeUColBcrPFKlYVJJUlBZtKA2nugkgKGDNpVe6Up1AUU33Cp7hSxlWyA95V7igRAIC3KOVQRMgEyKAyKMhLIQWJohIpSMIMvy0ieJId8D6rzEI5kf9PRek+WO3iknAXmwwiW+45K1OwTOWT1J7IqQTPuUE5McfincV94w+dEqx3vjKJSIEjhejohtAC5vxde6oEhm7Lqwxlc/l1Oj2T4YixSI5KePyQEq+ivIYDVWA57NqoKiH65KNgwBOnZXEdUW0FXDSXO7uO6IOiMI+UlUYkFnB6t1TDQZyFA4zomHaTptKGQLOzgdlDViXQnVRyD5IHxgEfRSLkYcnyQwUpdill1ctw1GEG6b/AGlkWQZfoqLFU5A6qbsMdFBR/NKmxdGcnP0QEYRSp9lmsitUgkzi58lmtRh5EHj4rzny9YEnXpuRofyXnvlwMnqFeHdOfZ5+f3PplWQC0xjuhnqykZNg6dV3eZcgG8tUUQTjLqvFMrESWyI90DK4uRjI6r1HxdEfYEo5jL7o/wCIXn+LH/ljj0OAf6leq+NolRWR+0l4qUbKoiMdvbRGygGUSiBIS5BOQEKhJCFNIQEKAFRVlCSggKOKWNU2KotsKIuiiDYyooihKBU0qQTZpMiooJBLkEwoSgUrZXtV7UEAVsrARAIBARAK2RMgplGVqIAIQEJpQFABCgREKmQcr5+zbx4RGspaLgy+zxOV1Pn5SlZGHSOn1XJtljbqtTsEk57vqVq4ObYjxZYiXK6Pxcd3Ih5peyzu9r8fDbWB4LXjqk8UCNY8kyc4xi8iAO5XJ1Nic+CfCWG0XHv+UhAtF5EdAOqlXy2B7g25wqO2CHRRIdcuHydRZzk9P+60w5tUmO4fRDHQDdVe1ZRzaWDTGcPrlOjfGXXzCvRMphiNUBh16o33aIZSEdcpUmqZ8OrEYszBU4d/yCJ2QCw6BlbISTp1RRf6+CAJROgP1QgHq6dKOPFKILt+qljUoW9KCT6NlObofogIBKlWUkhsv+KXIn/smzABQHCypZ0SZ4j4p50SLSGP5qKx3HuvN/KyckdF6G+YAMui818mXJ8Oq1wnVOXZxph5FAcHwTJ4mD9ChkNQu7z1Ilvom1vuDYCTtyD31WmmDmIZy6Du/GcLfT7h00816KmOyqMT0DLN8bxTVxYxkHMs+S2D046dFkqwrdCSo6IJ1RVblHQUQgkiJQkoFySiU2RSpaqCApsEgFNgVQ7ooqfCiDeQhJVkoJFAuZSJnKZMpJUFFRlbK2QAytkTKAIqmRAKAIggpRQoSUQToXVEqnQW6olRUUEJVOqdCZMg4vzlJM4zbouHMFie5Xrfla4/woykHMj+XZeWui5LBgNFZfj6bvCyS/8A0zAZ8Su18FTu5ALaLlxhnK9F/btIeUyE5XocZ1ekiNteNfFcrl3GyZBL7ToZN+S3Xn0t37rBCAc7hEeID/1XN0jKayM7QP8ASGYpc7LoZDgHC6OyIxCTDsxA/BEOLXNxKLfoU8l8XDlzbBg+rxOqA/IWuCJEEfiuvf8AFU2R+0DxC513w84Oapkt0PZWcuNTx5RI/J2B4mTPkP3XR4/zJlCJJYjEz0wuJPg2tl3TKOPYJ7SDEyDf9/NXIS17Gr5SJAEiN2AWxrot8L42xXjqRbCRIJcNEDxbRdjh8mTB3JAWdazXegQfJFgf4LFRaTEPq2QnC7djoNVdZvG6f9VYSDbHqrlaBkH6lNTDycauUMg/ks55cAHkQB3LBL/+RokcSB8k2GVs2hspZGpdBHkQkHjLCGVoIwUuL1SeqWT2V73KGUvFZWAJys15xhOsLZKyWy6KNMt+YLzXyRaZbovSW5x2XmfliPcP5K8O7PP+rmnL+BQnuiB7oZYJXdwWA66fxFIt5dUTo7/gudAakrvf2zxjZbKzpAsiPUQ9MAGZgqkUZihMVnDQGSrciMFBBMNCCiRCCvYmJpZQFaNiE1q4azF0uUSVs9sKe14JhrEISTIwK1CoIxUFcTWXaWUWv21Ew1W9CZ4SN5UcrLQpSQaqEFRQWFagyrAVEZQBWyjFBGUZFtKm0qAGVGKbsKntqhG1Tan+2qNaYEoSnGtCaypgRJKkei0SrKAU7pxHch0wL+UqP8CsHVnXmORDbtHg69T85ORshTDQAABcD5Gr2rBWT6gM+Cxwu8q9nt4zj6eO92CEAZZwF6z4WgVcXdn1ZXneJULLowGZSI6L2Ndca6RFugAC3yrhxgJHfBtQss7I1/cNpH0XRjT6dMrNyPj53Ah2fpquWusji8r5qmmTR9RHXQfisZ/ueYxCD9mXSu/t+kvvcy6EaBZLP7eEQTD1Hoy1PBL5/DOP7s5ALSoB+qKX90RLe5xpRfq4SpfBWwnvEgJDXcMJN3xnMhM2GG8Fg408srXjw+mN9kb6/laLvVmPicrbVfXLUAgjEguJTwo00yslIwv19ohwQnceeHq+391Z1BUvGfDct+Y7YjGR3DK1cOBLZcjVcqi06Ox8V2fi5CbjqNVityx0qIkDPVGXDlPhWSB+qCyDAg5K18MbtZpXMBI/isPM5pDQDtqSMn8E/lTjAa57LkWzE5F1nWpGbl8+UiQJFgMy6uf2hY48uyD7Hc9HLf5rdLjwkGfyfosl3CO1gXkclv6lamJdHV8xyqiHsJ7gsttfzcpeL98LiHhWxJO3A06KCjkk4hL9VcjG13q/l5CZJLxGj/oVpHzNZOSxP4LzXs8oHaYk+YZNrhMBpwI/RTPyu/h6mrlV3weBD9R1CTacrh8e+2iwSrPmD9pH6rpw5UeQHdpDUHClihtLQlLsvL/MWf8AKAvT8gtWey8h8hMzvkdQ6vrnVn2X/UkKyHQjoj6/QLs4qJbTwXs/7ao9vhe4QxsLrx1VcrLIwGd0mX0TiUxp48KxgABEpqpWcIXCItlBFQFEFBBFEIqAq9yCbVWxXuVuFRQgr2K9wV7ggraFe0Kt4U3juqi9qiH3AogxRpKYK0/YptWcUj2lPaT2VK4FCtkWwIlToiCAV7ArBVugraFNqtwoCEwWIBFsVxIRqKDYEBgnOEEkCjFCYphQkoFGCARaQLaFNKAkOEUq/jCXKFstIDcvK/Iz3cuZPqyvcXRAqnPqILwd0T/KlE5bJWOMy16fbyvKcXR+DpE+TEnoNy9SKwdAuD8BD1TnqdF6GuJ06dSpy7sTsKMekcnumirDHKKseGE0gMpjWsc+JE5iWP5JE+NZE5jjuMrqABXsCvivk4xrBwW8ipOqucNkoxI7ELryphL7gD5h0mfDpP7B/gnjTyjiT+NokThnWa/4Wgh4kxs6S6heiPHhH7QB5JM6XOdFmxqXXDt4QIgR/wCoMP381u4PHlC0gaYcrRZCAl6YhzoVo4tZGSkhftvpjhku8+kp1bxgT0Zlj5EwAfyW+XZz49a8/wDI2Tlaa4lnySuZOwAtkrrWUG73C3qPXqgnwv8Aj21tCRGpyuW58OtmuLPkSifTEk9kmfycqf8A1IgeALyWu34TnZIuMn1YMqnwLIcWdIpBlIZmdSfF1uePyzZy+CqP7h4IIFkZQ8dV2OJzPjuUB7VsZns+fwXmp/G80UCP8eUW/dEP6Vilw5wkDCMoydtCPzVvDje1xjz5TpZr3xqrIYMI9/8ANZruLFvu2j/U7n8F5bj/ACfyHDIjvlOsaPn9V2eJ81C9vcHq0B6Os3hZ+Wpyl/BlnGizwlukPoUiiVlPIG4GIl/qHdbiRPXPZkudcZHOCNCpKWK509vHlLTGV5CyW+RkvT/K2beGQSxIYLzG31Munr7OfsvZNrRCOJ3E+AZVMMVIaro5un8Jxvc5kJkemHqJXsfdAXC+CpjDiixvVIv9F09x+ilqNErsIBes87GCESKajYLkYtWSMkwSU1Wn3Cr9wpG5WJIHbypvKU6joYcJlQzKVuVGSBhmUPuHulmSAyTTDvcUSNyiaOkZBCbAstnJAWeXLVRvNoQm0LnnlIDyyqjom1D73iuYeVI6If5M+6bDq63vK/eC5I5Ez1V+/NtU06uobgijaD1XKFkz1Tq7Jd1NXHVjYO6YJhc+uZTxMqaNO8IJWBJMihlIpphpsCWbQlSKWSU1cNlagFrzA8UooY/eD4qaOxZn/j/1xC8V8nSaflLoEdXxovcTqlP25RwR+i89/cnBAkOTDXSQGpSd3o5ds/kfwNZ9kyPUrvVhtNT+i5Pw8DDgwf7iDI9V1ayWfTus3uk7NMThu3ZGASkRJMvBOie6oPARgII5KYDhWJQkeKo5CIv1QksgXI/5LPI6unTIbH1Si2Ss1uFAB3bJ0T6gQR3SwCxL4T6I6YP1SRbejRJ/aZ2fqubytzbXd10rPsAdcvkAl31GFebPrZagIzfvhOhTv11GiXGJWqlwxfPVc46UMqsaICAPuAkOxC3GI1H0Q+1GRytYxrnW01WOQNpIbVZZ/FwlLdHrqF1pcOOsSxVfxpt3PdTqvR5zkfB1OcM/ZYB8L7TmOYleuson0iEr+MW9QCm0yOJRRbGIjkt16pxBGCCun7EYn0hkm6A25Cz11bjgfN440Rq5XnxE7or0PzoauETq64bBwQu/Ds4c+5UouT4JnE487rNsA5GSj9t5H8l3fhOEK6jeW3T6LWpePTWnix9moQGE02FFKOUDKMBJJKIKlAoGxKMFKBRugYCjBSAU2BVDFCrGiiClMqKIBIS5JkikzKCnUQPlRAFsi6S5TrUhQWhKIKiFQCpX1VILCZEIAmxQFEJ0AlxCdAIHwT4jCVAJ8QoBIQkJrISECiEshOISzqgWQhb1BMKDQug9BWwoiwyRlcP5x/48hqXwtvH5m70vkBZPmmlx44cymAApvXHeT/Xe/QfD2V0Qg+z0h1ri4iw6HV9VhBsiINMmQDMNP8loFkmaLHuSpTi1VkAuMnqU8EtrhY4EMHyB1C0CYGT+CkrR8S2XRGY7LP7gbslT5O1+yvkTjrYbYpU+TWxDsubdzmB2lZXuu6+nus3m3PXPl0LOdEDbEjcqhyN8hE6lYqKXuET0ySujGkRl4jqrJTlkaa4h3GqdWGkSPxSoy9OvqGidWSA/V9PBbjlR2gN5dFz7wZAlsLo2aD8lis0P6KcjhWAlixDhPqPUaLLyjsBAHXol8bkiMwJ6E4C5247ZsdeEsJgDnRKqkJfaXT4llqOdQBsMrLK9zluyhiPN1pkqQBSbAM+KeXSrOqzWozyH5LNYHyy1S6Hus9wCyPM/Php1s/V1x4xO5db5uQPIiOw0WXgcb37c/aNV1lzi52byyKjxjKyusazyvScSkVURiOyyU8f/AN7vbAiAAuptDYWeN22t+7OPHjx++rPIJZC0SCTILTzlEKkZQFUECrdACrdAYKbApAKbAojQCqJVRKhKKjqOhJVOiLJSZlMkUmZRQPlRV1UQXakPlPtDpJioIFCorZADKMmABXtVCxFMiFTIgEBxTq0mJTYaqDVWnxSKynRKAygKJ0BKAZJZREoCgEoSiKpkACRhLcNQq5M53zpyYRiXkcfoiIKzyqE+QNxaMRuOfyZM6tceVnT4rcxDkOT4HqpHcYnfknoNSUETTkxLDwdkVZO+W7BOhOjLFdY0xnIAOQABoEcpxjnqOh7pe1iAznr4JPL5dVFZlOW2MeqjcVfzIVg9GWOPI5PMP/DE7P8AVLRc+22/mSE5g10HIifukF1eNICuMYFh+3xUsblNr4UY+qyRnM/gtcKhGOnklwG7G7P6JkpHaNuO+7orOJaVBqeT6hiYw61S5ERJz2yQuV8tdI1xlXL1wLggdSubH526EhG2oEH90Tr45WrL8M7L3erjMHIOE+ueRnAXneJ8tXb6Rg/6ZYP0XWp5MZByRlNSx0p2vH/FY7p4DIZ8mMQzvhYeRzYRBzkKWnHiK7Mo9crNyahGQlHqWKCF5sluJ/yVzn7t0ahli58ln8OjZw+RKBEJadF0oWPkYXKlVIB4nRaOLc+JdFZ06M8pL1dJ8Byr3DQJcZggMobARhlpzWZDQnKTM7jksEcpdkicxAhyw6nopVDMAHGhWS4s7p1lkRrgahv8livmAN+gIy/6qDzfyNgs5ch/pLLX8XWxLBZuPxZ8rkWXH7XLHuutTSONHZDM5JzvTxjXqnflf4auNWDMz7YWgqqK/bqAOupRkLfGZHD28vLnfx0hMkuQTjFCYrTmzyilELTKKVOKBKtQhRigsJkClsUyAKB0SiKGIKMAoFkKMmGKmxEKISZhaTAoJVlBmZRO9ouoqBmEkhNJSyVlQEKlZQuqDCIJbonYILbKIBADlNiHQXGKOIYqRCIhQOgU0SWWEk2MkD9yAlDuUdUQlCVZVKCiFSJUgEjus0rDXycHBj6ugZalitj/AO+hu0kMeLIs7x0KowFAfGMAI6wHiCAT0j2/FJhaCGBYEEE9m1V1NGb6ylpLq3Rc/l2jXPI3Dp2XnuRC3m/IGMgTTV9se8u69MWEPBci8SqskQ/q1Ojdlm3K6TsyyqawGYG2I1KlHJrpuwNwGB11Way//l2TLykWHZbqOJIB4aHvnVWGmR5Es7LMB3I/oUZvIiJTckMA6H+POAyHDtojHDnZ6i+M/wDQWpUrLyv+eRlEsSzE9B4LnW8cxmRHLnGHJXcs4WwQIydfqkS4hMt8XePbo6aZrk1gxJDevp0XRp5UogCRyOqV/HaZeBc6lnLeCVCFokWeWcDsFdlTrGqzlTZ9wPYBc+/mz3sJMEdknm24uR9Euzi5i3X7vNOkNtXVzeXI7IEAd5B11vjotISszZL9y5VVBiC5yC4GrgdF1eFKEBEl+79iVm5vRuXo6+wGPgeqzzemzdqOquNw2gbtQ8fopK2uysxOSdRqliStVV4mGBwmbnwuJRyxTf7Uyz/aT4LpC2JG4HPgmmHTljx8EiUzKLnUHUKpWExd8pU5mILZKaijY5MZa9MMfxWD5GwCiRiSSfSB2JwnGzG4n1P9p6+Szcv1zqjkbpAl0iX6N41Ma6xGI0Cfxqd0366kqVgjHddTjcX24OR6jqnDj5XV58/Djc73pCdimxa/aCv2gu+PJrCa1XtLd7QU9sJia5xpPZLlQV0zWEBqCYuuYeO/RV/HK6XtBV7YTEYBx/BFGhbfbCm0Jis8aUYqTgAiZMCPZCsUhPAVsmIz+yOyE0hakJZUZvZHZRaMKIOA5V7SVpjxZdkyPEPZYxdYTWUBrI0XUHDU/h+CuGuVtkrESV0zwh2VDiN0TDWCMC60QrwtMeN4J8OOOyYayCvwUNZXQHHHZX/HCeJrl+3J0yNcl0P447KxQOyeJrEK5dkXtlbxSFfsjsmGsHtHsq9oroGoKvaHZMNYPaKnslb/AGgqNYTBg9krnfKQNNtFrFnMSR4rv7Fi+X4ot4Mz/wD5f8gA6kJhL1YaySYjaBWca/mm/baCG2OASsfF5AsEA4ba+4ZZaJ7hmJ3A5HQBcq9EdeO2QiCH7MsfNB3FmJBYBO4UweNEuzDJWesWDkkSO5nIj4d1jk3xcjm8KVtR9uO20fbJYOP/AHBPiz9jlQlCyPpl1XsY11zAM4iI/aF5/wDub4AXNzeOGmMTb8ir6/q9l5be3dp4vy9V0dwLx1XQjz6ixcDuFxP7bi/HsptiBZFgQeoXbp+LqPFIEdsw7S6grdmMeU+ZhsOTTPAI806J48ugcrnQ+K5keN7olukz7CNQh48OXZEmNUiI6gKNZxu5ezoWcWmYOzU6rGfjzB41gAnVAeTZVLbPdGXYhlf8+wYd0sak/kUfjd33AGIGe7rn28C6uxoh47jr/p6LoDnSOdD1bRT+ZElpdOqzlMc6XHsqgZT1MSwbqeyEmYhESLYy3guob6rCQWdKnVVI7gBq5Pkp1gwRncwIk40bt2Vw5hgZ7j6iHWk8aEiJQLP/ANOs1vE2y32TEiHyOiRLWfkid1UpQJJHqieoIWv4rmzuq22emyGJx/xQ1moxEYg7i+VXCoblWSDmLBLVnd07LJtjVInORYAuSMptkHAfPcLPYWLwABZg6JQGIIjAB4f1SaqrrOdCiv1sCQD0Hi60ho1PEFxlkfwlgsu5F8gNziAfVvFb4Tbjnz5eM2OnxeD7ZE7SJSGkRoFtcLP747qvfC7SSTI8/LleV2tLhU6z++FRvCqNBIVbgsx5A7oTyB3QaSQqJCy++O6r+QO6DUSEJIWY3juglyAOqitRkEO8LIeSO6A8kd02I3bwp7gXPPJVfyvFNg6PuBX7oXM/leKn8opo6JtCE2juuceV4oDyT3TR0veHdRcv+RLuomnV3BSEYqCbhWGRQCoKGsJjhRwiEyrQe2FoJCAkIFCsJkYKbgoJh0BiKtgh3hUbAgJgrACSbQp7w7oHhTCR7w7qjeO6B5VLObx3Ve/4qjThVhI95Ubh3UDiyG2uFtcqpfbMEH6rObx3VfyB3QecqMKLLeMAwqkYtph10RGUwSw27WY9WWX5iIr5tfNJ/wCOTRn4MrqvmfVKIMZ4qjqw/wBRXPlOrvwvRu4VgmJQJYDONPotQlCDSHpGNdfoudAxqMSSGP3S0BTpxfYYkCEj92fwXOuvF0apEbiQC/Q/cmmEbImBHpIZZaJlyNQNJagfVboDdEFvqrC1wORwJUWylUTHttwyZxPkbvjuGarKLeUI7jGYmPcL5aW/VdO+vcCwz2SIVxkdtmukVuNbOc/2n8/Ld8X8jxefxY28aYmCGlAfdXLrGUdQQtvFphGth1JP4ry3K/tfjXXG8GdNsgxnTIwJ82XU4l3yXCphROEeVXXERhIH27mGm7c8T5q5+HHl6r18bK3/ACHCrvp2EZJDHrqgHw/DERH2gWDORlY7P7glGUYWcDkwG71SiI2AAdfRJ08/3D8ZEeu/231E65xbz9KnRPH2yZnIiz4OqzkjY8KgHkI4crLyPh35EY1yMY/uAPROq/uv4Wc7P/c7DEsN8JgSbrH05CnF+d4XJuscTriMV2WQMYzHUjqPqpYsvs+rcjDzvjzXOFdL7pH1E9B3XC+R5vI4EztIlES2gE5P4Lu875Oc7bJUVuANsLJaHxbsvP2fF38+cTdLcRkdBnVJZO7p4+yz6/bPX/cl0iI+0XOgBdbuPPm80kyrMID9r5K6HC+C43Hh6IAz/dMroQ4wrGyA8SVz5c5bnGZ+Wpxydbrmcbj3VFzHR2b+q6HBoMN0pjEi4IWuNQmTEgiIyyfIQgGOAymJrJeIwcOzjqufGzcZGQxEkE9wmc+42SFder5KSJRhWxydCOquJpfMtlVXKwHA+xv0Kv46Y4/GDvvs9c/MrJyjK66PH+6GtjftAWkMAw0Gi68ZkcfZduNh5hVfyysjKwFra541/wAqXRUeTNZwCrTTDjyJd1XvzKUommGe7Lur9ySUrUMGbCgMiVFRCCGRKrcqKiCOqcqFUSgjqtyolU6oJ1ToXUdATqIHUQeo90KxcFlcqOU0xq94Ke8s+VYTTDzYgNiFCU0xJWFV7pCGSBNTDDeUBvKXIpRllNphkr5d0Pvy7pcigdNXD/elq6o3y7pL+Kp00w33pHqiFku6QDlEJBQxoEyoZFKEle5BCVW4qiXVM6BPOpHJ45rOZaw/8lg4MibZCeJxw3QNrqup7ZlIZyMrDyoDj3i+I/8AUIEhoAehUv06cJ002dkIyNcDJ45OM/iVq4vIEton9x0y5+gGFjuMZRlH07md2dm7Ml8HkSk0iB6NI6DzYdVmzo3L1dmiVh5BEpekaMAAuhXYM7c91yYcgWwcyY9H1WiqcwBZY46MSMrMbzW6WrdO6TZWDmIyNEzfvAbqNVmsvlXIiQAgOvVa1JsaaeROH3Bx1Tv5FUhqxWKF0LAJQIPh1ReIWpzayXr2apCuUSQXPTqVnnRDqGfUICZR0LdyrG8jJTylamztSLONUHMR5hZLhCPpjqttkTnKTsyxDnt0WLY1535us8aTIuMLVVxxgQGNCUddWc6BaIxMeiz3ZvK1ftiutohyst05V2Qri5fUvjKdbZsO6WREaDVZoA7977wQ5fBUrLbCca6zuy3dc/mc0B4xLz6Dsg5vNAGDnTy8VzK7J3TJDOH3FWMtNdZc5cs+Uj5HkQrjvdiBhu6VLl+3GzPq1Mj2WLiGz5HmRnINRVkBb48flnlyyNnx/GnCBvtJNtuS/bote1NMfoqZbcgCKIRRAK2RAsoyIqkAsrZEoyAWUVqkRFRCtUgEhUyJUUAlCURQFBRKp1StUUqKPaqlFACijZUQehUCF1YKijCMBLCbHREUVRVyQEoKOUBjlMChCBE1mmWK1WBZphAuRKF0ZihIQU6rcrIQsgt1YkhZWAgaCidBFGAgpWAr2ogEAgy7dMJPLrFlMoHUj81oD7iOjJVj7m7rPJ19fZzeNa9P2tKJ2zPVwh4dhpvsZnJ1LdegZVeBTyN2TW7Sbv3ClgEOZGQDe4MEFv0T/wBX/wAdiq2UIgwERvYGUsN/4hlqiAJA+oj9xbH4rBVfXEmmUgLBkAB456y6Fb+PfXZL2hOWGGm0fQKY1rVGOHyBqk2mqYMJhz2WgTrEm3BtFntjvtx3wfBTGo5fIBpl7kd0Yg+rP+ClXyNpg8iIvL0jwHddKXG3B3wcF/8AJZLvjxYSQHAwFOy9KH/5KYyYuOiMfJuJExcx0iNUqNFgnGM44iMgd02NMZ2eoMOnj+CbVyKq5V17vDZ28lrqq7klDXSIkNgJojtGCSBq6hRjaNcJds/RiTF0E5kNuGCx/wCis104CfrGAcF/yRDJWRnggyj17jxWblcqNNctpzEMEfIsFcS5IGsR1Hn4Li2WnkWmMT6Ccno6SJam6VplMlnDuf8ABALoV1GEfsP3zfJPdHdbDb7cQ20MT4LlWzlafapfLAnwXTjNc7TCbefyRTAtVH7j4LtcGquucoVhowADJHA4kePWSB6yMlaeEZe7MkYPZWXrk7ROUybe9ayFTIyqVc1MqVqIIyEolGRAsrVgK2QAVTIyFTIAZUQmMqIQLKpEQhKASgKYUBQAyIBRWFRbKpBW6olADZUUfKiDskqCSAlVuyorTGSYJLLGaaJ4RDJFLdWZICgOJREoIqyUATSJJk5ZSicoKIQkI1TIF7VW1MVMgDarEEbKwEFCKMRUCIIIArZWoThAHUoCHRP6JINxCzydvXOjPdTGYLjoQuJyTKmcYyBEoHBHZehkXHiuZ8pxzbUdgeQyT1YJxvVeU6HcOdkpAhwHcA5D+K2zuvIAsk/XcInHkFy/iORIQAjJpD7vp26LoR5MuRA2WD/giXaL+ojQeKXucezfx4zFby3bZ4jKYc/5Ba4wlGLyyW7guFm3n0TIJByIl3C2xJlEHQs2Wb6pIqqYBhjcdT4KrPb+59r9PJGfbDQfAwwwhiBLMmEQde6DPGAIlPbul3PUeSmzblmJ0HVuy2e1Ej04AyT0KRaHO8DA691LFnIAO8CEf/qdETAAx/drtOUuc9pJB9PRuh80r3ao7pE9nB6KYto7rqqwYEmJl9oOA3ZYLRul6fUNRuZwfqqvs90zjHMQxgeoPVczkcqcIuWIBI3eI6FM1NBy+VIk0g7pZMu4ZIhPbDTbMjVJhu9Vkvvl+iMbbZyiNGZwtYxpFth5EtkMR6f7j3WvhcMQkLCPUAxI6q6OIBHdKOQWI8F0KoAYjopy5fEXjx+avIiWDYTOHARgZdScqSA2oqSBBgnD5T29oa6iHco625LUQuo6AlYQhGAiIrZWArZADKMmbVNqBbKiE7ahMcIM8wllPnFJlEqgCUKIhRigBlRwmbUJigHchLo9iIVnqiaSxUTvbyorhroEoCVaGSwqxNNjNZTqjgVRrEnRM6VWU+OVRAMIZJjIZBQZ5ZS+qdMJe1BShVgKFFAorLKkRFaijoLdXuWe3k1VD1SA8Eg/I1Ngur436NjoGSXO0Aarn0XfI/J3Gj4+rc33WyxGP1Xe4f8Aa1dbWfIciV8tTAemCvj91PLe01ghPdA9idVR0XQ+Wr49IrrogIRHZc8FcuXd6PX/AFCdEqcQH3HEtE+QSZsYtJRtyjKXH5M6ziuQdj1B8tF1OJ7cqBGR9IDsBp+Cxc7jGyLxHrjmPi3RJq5duwQHoI+6OjrfeOfavQV8mQiCWjEYBGpf9E2dzkAFpM8mXFjyYCvYcAFxHqfNa6raojfZJpAY65Ky1MdGU4xNYBDu8pvr5OjFplWYECJGS2n4rB79cqoTd5Bmjo5TI3VxxueMtIpqulx74+08pf5oLDGZDlm0H9FllzKss2Awws1/MHs1zgWm7HqU1GomB2vIEZlt8R3WG+/3Je1aHBLsBiPmUPuwiSTkxBIOMrncjkbz7kZZEsjoR4p3LR38iLT2f+nEsOh8Vy7bYX2MCTAHUlVyOUbHrrxWCSSr49AmRJiIdPHzWpMmsd6dGJkDEBoR1kcElOprDPAfVF7cY1jr0AWiinrPDagLNrciU17Qzuf3LVGO3PRSNQfccdgiIz4Lna6SBkCRlOhxLdu6OY64SzotfC5BGD0W+Fxz9nHWfaXIOqmxdSXFq5EfQdsz16OuJz7vkPjbGv45nT0thkfVdZ1cLLO52wqxBc+PztJkHj6D1XV4llHKjupkD4K+NTQxgjECtMeOUY46eKayiKMRWkUIhQmGsogr2Faxx1fsK4aybEJrW72FPYTDXOlU6WaXXU/jhV/HCYmuT/HKg457Lrfxgq9gBXByv45VjjeC6fshT2gmDmjjZ0Rji+C6HthWIBMGD+L4KLo7Qog5jISFp9rwVeyVzxrWSQVRwVqlR4IRQX0VxNVArRWUEaSnQrKYas5QkFOFZV+0mGsc4lBsK3GkoTSBrhMNYtpQyBCddfx6h6pAlc2/5WvStXxprRLGqCV1UA8pBcqzn2TOCwSJ39ytT1/aeX06tnyMI/aMdyuZf8rfbMwrLQ6lYeTySRtidUuMtsdo0Wsk7RNtaJ39XJPcpvxfE5HyfNjxqiRE5sl2j1XPnPC9v/Z3BHH4R5Uh/wAlxwf9qnK5GuM2vQcLicf4/jR4/HiIxiPqT3Kq20gEq5T6lZLZkxLZZcrXWRy/krZSuiCs4KC2/wB+yREjKIJEdwYjuFccjK58u7rx7DcfRBKIPR1YLeSubNlRaTJtFkv4tdoyTGccxkOi2SAB3OgI3KpY5BnfXLbKLgfdMBwnR5Fc5gPufo+F0DWAJdz1XPu49RzOp2+0xwStbKzlnY2y8TEYu209OiqXLmCZRyDgnoFjl8fZ/wDblKJPQSxFDPi8urSyWegzlMn2m36bpc+e0A4kf0VHlRk0SQAM7uxXNlxuXlyS2rnOVQ4HInEGT+IPZXJ9m36ap86AlIQz1iFhuvnZFshzluvmtJ4MQRAkxkegTq+NCEvt6+kdAmyJlrPxuLMl7YtHpHuuhXWGYHbEZb/BGICBMpFyfyTKqxL1dP8ArRYvLW5xVCgyIkSw6RWuNIDbumgRVRETuJc6ABM6gM7rFrpIHZ1J+ishMIZCQSc6LLRUsAlVxpHfIMQ/U6HyUt/JK4YJ5VhbDfc+vkFqdr+mL3n7djj2EMFuruEvTMCUTqDlc2AZaajlytceTPLixfO/29x7KpcvgxFd0Q8qwPTIf1XleLzpce3dW9chqOmF9ANjVSPRivmPImP5NpjoZH9V34ctefnxx7Lgf3Fx7GhyRsPSQ0XZru49geFkSD2IXzaNreaZ78seohuy3kYfShEaogAvD8D5rlcKQMZmyr91ci/4L1fA+W4vOgJVy2z/AHVnUJYmtzKMEO8KGYUUTKmQGwKe6EBsoyX7oVG0IGEBCWSjcEuVyBxZUSFnNyE3eKDRuCrcFlN7dUB5PimjdvCiwfygomjq+wFPYC0sFTIMxoCo0BamQlgHOB3QIFI7IxUFnv8AlOFQWlPdLtHK5t/9xSDimEQOhlqrlTY7mwBZ+RzuHxwfcsG4ftGSvL3/ADXLtffaWPQYH5Ln2ciUiS6via9Jyf7hhEEVQbsSuRyfmeRcSNxbwwubKeHQGw9EyHU+d1k/uLpUrANT9EqdhZJJ7lUONvXRBKbxQjOiXZJ2ESgAyeweCN0qB9RJRuoooRNlsID9xAX07g1ijiVVANtiAvnnw1XvfJURZ2kCfovo40AXPnerp650qTkSFi+QujTxJ2SkYNpOIcgrXMsFx/mr2hCqNvtzmftZ98eoXOd3S9nOju2gykZyl6jI4JdOgceaUT/yEfRHEthZrpIfFmUBdwUMSiDFRQygDFuiXsI/RNIPVCQwdEKMZAs/pKXNthHbAT/265Vyr3EOiMka2eT+SkRHbkvLv/RaZVY8+iEVthVcZhWCxl+ARTrJHo9I7laBXDsFZHQKamMsONCLyOZHqj9qLYDBOP8A0EPhoB0U1ZAQoB+7L9FpjW2AMDRXXAnTRaBDQBS1qQuEGZ9SmEB/JHtw36KxHqR9VlQCLoZsPSEyUmj2SZZyis9x6JfxsX5Fs9oDltzuS3gjuxqr+IrbfMwiDPO4FzLxPZan9axe/H9ulELRCOEqIToHGVItBy7RVxLrDgRgf0XzSct05HuSV7f+6eWKPjJVgtO47QPDqvCAr0+qdNeb23rIYJEDCsTJICU6uB9Xkujk1m1j5J1N8okTBII0I1WEF8lHGZ6Kj0vF/uLlVgCxrYjviS6nH+b49+H2S7FeMhYQHTIXnyTuj3H8oEPEuFX8leW43OshIbZN4dCtsPmAC1sdvYhZvG/Cu3/IKo8grDVzKbQ8ZjyTN3ULPVTzeUBuklEqOpphnuSKozKAKJpiSkSlklGUKKrKitsKIPTmwIZXxiHkWC4/L+XqpeIO6XZcPl/Kci4ndIiP+kLpJ9sW/T0PM+eopeFXrn+S4XL+X5N5O+Z2/wCkYC5kr36pM7nOFrt2TPtrlyZE/dhLlZ11WU2HoqNj66op8rEs2JRmUJkUDJWpZskhIOqjFBMnqqLup5qY7qCElmHVFtERkOUMTlXOfpKKXBmJUJ7KoaKSQdn+1Y7/AJIH/SHXvexXhv7PH/7CX/iV7eUmC48+7t6/6wFs8Lg/JzlLlVRE4mIOYH7wfDwXXvK4nKjYeZGcoR2ZEbQfUW/asT5/Tdnb9g/+4T3TAQlEHc6bg+ay2KMugTYZSAAJJ8NEVZceRVEHRkwZ007ISDocIgDEFXEPjr0VgdFGY5RAyj6sh+yk326gJ0QCHfPZJkJGWeqqlxfr/wB1GymCD4RiHXqpQmNZJwD4lMFOQWbzTo1k5TIwHVZUMIgBHtkThXFhoEUThRVYiD+4oXLPIoih2jqigIJ8kBwE0j8Ei1ycKDJySTEtroFq+NoNVLbBXIn1Rid2fErJfEyIh7fug5nB9vp811ePVGFUIRAiANB0Wv8An91n/r9Q6A6JrMhiMpXN5MOJxbeRPSuJI804wteS/u3mi/nDjxLxoDH/AMivPlFfdO++d0y8pyMj9UBK9cmSR5OV22/aOirByUs6JsDtiqyuRVhwXCoEEuVYPZUMFndXviThLRgPFAfunvgLTXfCY2z06FYdpCoEhB0TCyI3VScdAtHH+Uvp9NvqA6HVc2m+cDrhaxKu4er7k7o7dPOouGJAHsU/cDpleaNc6zugVq4/OsiQJHCzeH0uu2JBXuWKvmRnrgDqnxnGWhdZvGxdNdRA6t1FH0UQ7iyiDhW3El3dZpWnqlymgMl2YGZklDudA/YqnUDNyoyQlVnogIklTc2FQBdXIIool+qvadUETtyr9xEUUPVGJAqsO6AdCqnocI2fTVVOJESDlFBE+lTo6uOio6oO9/Z//wCdM/7V7SRDLxv9nZ5tn/ivXycLjz7139f9Yz3yXCuYc0SFZjLLzf0keAXcuYfRcOzaeYWM92XgftH/AIrE/wCv06X/AJ/ZkgHZMjHA/VCQCQU0D04WVLOEcChlFSKK1QiG80e3HdLqLp0W/FVKWYsqZwxTjFBsO7wUqAEMM7Ke1I9U3arZgooBW3VEwCgduyIA+fimmKAKIDKm3ofyRjt0UVUQ2WdWR1H5oiw8Eucug/FBRYqgGQgdSVbso0GZyzJUwjBJful8jdGs7QZSPQanyTuW4z1USv5cZSqOwZhYTggf5rsRg30WX47jwhE2RjKG7SMy5A/RboxIGcrV759MT7++q4jC8t/eXyIEYfH1nJ9dvl0C9JyOTVxuPZfYWhXEyK+a87lWczlW8mw+qyRPkOgXT18eu/Tn7eWTPtnUUUXdwTUsjDlVADUqzIoi9MKwgCOOVQUUQLYQqB3dAeoVEKwR1CsEIBAZFGZd9FUlQCDTC4nBLoyXGQsgYdUyM5d8Ko0QnKBeJx2WqvlgMTg91hEj1ViQ11Qdqrlg/cXWiFkZaH6LhRuZmK0VciWP8VLxlXa66iwfyiz7sdlFnwv2eTgkkqnZUSxUdaFqO2ip1AgJ1RkeiEnKpAQmXV70vCiBm9QSCW6gKAzJRyhCMICj4qTZkJl20UL7Sgg+3xQyUiQyhyg9D/ZjHmWj/avZSgdD+K8V/Zp//aGP+qBXvDE9foVy5zq7+u9Iw219Vw7YD+YQLN2v/G2Y+a9JKJLjr0XGlCyXMIkYHVjH7vKSxnSt71n7IiMt2TIxKZKrbN0wVhnCw2zzhhL+3yWswwT+IWaY2y8EQ6nIwWTwAQx1WSHQhaYyxoimBgGVkBBp5qwSiYIRJV7SclVGYP8A0yMSB0+qACPAK8FQ9XUhooogAOn1UwFWOmUJkO6guUglyLnsr1Lqojr+aKoKF30RbR9UcYOoaVCA647lZ51/yeRGppAAuJRLDb1fqtPIsFUNCe4jkt4Jnx/GjXWZjd68tPUDoFqdOv8Ahnld6f5aIwADDQaJsYvjuoBlI+Q5tfA4dvKswKxgdz0CsiWvMf3l8kI7Pjaj/vuI/wD6heSTeTyLOVyLORad07JGUilFejjMmPNy5bdRlNSoriOq0yLoqVk4ZCEBRCIMEPiiHdURWHUcBXvQENNPqoZP4BD7hP06KiUFjXKt0IPVR0Bkv4dlYJGiByoJFA0TJVifQJW7oriQiHwI6lMFjYBcrNvzjCvcPqqNXujaz5UWb3CzqIEYVqKKKoooqKIBKoqKIIqKiiCKKKKC/wBVYdRRUXFnRHRRRAuPgrUUQdr+0X/+Zq7MXX0YsooscnXh2/kmbN49FyD7P86bbfeb17dfB1FFzvauk7wy3Zu/61QhlFFzrpFy29Pqs17dFFEAVt9Fqh0UUQEdM6IVFEF9FAyiiC8ddE2LeCiihVSfoyXh8qKKVYqTMiizYUUQHDamYbKiiMsPIf8Akw+/Z+7Zo3+/wXVg3RRRa+k+aLr4Lyv98/zPaob/APDc7m/19Nyii3w7xj2f1rxqpRRd3BUlcXfCiiIKWqgUUQWNcolFFRXVQqKIIrUUQWHUO7qoogtVn6KKILCsO2FFEEUD9FFEF5ZRRRB//9k=', 'Software Developer', '2020-04-12 01:31:48', 1, '2020-04-12 01:34:42', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_has_time_use`
--

CREATE TABLE `user_has_time_use` (
  `id` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `tgl_awal` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_social_media`
--

CREATE TABLE `user_social_media` (
  `id` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `facebook` text,
  `twitter` text,
  `wa` varchar(45) DEFAULT NULL,
  `instagram` text,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_social_media`
--

INSERT INTO `user_social_media` (`id`, `users`, `facebook`, `twitter`, `wa`, `instagram`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'dodik rismawan', 'dodik.rismawan', '085748233712', 'dodikrima1212', '2020-04-12 01:34:42', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_status`
--

CREATE TABLE `user_status` (
  `id` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `ip_address` varchar(150) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'LOGIN\nLOGOUT',
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `angkatan`
--
ALTER TABLE `angkatan`
  ADD PRIMARY KEY (`id`,`sekolah`),
  ADD KEY `fk_angkatan_sekolah1_idx` (`sekolah`);

--
-- Indexes for table `bobot_nilai_kategori`
--
ALTER TABLE `bobot_nilai_kategori`
  ADD PRIMARY KEY (`id`,`kategori_soal`),
  ADD KEY `fk_bobot_nilai_kategori_kategori_soal1_idx` (`kategori_soal`);

--
-- Indexes for table `day`
--
ALTER TABLE `day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id`,`sekolah`),
  ADD KEY `fk_guru_sekolah1_idx` (`sekolah`);

--
-- Indexes for table `guru_has_mapel`
--
ALTER TABLE `guru_has_mapel`
  ADD PRIMARY KEY (`id`,`guru`,`mata_pelajaran`),
  ADD KEY `fk_guru_has_mapel_mata_pelajaran1_idx` (`mata_pelajaran`),
  ADD KEY `fk_guru_has_mapel_guru1_idx` (`guru`);

--
-- Indexes for table `guru_has_user`
--
ALTER TABLE `guru_has_user`
  ADD PRIMARY KEY (`id`,`guru`,`users`),
  ADD KEY `fk_guru_has_user_users1_idx` (`users`),
  ADD KEY `fk_guru_has_user_guru1_idx` (`guru`);

--
-- Indexes for table `hak_akses`
--
ALTER TABLE `hak_akses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jam_pelajaran`
--
ALTER TABLE `jam_pelajaran`
  ADD PRIMARY KEY (`id`,`sekolah`),
  ADD KEY `fk_jam_pelajaran_sekolah1_idx` (`sekolah`);

--
-- Indexes for table `jam_pelajaran_has_day`
--
ALTER TABLE `jam_pelajaran_has_day`
  ADD PRIMARY KEY (`id`,`day`,`jam_pelajaran`),
  ADD KEY `fk_jam_pelajaran_has_day_jam_pelajaran1_idx` (`jam_pelajaran`),
  ADD KEY `fk_jam_pelajaran_has_day_day1_idx` (`day`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id`,`sekolah`),
  ADD KEY `fk_jurusan_sekolah1_idx` (`sekolah`);

--
-- Indexes for table `kategori_soal`
--
ALTER TABLE `kategori_soal`
  ADD PRIMARY KEY (`id`,`sekolah`,`mata_pelajaran`),
  ADD KEY `fk_kategori_soal_mata_pelajaran1_idx` (`mata_pelajaran`),
  ADD KEY `fk_kategori_soal_sekolah1_idx` (`sekolah`);

--
-- Indexes for table `kategori_ujian`
--
ALTER TABLE `kategori_ujian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`,`sekolah`,`jurusan`),
  ADD KEY `fk_kelas_sekolah1_idx` (`sekolah`),
  ADD KEY `fk_kelas_jurusan1_idx` (`jurusan`);

--
-- Indexes for table `klasifikasi_jam_mapel`
--
ALTER TABLE `klasifikasi_jam_mapel`
  ADD PRIMARY KEY (`id`,`jam_pelajaran`),
  ADD KEY `fk_klasifikasi_jam_mapel_jam_pelajaran1_idx` (`jam_pelajaran`);

--
-- Indexes for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD PRIMARY KEY (`id`,`sekolah`),
  ADD KEY `fk_mata_pelajaran_sekolah1_idx` (`sekolah`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengaturan_nilai`
--
ALTER TABLE `pengaturan_nilai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengawas_ujian`
--
ALTER TABLE `pengawas_ujian`
  ADD PRIMARY KEY (`id`,`guru`),
  ADD KEY `fk_pengawas_ujian_guru1_idx` (`guru`);

--
-- Indexes for table `penilaian`
--
ALTER TABLE `penilaian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peserta_ujian`
--
ALTER TABLE `peserta_ujian`
  ADD PRIMARY KEY (`id`,`ujian`,`siswa`),
  ADD KEY `fk_peserta_ujian_siswa1_idx` (`siswa`),
  ADD KEY `fk_peserta_ujian_ujian1_idx` (`ujian`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_access`
--
ALTER TABLE `role_access`
  ADD PRIMARY KEY (`id`,`guru`,`module`),
  ADD KEY `fk_role_access_guru1_idx` (`guru`),
  ADD KEY `fk_role_access_module1_idx` (`module`);

--
-- Indexes for table `role_has_menu`
--
ALTER TABLE `role_has_menu`
  ADD PRIMARY KEY (`id`,`role`,`menu`,`hak_akses`),
  ADD KEY `fk_role_has_menu_menu1_idx` (`menu`),
  ADD KEY `fk_role_has_menu_role1_idx` (`role`),
  ADD KEY `fk_role_has_menu_hak_akses1_idx` (`hak_akses`);

--
-- Indexes for table `sekolah`
--
ALTER TABLE `sekolah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sekolah_has_user`
--
ALTER TABLE `sekolah_has_user`
  ADD PRIMARY KEY (`id`,`sekolah`,`users`),
  ADD KEY `fk_sekolah_has_user_users1_idx` (`users`),
  ADD KEY `fk_sekolah_has_user_sekolah1_idx` (`sekolah`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`,`sekolah`),
  ADD KEY `fk_siswa_sekolah1_idx` (`sekolah`);

--
-- Indexes for table `siswa_has_angkatan`
--
ALTER TABLE `siswa_has_angkatan`
  ADD PRIMARY KEY (`id`,`siswa`,`angkatan`),
  ADD KEY `fk_siswa_has_angkatan_siswa1_idx` (`siswa`),
  ADD KEY `fk_siswa_has_angkatan_angkatan1_idx` (`angkatan`);

--
-- Indexes for table `siswa_has_jawaban`
--
ALTER TABLE `siswa_has_jawaban`
  ADD PRIMARY KEY (`id`,`ujian`,`siswa`,`soal`,`soal_has_jawaban`,`status_jawaban`),
  ADD KEY `fk_siswa_has_jawaban_soal_has_jawaban1_idx` (`soal_has_jawaban`),
  ADD KEY `fk_siswa_has_jawaban_siswa1_idx` (`siswa`),
  ADD KEY `fk_siswa_has_jawaban_status_jawaban1_idx` (`status_jawaban`),
  ADD KEY `fk_siswa_has_jawaban_ujian1_idx` (`ujian`),
  ADD KEY `fk_siswa_has_jawaban_soal1_idx` (`soal`);

--
-- Indexes for table `siswa_has_kategori_random_soal`
--
ALTER TABLE `siswa_has_kategori_random_soal`
  ADD PRIMARY KEY (`id`,`siswa`,`ujian`,`kategori_soal`,`soal`),
  ADD KEY `fk_siswa_has_kategori_random_soal_siswa1_idx` (`siswa`),
  ADD KEY `fk_siswa_has_kategori_random_soal_kategori_soal1_idx` (`kategori_soal`),
  ADD KEY `fk_siswa_has_kategori_random_soal_soal1_idx` (`soal`),
  ADD KEY `fk_siswa_has_kategori_random_soal_ujian1_idx` (`ujian`);

--
-- Indexes for table `siswa_has_kelas`
--
ALTER TABLE `siswa_has_kelas`
  ADD PRIMARY KEY (`id`,`siswa`,`kelas`),
  ADD KEY `fk_siswa_has_kelas_siswa1_idx` (`siswa`),
  ADD KEY `fk_siswa_has_kelas_kelas1_idx` (`kelas`);

--
-- Indexes for table `siswa_has_ujian`
--
ALTER TABLE `siswa_has_ujian`
  ADD PRIMARY KEY (`id`,`ujian`,`siswa`),
  ADD KEY `fk_siswa_has_ujian_siswa1_idx` (`siswa`),
  ADD KEY `fk_siswa_has_ujian_open_ujian1_idx` (`ujian`);

--
-- Indexes for table `siswa_has_user`
--
ALTER TABLE `siswa_has_user`
  ADD PRIMARY KEY (`id`,`users`,`siswa`),
  ADD KEY `fk_siswa_has_user_siswa1_idx` (`siswa`),
  ADD KEY `fk_siswa_has_user_users1_idx` (`users`);

--
-- Indexes for table `siswa_ujian_time`
--
ALTER TABLE `siswa_ujian_time`
  ADD PRIMARY KEY (`id`,`siswa_has_ujian`),
  ADD KEY `fk_siswa_ujian_time_siswa_has_ujian1_idx` (`siswa_has_ujian`);

--
-- Indexes for table `soal`
--
ALTER TABLE `soal`
  ADD PRIMARY KEY (`id`,`sekolah`,`mata_pelajaran`,`kategori_soal`),
  ADD KEY `fk_soal_kategori_soal1_idx` (`kategori_soal`),
  ADD KEY `fk_soal_sekolah1_idx` (`sekolah`),
  ADD KEY `fk_soal_mata_pelajaran1_idx` (`mata_pelajaran`);

--
-- Indexes for table `soal_has_jawaban`
--
ALTER TABLE `soal_has_jawaban`
  ADD PRIMARY KEY (`id`,`soal`),
  ADD KEY `fk_soal_has_jawaban_soal1_idx` (`soal`);

--
-- Indexes for table `soal_penilaian`
--
ALTER TABLE `soal_penilaian`
  ADD PRIMARY KEY (`id`,`soal`,`penilaian`),
  ADD KEY `fk_soal_penilaian_soal1_idx` (`soal`),
  ADD KEY `fk_soal_penilaian_penilaian1_idx` (`penilaian`);

--
-- Indexes for table `start_ujian`
--
ALTER TABLE `start_ujian`
  ADD PRIMARY KEY (`id`,`time_limit`,`ujian`),
  ADD KEY `fk_start_ujian_ujian1_idx` (`ujian`),
  ADD KEY `fk_start_ujian_time_limit1_idx` (`time_limit`);

--
-- Indexes for table `status_jawaban`
--
ALTER TABLE `status_jawaban`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_limit`
--
ALTER TABLE `time_limit`
  ADD PRIMARY KEY (`id`,`sekolah`),
  ADD KEY `fk_time_limit_sekolah1_idx` (`sekolah`);

--
-- Indexes for table `type_soal`
--
ALTER TABLE `type_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ujian`
--
ALTER TABLE `ujian`
  ADD PRIMARY KEY (`id`,`sekolah`,`users`,`mata_pelajaran`,`kategori_ujian`),
  ADD KEY `fk_ujian_kategori_ujian1_idx` (`kategori_ujian`),
  ADD KEY `fk_ujian_users1_idx` (`users`),
  ADD KEY `fk_ujian_sekolah1_idx` (`sekolah`),
  ADD KEY `fk_ujian_mata_pelajaran1_idx` (`mata_pelajaran`);

--
-- Indexes for table `ujian_has_pengawas`
--
ALTER TABLE `ujian_has_pengawas`
  ADD PRIMARY KEY (`id`,`pengawas_ujian`,`ujian`),
  ADD KEY `fk_ujian_has_pengawas_ujian1_idx` (`ujian`),
  ADD KEY `fk_ujian_has_pengawas_pengawas_ujian1_idx` (`pengawas_ujian`);

--
-- Indexes for table `ujian_has_soal`
--
ALTER TABLE `ujian_has_soal`
  ADD PRIMARY KEY (`id`,`ujian`,`soal`),
  ADD KEY `fk_ujian_has_soal_ujian1_idx` (`ujian`),
  ADD KEY `fk_ujian_has_soal_soal1_idx` (`soal`);

--
-- Indexes for table `ujian_has_soal_limit_keluar`
--
ALTER TABLE `ujian_has_soal_limit_keluar`
  ADD PRIMARY KEY (`id`,`ujian`,`kategori_soal`),
  ADD KEY `fk_ujian_has_soal_limit_keluar_ujian1_idx` (`ujian`),
  ADD KEY `fk_ujian_has_soal_limit_keluar_kategori_soal1_idx` (`kategori_soal`);

--
-- Indexes for table `ujian_poin_lulus`
--
ALTER TABLE `ujian_poin_lulus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ujian_status`
--
ALTER TABLE `ujian_status`
  ADD PRIMARY KEY (`id`,`ujian`),
  ADD KEY `fk_ujian_status_ujian1_idx` (`ujian`);

--
-- Indexes for table `ujian_time`
--
ALTER TABLE `ujian_time`
  ADD PRIMARY KEY (`id`,`ujian`,`time_limit`),
  ADD KEY `fk_ujian_time_ujian2_idx` (`ujian`),
  ADD KEY `fk_ujian_time_time_limit1_idx` (`time_limit`);

--
-- Indexes for table `ujian_time_jadwal`
--
ALTER TABLE `ujian_time_jadwal`
  ADD PRIMARY KEY (`id`,`ujian`,`jam_pelajaran`,`klasifikasi_jam_mapel`),
  ADD KEY `fk_ujian_time_jadwal_klasifikasi_jam_mapel1_idx` (`klasifikasi_jam_mapel`),
  ADD KEY `fk_ujian_time_jadwal_ujian1_idx` (`ujian`),
  ADD KEY `fk_ujian_time_jadwal_jam_pelajaran1_idx` (`jam_pelajaran`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`,`hak_akses`),
  ADD KEY `fk_users_hak_akses1_idx` (`hak_akses`);

--
-- Indexes for table `user_has_menu`
--
ALTER TABLE `user_has_menu`
  ADD PRIMARY KEY (`id`,`users`,`menu`),
  ADD KEY `fk_user_has_menu_menu1_idx` (`menu`),
  ADD KEY `fk_user_has_menu_users1_idx` (`users`);

--
-- Indexes for table `user_has_profile`
--
ALTER TABLE `user_has_profile`
  ADD PRIMARY KEY (`id`,`users`),
  ADD KEY `fk_user_has_profile_users1_idx` (`users`);

--
-- Indexes for table `user_has_time_use`
--
ALTER TABLE `user_has_time_use`
  ADD PRIMARY KEY (`id`,`users`),
  ADD KEY `fk_user_has_time_use_users1_idx` (`users`);

--
-- Indexes for table `user_social_media`
--
ALTER TABLE `user_social_media`
  ADD PRIMARY KEY (`id`,`users`),
  ADD KEY `fk_user_social_media_users1_idx` (`users`);

--
-- Indexes for table `user_status`
--
ALTER TABLE `user_status`
  ADD PRIMARY KEY (`id`,`users`),
  ADD KEY `fk_user_status_users1_idx` (`users`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `angkatan`
--
ALTER TABLE `angkatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bobot_nilai_kategori`
--
ALTER TABLE `bobot_nilai_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `day`
--
ALTER TABLE `day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `guru_has_mapel`
--
ALTER TABLE `guru_has_mapel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `guru_has_user`
--
ALTER TABLE `guru_has_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hak_akses`
--
ALTER TABLE `hak_akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jam_pelajaran`
--
ALTER TABLE `jam_pelajaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jam_pelajaran_has_day`
--
ALTER TABLE `jam_pelajaran_has_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kategori_soal`
--
ALTER TABLE `kategori_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `kategori_ujian`
--
ALTER TABLE `kategori_ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `klasifikasi_jam_mapel`
--
ALTER TABLE `klasifikasi_jam_mapel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengaturan_nilai`
--
ALTER TABLE `pengaturan_nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengawas_ujian`
--
ALTER TABLE `pengawas_ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penilaian`
--
ALTER TABLE `penilaian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `peserta_ujian`
--
ALTER TABLE `peserta_ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_access`
--
ALTER TABLE `role_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role_has_menu`
--
ALTER TABLE `role_has_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sekolah`
--
ALTER TABLE `sekolah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sekolah_has_user`
--
ALTER TABLE `sekolah_has_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `siswa_has_angkatan`
--
ALTER TABLE `siswa_has_angkatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `siswa_has_jawaban`
--
ALTER TABLE `siswa_has_jawaban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `siswa_has_kategori_random_soal`
--
ALTER TABLE `siswa_has_kategori_random_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `siswa_has_kelas`
--
ALTER TABLE `siswa_has_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `siswa_has_ujian`
--
ALTER TABLE `siswa_has_ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `siswa_has_user`
--
ALTER TABLE `siswa_has_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `siswa_ujian_time`
--
ALTER TABLE `siswa_ujian_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `soal`
--
ALTER TABLE `soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `soal_has_jawaban`
--
ALTER TABLE `soal_has_jawaban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=764;

--
-- AUTO_INCREMENT for table `soal_penilaian`
--
ALTER TABLE `soal_penilaian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `start_ujian`
--
ALTER TABLE `start_ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status_jawaban`
--
ALTER TABLE `status_jawaban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `time_limit`
--
ALTER TABLE `time_limit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `type_soal`
--
ALTER TABLE `type_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ujian`
--
ALTER TABLE `ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `ujian_has_pengawas`
--
ALTER TABLE `ujian_has_pengawas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ujian_has_soal`
--
ALTER TABLE `ujian_has_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `ujian_has_soal_limit_keluar`
--
ALTER TABLE `ujian_has_soal_limit_keluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ujian_poin_lulus`
--
ALTER TABLE `ujian_poin_lulus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ujian_status`
--
ALTER TABLE `ujian_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ujian_time`
--
ALTER TABLE `ujian_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ujian_time_jadwal`
--
ALTER TABLE `ujian_time_jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_has_menu`
--
ALTER TABLE `user_has_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_has_profile`
--
ALTER TABLE `user_has_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_has_time_use`
--
ALTER TABLE `user_has_time_use`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_social_media`
--
ALTER TABLE `user_social_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_status`
--
ALTER TABLE `user_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `angkatan`
--
ALTER TABLE `angkatan`
  ADD CONSTRAINT `fk_angkatan_sekolah1` FOREIGN KEY (`sekolah`) REFERENCES `sekolah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `guru`
--
ALTER TABLE `guru`
  ADD CONSTRAINT `fk_guru_sekolah1` FOREIGN KEY (`sekolah`) REFERENCES `sekolah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `guru_has_mapel`
--
ALTER TABLE `guru_has_mapel`
  ADD CONSTRAINT `fk_guru_has_mapel_guru1` FOREIGN KEY (`guru`) REFERENCES `guru` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_guru_has_mapel_mata_pelajaran1` FOREIGN KEY (`mata_pelajaran`) REFERENCES `mata_pelajaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `guru_has_user`
--
ALTER TABLE `guru_has_user`
  ADD CONSTRAINT `fk_guru_has_user_guru1` FOREIGN KEY (`guru`) REFERENCES `guru` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_guru_has_user_users1` FOREIGN KEY (`users`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jam_pelajaran`
--
ALTER TABLE `jam_pelajaran`
  ADD CONSTRAINT `fk_jam_pelajaran_sekolah1` FOREIGN KEY (`sekolah`) REFERENCES `sekolah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jam_pelajaran_has_day`
--
ALTER TABLE `jam_pelajaran_has_day`
  ADD CONSTRAINT `fk_jam_pelajaran_has_day_day1` FOREIGN KEY (`day`) REFERENCES `day` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jam_pelajaran_has_day_jam_pelajaran1` FOREIGN KEY (`jam_pelajaran`) REFERENCES `jam_pelajaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD CONSTRAINT `fk_jurusan_sekolah1` FOREIGN KEY (`sekolah`) REFERENCES `sekolah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kategori_soal`
--
ALTER TABLE `kategori_soal`
  ADD CONSTRAINT `fk_kategori_soal_mata_pelajaran1` FOREIGN KEY (`mata_pelajaran`) REFERENCES `mata_pelajaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kategori_soal_sekolah1` FOREIGN KEY (`sekolah`) REFERENCES `sekolah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kelas`
--
ALTER TABLE `kelas`
  ADD CONSTRAINT `fk_kelas_jurusan1` FOREIGN KEY (`jurusan`) REFERENCES `jurusan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kelas_sekolah1` FOREIGN KEY (`sekolah`) REFERENCES `sekolah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `klasifikasi_jam_mapel`
--
ALTER TABLE `klasifikasi_jam_mapel`
  ADD CONSTRAINT `fk_klasifikasi_jam_mapel_jam_pelajaran1` FOREIGN KEY (`jam_pelajaran`) REFERENCES `jam_pelajaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD CONSTRAINT `fk_mata_pelajaran_sekolah1` FOREIGN KEY (`sekolah`) REFERENCES `sekolah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pengawas_ujian`
--
ALTER TABLE `pengawas_ujian`
  ADD CONSTRAINT `fk_pengawas_ujian_guru1` FOREIGN KEY (`guru`) REFERENCES `guru` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `peserta_ujian`
--
ALTER TABLE `peserta_ujian`
  ADD CONSTRAINT `fk_peserta_ujian_siswa1` FOREIGN KEY (`siswa`) REFERENCES `siswa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_peserta_ujian_ujian1` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `role_access`
--
ALTER TABLE `role_access`
  ADD CONSTRAINT `fk_role_access_guru1` FOREIGN KEY (`guru`) REFERENCES `guru` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_role_access_module1` FOREIGN KEY (`module`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `role_has_menu`
--
ALTER TABLE `role_has_menu`
  ADD CONSTRAINT `fk_role_has_menu_hak_akses1` FOREIGN KEY (`hak_akses`) REFERENCES `hak_akses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_role_has_menu_menu1` FOREIGN KEY (`menu`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_role_has_menu_role1` FOREIGN KEY (`role`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sekolah_has_user`
--
ALTER TABLE `sekolah_has_user`
  ADD CONSTRAINT `fk_sekolah_has_user_sekolah1` FOREIGN KEY (`sekolah`) REFERENCES `sekolah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sekolah_has_user_users1` FOREIGN KEY (`users`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `fk_siswa_sekolah1` FOREIGN KEY (`sekolah`) REFERENCES `sekolah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `siswa_has_angkatan`
--
ALTER TABLE `siswa_has_angkatan`
  ADD CONSTRAINT `fk_siswa_has_angkatan_angkatan1` FOREIGN KEY (`angkatan`) REFERENCES `angkatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siswa_has_angkatan_siswa1` FOREIGN KEY (`siswa`) REFERENCES `siswa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `siswa_has_jawaban`
--
ALTER TABLE `siswa_has_jawaban`
  ADD CONSTRAINT `fk_siswa_has_jawaban_siswa1` FOREIGN KEY (`siswa`) REFERENCES `siswa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siswa_has_jawaban_soal1` FOREIGN KEY (`soal`) REFERENCES `soal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siswa_has_jawaban_soal_has_jawaban1` FOREIGN KEY (`soal_has_jawaban`) REFERENCES `soal_has_jawaban` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siswa_has_jawaban_status_jawaban1` FOREIGN KEY (`status_jawaban`) REFERENCES `status_jawaban` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siswa_has_jawaban_ujian1` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `siswa_has_kategori_random_soal`
--
ALTER TABLE `siswa_has_kategori_random_soal`
  ADD CONSTRAINT `fk_siswa_has_kategori_random_soal_kategori_soal1` FOREIGN KEY (`kategori_soal`) REFERENCES `kategori_soal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siswa_has_kategori_random_soal_siswa1` FOREIGN KEY (`siswa`) REFERENCES `siswa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siswa_has_kategori_random_soal_soal1` FOREIGN KEY (`soal`) REFERENCES `soal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siswa_has_kategori_random_soal_ujian1` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `siswa_has_kelas`
--
ALTER TABLE `siswa_has_kelas`
  ADD CONSTRAINT `fk_siswa_has_kelas_kelas1` FOREIGN KEY (`kelas`) REFERENCES `kelas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siswa_has_kelas_siswa1` FOREIGN KEY (`siswa`) REFERENCES `siswa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `siswa_has_ujian`
--
ALTER TABLE `siswa_has_ujian`
  ADD CONSTRAINT `fk_siswa_has_ujian_open_ujian1` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siswa_has_ujian_siswa1` FOREIGN KEY (`siswa`) REFERENCES `siswa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `siswa_has_user`
--
ALTER TABLE `siswa_has_user`
  ADD CONSTRAINT `fk_siswa_has_user_siswa1` FOREIGN KEY (`siswa`) REFERENCES `siswa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_siswa_has_user_users1` FOREIGN KEY (`users`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `siswa_ujian_time`
--
ALTER TABLE `siswa_ujian_time`
  ADD CONSTRAINT `fk_siswa_ujian_time_siswa_has_ujian1` FOREIGN KEY (`siswa_has_ujian`) REFERENCES `siswa_has_ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `soal`
--
ALTER TABLE `soal`
  ADD CONSTRAINT `fk_soal_kategori_soal1` FOREIGN KEY (`kategori_soal`) REFERENCES `kategori_soal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_soal_mata_pelajaran1` FOREIGN KEY (`mata_pelajaran`) REFERENCES `mata_pelajaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_soal_sekolah1` FOREIGN KEY (`sekolah`) REFERENCES `sekolah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `soal_has_jawaban`
--
ALTER TABLE `soal_has_jawaban`
  ADD CONSTRAINT `fk_soal_has_jawaban_soal1` FOREIGN KEY (`soal`) REFERENCES `soal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `soal_penilaian`
--
ALTER TABLE `soal_penilaian`
  ADD CONSTRAINT `fk_soal_penilaian_penilaian1` FOREIGN KEY (`penilaian`) REFERENCES `penilaian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_soal_penilaian_soal1` FOREIGN KEY (`soal`) REFERENCES `soal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `start_ujian`
--
ALTER TABLE `start_ujian`
  ADD CONSTRAINT `fk_start_ujian_time_limit1` FOREIGN KEY (`time_limit`) REFERENCES `time_limit` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_start_ujian_ujian1` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `time_limit`
--
ALTER TABLE `time_limit`
  ADD CONSTRAINT `fk_time_limit_sekolah1` FOREIGN KEY (`sekolah`) REFERENCES `sekolah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ujian`
--
ALTER TABLE `ujian`
  ADD CONSTRAINT `fk_ujian_kategori_ujian1` FOREIGN KEY (`kategori_ujian`) REFERENCES `kategori_ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ujian_mata_pelajaran1` FOREIGN KEY (`mata_pelajaran`) REFERENCES `mata_pelajaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ujian_sekolah1` FOREIGN KEY (`sekolah`) REFERENCES `sekolah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ujian_users1` FOREIGN KEY (`users`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ujian_has_pengawas`
--
ALTER TABLE `ujian_has_pengawas`
  ADD CONSTRAINT `fk_ujian_has_pengawas_pengawas_ujian1` FOREIGN KEY (`pengawas_ujian`) REFERENCES `pengawas_ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ujian_has_pengawas_ujian1` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ujian_has_soal`
--
ALTER TABLE `ujian_has_soal`
  ADD CONSTRAINT `fk_ujian_has_soal_soal1` FOREIGN KEY (`soal`) REFERENCES `soal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ujian_has_soal_ujian1` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ujian_has_soal_limit_keluar`
--
ALTER TABLE `ujian_has_soal_limit_keluar`
  ADD CONSTRAINT `fk_ujian_has_soal_limit_keluar_kategori_soal1` FOREIGN KEY (`kategori_soal`) REFERENCES `kategori_soal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ujian_has_soal_limit_keluar_ujian1` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ujian_status`
--
ALTER TABLE `ujian_status`
  ADD CONSTRAINT `fk_ujian_status_ujian1` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ujian_time`
--
ALTER TABLE `ujian_time`
  ADD CONSTRAINT `fk_ujian_time_time_limit1` FOREIGN KEY (`time_limit`) REFERENCES `time_limit` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ujian_time_ujian2` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ujian_time_jadwal`
--
ALTER TABLE `ujian_time_jadwal`
  ADD CONSTRAINT `fk_ujian_time_jadwal_jam_pelajaran1` FOREIGN KEY (`jam_pelajaran`) REFERENCES `jam_pelajaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ujian_time_jadwal_klasifikasi_jam_mapel1` FOREIGN KEY (`klasifikasi_jam_mapel`) REFERENCES `klasifikasi_jam_mapel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ujian_time_jadwal_ujian1` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_hak_akses1` FOREIGN KEY (`hak_akses`) REFERENCES `hak_akses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_has_menu`
--
ALTER TABLE `user_has_menu`
  ADD CONSTRAINT `fk_user_has_menu_menu1` FOREIGN KEY (`menu`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_menu_users1` FOREIGN KEY (`users`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_has_profile`
--
ALTER TABLE `user_has_profile`
  ADD CONSTRAINT `fk_user_has_profile_users1` FOREIGN KEY (`users`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_has_time_use`
--
ALTER TABLE `user_has_time_use`
  ADD CONSTRAINT `fk_user_has_time_use_users1` FOREIGN KEY (`users`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_social_media`
--
ALTER TABLE `user_social_media`
  ADD CONSTRAINT `fk_user_social_media_users1` FOREIGN KEY (`users`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_status`
--
ALTER TABLE `user_status`
  ADD CONSTRAINT `fk_user_status_users1` FOREIGN KEY (`users`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
