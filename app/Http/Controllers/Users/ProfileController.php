<?php

namespace App\Http\Controllers\Users;

use Exception;
use App\Models\Guru;
use App\Helpers\User;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Jurusan;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use App\Models\MataPelajaran;
use App\Models\User_has_profile;
use App\Models\User_social_media;
use App\Models\User_social_medial;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public $limit = 5;
    public $tb = "users";
    public $tb_sekolah = "sekolah";
    public $tb_jurusan = "jurusan";
    public $tb_mapel = "mata_pelajaran";
    public $tb_guru_mapel = "guru_has_mapel";
    public $tb_user_profile = "user_has_profile";
    public $tb_user_media = "user_social_media";
    public $tb_user_access = "hak_akses";
    public $keyword = "";
    public $sekolah_id = '';
    public $link_get_url = "menu=profile&child=user";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/url.js'),
            'js-2' => asset('assets/js/message.js'),
            'js-3' => asset('assets/js/validation.js'),
            'js-4' => asset('assets/js/controllers/profile.js'),
        );
    }

    public function getModuleName()
    {
        return "profile";
    }

    public function edit()
    {
        $content['data'] = $this->getDetailProfile();
        // echo '<pre>';
        // print_r($content);
        // die;
        $content['module'] = $this->getModuleName();
        $view = view("profile.profile", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Profile';
        $dataput['title_top'] = 'Profile';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getDetailProfile()
    {
        $data = DB::table($this->tb)
            ->leftJoin($this->tb_user_profile, $this->tb . '.id', '=', $this->tb_user_profile . '.' . $this->tb)
            ->leftJoin($this->tb_user_media, $this->tb . '.id', '=', $this->tb_user_media . '.' . $this->tb)
            ->join($this->tb_user_access, $this->tb . '.hak_akses', '=', $this->tb_user_access . '.id')
            ->select(
                $this->tb . ".*",
                $this->tb_user_profile . ".id as profile_id",
                $this->tb_user_profile . ".firstname",
                $this->tb_user_profile . ".lastname",
                $this->tb_user_profile . ".foto",
                $this->tb_user_profile . ".tentang",
                $this->tb_user_media . ".id as media_id",
                $this->tb_user_media . ".facebook",
                $this->tb_user_media . ".twitter",
                $this->tb_user_media . ".instagram",
                $this->tb_user_media . ".wa",
                $this->tb_user_access . ".hak_akses as access"
            )
            ->where($this->tb . '.id', '=', session('user_id'))
            ->orderBy($this->tb . '.id', 'desc')
            ->first();

        return $data;
    }

    public function submit(Request $req)
    {
        $data = json_decode($req['data']);
        $is_valid = false;
        $message = "";

        DB::beginTransaction();
        try {

            $profile_id = $data->profile_id;

            $push_profile = array();
            $push_profile['firstname'] = $data->firstname;
            $push_profile['lastname'] = $data->lastname;
            $push_profile['tentang'] = $data->tentang;

            if ($profile_id == '') {
                $push_profile['users'] = session('user_id');
                $push_profile['createddate'] = date('Y-m-d H:i:s');
                $push_profile['createdby'] = session('user_id');
                User_has_profile::insert($push_profile);
            } else {
                $push_profile['updateddate'] = date('Y-m-d H:i:s');
                $push_profile['updatedby'] = session('user_id');
                DB::table($this->tb_user_profile)->where('id', '=', $profile_id)->update($push_profile);
            }

            $media_id = $data->media_id;

            $push_media = array();
            $push_media['facebook'] = $data->facebook;
            $push_media['twitter'] = $data->twitter;
            $push_media['instagram'] = $data->instagram;
            $push_media['wa'] = $data->wa;

            if ($media_id == '') {
                $push_media['users'] = session('user_id');
                $push_media['createddate'] = date('Y-m-d H:i:s');
                $push_media['createdby'] = session('user_id');
                User_social_media::insert($push_media);
            } else {
                $push_media['updateddate'] = date('Y-m-d H:i:s');
                $push_media['updatedby'] = session('user_id');
                DB::table($this->tb_user_media)->where('id', '=', $media_id)->update($push_media);
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }

        return json_encode(array('is_valid' => $is_valid, 'message' => $message));
    }

    public function submitPassword(Request $req)
    {
        $is_valid = false;
        $message = "";


        DB::beginTransaction();
        try {
            $user_id = session('user_id');
            $data_user = DB::table($this->tb)->where('id', '=', $user_id)
                ->where('password', '=', $req['password_lama'])->first();

            if (!empty($data_user)) {
                $push['password'] = $req['password_baru'];
                DB::table($this->tb)->where('id', '=', $user_id)->update($push);
            } else {
                $message = 'Password lama tidak ditemukan';
            }

            DB::commit();

            if ($message == '') {
                $is_valid = true;
            }
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }

        return json_encode(array('is_valid' => $is_valid, 'message' => $message));
    }

    public function submitFoto(Request $req)
    {
        $is_valid = false;
        $message = "";


        DB::beginTransaction();
        try {
            $profile_id = $req['profile_id'];
            $push['foto'] = $req['image_src'];

            DB::table($this->tb_user_profile)->where('id', '=', $profile_id)->update($push);
            DB::commit();
            $is_valid = true;
            if ($is_valid) {
                session(array(
                    'foto_profile' => $req['image_src']
                ));
            }
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }

        return json_encode(array('is_valid' => $is_valid, 'message' => $message));
    }

    public function changePassword(Request $req)
    {
        $content = array();
        return view("profile.formchangepassword", $content);
    }
}
