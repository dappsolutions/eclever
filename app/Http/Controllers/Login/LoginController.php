<?php

namespace App\Http\Controllers\Login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public $tb_user = "users";
    public $tb_hak_akses = "hak_akses";
    public $tb_guru = 'guru';
    public $tb_siswa = 'siswa';
    public $tb_sekolah = 'sekolah';
    public $tb_guru_has_user = 'guru_has_user';
    public $tb_siswa_has_user = 'siswa_has_user';
    public $tb_sekolah_has_user = 'sekolah_has_user';
    public $tb_user_profile = 'user_has_profile';

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function index()
    {
        $content['token'] = csrf_token();
        return view("login.login", $content);
    }

    public function sign_out()
    {
        session()->flush();
        return redirect()->to('/')->with('message', 'Anda telah keluar dari aplikasi')->send();
    }

    public function sign_in(Request $req)
    {

        session()->flush();
        $data = DB::table($this->tb_user . ' as us')
            ->select(
                'us.*',
                'hk.' . $this->tb_hak_akses . ' as access',
                $this->tb_sekolah . '.id as sekolah',
                'tb_skl.id as sekolah_dua',
                $this->tb_sekolah_has_user . '.sekolah as sekolah_tiga',
                'up.foto as foto_profile'
            )
            ->join($this->tb_hak_akses . ' as hk', 'us.' . $this->tb_hak_akses, '=', 'hk.id')
            ->leftJoin($this->tb_guru_has_user, 'us.id', '=', $this->tb_guru_has_user . '.' . $this->tb_user)
            ->leftJoin($this->tb_guru, $this->tb_guru_has_user . '.' . $this->tb_guru, '=', $this->tb_guru . '.id')
            ->leftJoin($this->tb_sekolah, $this->tb_guru . '.' . $this->tb_sekolah, '=', $this->tb_sekolah . '.id')
            ->leftJoin($this->tb_siswa_has_user, 'us.id', '=', $this->tb_siswa_has_user . '.' . $this->tb_user)
            ->leftJoin($this->tb_siswa, $this->tb_siswa_has_user . '.' . $this->tb_siswa, '=', $this->tb_siswa . '.id')
            ->leftJoin($this->tb_sekolah . ' as tb_skl', $this->tb_siswa . '.' . $this->tb_sekolah, '=', 'tb_skl.id')
            ->leftJoin($this->tb_sekolah_has_user, 'us.id', '=', $this->tb_sekolah_has_user . '.' . $this->tb_user)
            ->leftJoin($this->tb_user_profile . ' as up', 'up.users', '=', 'us.id')
            ->where('us.username', '=', $req->username)
            ->where('us.password', '=', $req->password)
            ->where('us.deleted', '=', '0')
            ->get();

        $is_valid = false;
        if (!empty($data->toArray())) {
            $is_valid = true;
            $data = $data->first();
            $sekolah_id = $this->getSekolahId($data);

            $foto_profile = $data->foto_profile == '' ? asset('assets/img/no_picture.jpg') : $data->foto_profile;
            session(array(
                'user_id' => $data->id,
                'username' => $data->username,
                'access' => strtolower($data->access),
                'sekolah_id' => $sekolah_id,
                'foto_profile' => $foto_profile
            ));
        }
        return json_encode(array(
            'is_valid' => $is_valid
        ));
    }

    public function getSekolahId($data)
    {
        $sekolah_id = '';
        if ($data->sekolah != '') {
            $sekolah_id = $data->sekolah;
        }

        if ($data->sekolah_dua != '') {
            $sekolah_id = $data->sekolah_dua;
        }

        if ($data->sekolah_tiga != '') {
            $sekolah_id = $data->sekolah_tiga;
        }

        return $sekolah_id;
    }
}
