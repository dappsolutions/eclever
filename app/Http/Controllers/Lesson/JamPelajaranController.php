<?php

namespace App\Http\Controllers\Lesson;

use Exception;
use App\Models\Guru;
use App\Helpers\User;
use App\Models\Siswa;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use App\Models\MataPelajaran;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Day;
use App\Models\Jam_pelajaran;
use App\Models\Jam_pelajaran_has_day;
use App\Models\Jurusan;
use App\Models\Klasifikasi_jam_mapel;
use App\Models\Time_limit;

class JamPelajaranController extends Controller
{
    public $limit = 5;
    public $tb = "jam_pelajaran";
    public $tb_sekolah = "sekolah";
    public $tb_mapel = "mata_pelajaran";
    public $tb_guru_mapel = "guru_has_mapel";
    public $tb_klasifikasi_jam = "klasifikasi_jam_mapel";
    public $tb_day = "day";
    public $tb_mapel_day = "jam_pelajaran_has_day";
    public $keyword = "";
    public $link_get_url = "menu=jam_pelajaran&child=lesson";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/url.js'),
            'js-2' => asset('assets/js/message.js'),
            'js-3' => asset('assets/js/validation.js'),
            'css-4' => asset('assets/plugins/components/timepicker/bootstrap-timepicker.min.css'),
            'js-5' => asset('assets/plugins/components/timepicker/bootstrap-timepicker.min.js'),
            'js-7' => asset('assets/lib/jquery.steps/jquery.steps.js'),
            'js-8' => asset('assets/lib/parsleyjs/parsley.js'),
            'js-9' => asset('assets/js/controllers/jam_pelajaran.js'),
        );
    }

    public function getModuleName()
    {
        return "jam pelajaran";
    }

    public function index()
    {
        $data = $this->getListData('?' . $this->link_get_url);

        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $view = view("jam_pelajaran.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Jam Pelajaran';
        $dataput['title_top'] = 'Jam Pelajaran';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function add()
    {
        $data = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $content['sekolah'] = session('sekolah_id');
        $content['data_sekolah'] = json_decode($data);
        $content['module'] = $this->getModuleName();
        $view = view("jam_pelajaran.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Jam Pelajaran';
        $dataput['title_top'] = 'Jam Pelajaran';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListDetailJamMapel($jam_mapel_id)
    {
        $data = Klasifikasi_jam_mapel::where('deleted', '=', '0')
            ->where('jam_pelajaran', '=', $jam_mapel_id)
            ->get();

        return $data->toArray();
    }

    public function getListDayJam($jam_mapel_id)
    {
        $data = Day::leftJoin($this->tb_mapel_day . ' as mpd', function ($join) use ($jam_mapel_id) {
            $join->on($this->tb_day . '.id', '=', 'mpd.day')
                ->where('mpd.jam_pelajaran', '=', $jam_mapel_id);
        })
            ->select(
                $this->tb_day . '.*',
                'mpd.id as mapel_day_id',
                'mpd.choose'
            )
            ->get();

        return $data->toArray();
    }

    public function edit($id)
    {
        $data = Jam_pelajaran::where('id', '=', $id)->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $data_klasifikasi_jam = $this->getListDetailJamMapel($data->id);
        $data_day_jam = $this->getListDayJam($data->id);

        $content = $data->toArray();
        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['data_day'] = $data_day_jam;
        $content['module'] = $this->getModuleName();

        $content_klasifikasi['data'] = $data_klasifikasi_jam;
        $content['list_genarate_jam'] = view('jam_pelajaran.listgeneratejam', $content_klasifikasi);
        $view = view("jam_pelajaran.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Jam Pelajaran';
        $dataput['title_top'] = 'Jam Pelajaran';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function detail($id)
    {
        $data = Jam_pelajaran::where('id', '=', $id)->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $data_klasifikasi_jam = $this->getListDetailJamMapel($data->id);
        $data_day_jam = $this->getListDayJam($data->id);

        $content = $data->toArray();
        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['data_day'] = $data_day_jam;
        $content['module'] = $this->getModuleName();
        $content['disabled'] = 'disabled';

        $content_klasifikasi['data'] = $data_klasifikasi_jam;
        $content['list_genarate_jam'] = view('jam_pelajaran.listgeneratejam', $content_klasifikasi);
        $view = view("jam_pelajaran.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Jam Pelajaran';
        $dataput['title_top'] = 'Jam Pelajaran';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListData($with_path = "")
    {
        $data = DB::table($this->tb)
            ->join($this->tb_sekolah, $this->tb . '.' . $this->tb_sekolah, '=', $this->tb_sekolah . '.id')
            ->select($this->tb . ".*", $this->tb_sekolah . ".nama_sekolah")
            ->where(function ($query) {
                $query->where($this->tb . '.deleted', '=', '0')
                    ->where($this->tb_sekolah . '.deleted', '=', '0');
            })->where(function ($query) {
                $query->Where($this->tb . '.lama', 'like', '%' . $this->keyword . '%')
                    ->Where($this->tb . '.judul', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_sekolah . '.nama_sekolah', 'like', '%' . $this->keyword . '%');
            })
            ->orderBy($this->tb . '.id', 'desc')
            ->paginate($this->limit);

        if (session('access') != 'superadmin') {
            $data->where($this->tb . '.sekolah', '=', session('sekolah_id'));
        }

        if ($with_path != '') {
            $data->withPath($with_path);
        }

        return $data;
    }

    public function cari(Request $req)
    {
        $this->keyword = trim($req->keyword);
        $data = $this->getListData('cari?keyword=' . $this->keyword . '&' . $this->link_get_url);
        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $content['keyword'] = $this->keyword;
        $view = view("jam_pelajaran.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Jam Pelajaran';
        $dataput['title_top'] = 'Jam Pelajaran';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getPostInput($param)
    {
        $data = array();
        $data['lama'] = $param->lama;
        $data['judul'] = $param->judul;
        $data['sekolah'] = $param->sekolah;

        return $data;
    }

    public function submit(Request $req)
    {
        $data = json_decode($req['data']);
        $id = $data->id;
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push = $this->getPostInput($data);
            if ($id == '') {
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                $id = Jam_pelajaran::insertGetId($push);
            } else {
                $push['updateddate'] = date('Y-m-d H:i:s');
                $push['updatedby'] = session('user_id');
                DB::table($this->tb)->where('id', '=', $id)->update($push);
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid, 'id' => $id));
    }

    public function delete(Request $req)
    {
        $id = $req['id'];
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push['deleted'] = 1;
            DB::table($this->tb)->where('id', '=', $id)->update($push);
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function getPostInputGuruHasMapel($param)
    {
        $data = array();
        $data['guru'] = $param->guru;
        $data['mata_pelajaran'] = $param->mapel_id;
        $data['handled'] = $param->checked;

        return $data;
    }

    public function changeMapel(Request $req)
    {
        $data = json_decode($req['data']);
        $is_valid = false;

        DB::beginTransaction();
        try {

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $guru_mapel_id = $value->guru_mapel_id;
                    $push = $this->getPostInputGuruHasMapel($value);
                    if ($guru_mapel_id == '') {
                        DB::table($this->tb_guru_mapel)->insert($push);
                    } else {
                        DB::table($this->tb_guru_mapel)->where('id', '=', $guru_mapel_id)->update($push);
                    }
                }
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function generateJam(Request $req)
    {
        $jam = $req['jam'];
        $durasi = $req['durasi'];
        $jumlah_jam = $req['jumlah_jam'];

        list($hours, $minute) = explode(':', $jam);


        // echo date('H:i', strtotime($jam . ' + ' . $durasi . ' minute'));
        $data_time = array();
        $jam_awal = $jam;
        for ($i = 0; $i < $jumlah_jam; $i++) {
            $jam_akhir = date('H:i', strtotime($jam_awal . ' + ' . $durasi . ' minute'));
            $push['jam_awal'] = $jam_awal;
            $push['jam_akhir'] = $jam_akhir;
            $jam_awal = $jam_akhir;
            array_push($data_time, $push);
        }
        $data['data'] = $data_time;
        return view('jam_pelajaran.listgeneratejam', $data);
    }

    public function submitKlasifikasi(Request $req)
    {
        $data = json_decode($req['data']);

        // echo '<pre>';
        // print_r($data);
        // die;
        $is_valid = false;
        $message = "";

        DB::beginTransaction();
        try {

            if (!empty($data)) {
                $push = array();
                $push['jam_set'] = $data->jam_set;
                $push['jumlah_jam'] = $data->jumlah_jam;
                DB::table($this->tb)->where('id', '=', $data->jam_mapel_id)->update($push);

                if (!empty($data->data_generate)) {
                    DB::table($this->tb_klasifikasi_jam)->where('jam_pelajaran', '=', $data->jam_mapel_id)->update(array(
                        'deleted' => 1
                    ));
                    foreach ($data->data_generate as $key => $value) {
                        $push = array();
                        $push['jam_pelajaran'] = $data->jam_mapel_id;
                        $push['jam_awal'] = $value->jam_awal;
                        $push['jam_akhir'] = $value->jam_akhir;
                        Klasifikasi_jam_mapel::insert($push);
                    }
                }
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid, 'message' => $message));
    }

    public function setMapelDay(Request $req)
    {
        $mapel_day_id = $req['mapel_day_id'];
        $day = $req['day'];
        $jam_id = $req['jam_id'];

        // echo '<pre>';
        // print_r($_POST);
        // die;

        $is_valid = false;
        $message = "";

        DB::beginTransaction();
        try {

            if ($mapel_day_id == '') {
                $push = array();
                $push['day'] = $day;
                $push['jam_pelajaran'] = $jam_id;
                $push['choose'] = $req['choose'];
                Jam_pelajaran_has_day::insert($push);
            } else {
                $push['choose'] = $req['choose'];
                DB::table($this->tb_mapel_day)->where('id', '=', $mapel_day_id)->update($push);
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid, 'message' => $message));
    }
}
