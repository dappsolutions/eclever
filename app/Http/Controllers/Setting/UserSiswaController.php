<?php

namespace App\Http\Controllers\Setting;

use Exception;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Guru;
use App\Models\Guru_has_user;
use App\Models\Jurusan;
use App\Models\Siswa;
use App\Models\Siswa_has_user;
use App\Models\User;

class UserSiswaController extends Controller
{
    public $limit = 5;
    public $tb = "siswa_has_user";
    public $tb_sekolah = "sekolah";
    public $tb_siswa = "siswa";
    public $tb_mapel = "mata_pelajaran";
    public $tb_user = "users";
    public $keyword = "";
    public $sekolah_id = '';
    public $link_get_url = "menu=user_siswa&child=setting";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/url.js'),
            'js-2' => asset('assets/js/message.js'),
            'js-3' => asset('assets/js/validation.js'),
            'js-4' => asset('assets/lib/select2/js/select2.min.js'),
            'js-5' => asset('assets/js/controllers/user_siswa.js'),
        );
    }

    public function getModuleName()
    {
        return "user siswa";
    }

    public function index()
    {
        $data = $this->getListData('?' . $this->link_get_url);

        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $view = view("user_siswa.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'User Siswa';
        $dataput['title_top'] = 'User Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListSiswa(Request $req)
    {
        $sekolah = $req['sekolah'];
        $data_siswa = Siswa::where('deleted', '=', 0)
            ->where('sekolah', '=', $sekolah)
            ->get();
        $content['data_siswa'] = $data_siswa->toArray();
        return view('user_siswa.list_siswa', $content);
    }

    public function add()
    {
        $data = Sekolah::where($this->tb_sekolah . '.deleted', '=', '0')
            ->select($this->tb_sekolah . '.*')
            ->whereNotNull($this->tb_sekolah . '.approve_by')
            ->get();
        $this->sekolah_id = session('sekolah_id');

        $data_siswa = array();

        if (!empty($data->toArray())) {
            $data_siswa = Siswa::where('deleted', '=', 0)
                ->where('sekolah', '=', $this->sekolah_id == ''
                    ? $data->toArray()[0]['id'] : $this->sekolah_id)
                ->get();
            $data_siswa = $data_siswa->toArray();
        }

        $content['sekolah'] = session('sekolah_id');
        $content['data_sekolah'] = json_decode($data);
        $content['data_siswa'] = $data_siswa;
        $content['module'] = $this->getModuleName();
        $view = view("user_siswa.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'User Siswa';
        $dataput['title_top'] = 'User Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function edit($id)
    {
        $data = Siswa_has_user::where($this->tb . '.id', '=', $id)
            ->select($this->tb . '.*', 'us.username', 'us.password')
            ->join($this->tb_user . ' as us', 'us.id', '=', $this->tb . '.users')
            ->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $this->sekolah_id = $data->sekolah;
        $content = $data->toArray();
        $content['sekolah'] = $this->getDetailSekolahSiswa($data['siswa']);
        $data_siswa = array();

        if ($content['sekolah'] != '') {
            $data_siswa = Siswa::where('deleted', '=', 0)
                ->where('sekolah', '=', $content['sekolah'])
                ->get();
            $data_siswa = $data_siswa->toArray();
        }

        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['data_siswa'] = $data_siswa;
        $content['module'] = $this->getModuleName();
        $content['readonly'] = 'readonly';
        $view = view("user_siswa.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'User Siswa';
        $dataput['title_top'] = 'User Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getDetailSekolahSiswa($siswa)
    {
        $data = Siswa::where('id', '=', $siswa)->first();
        $sekolah_id = "";
        if (!empty($data)) {
            $sekolah_id = $data->toArray()['sekolah'];
        }

        return $sekolah_id;
    }

    public function detail($id)
    {
        $data = Siswa_has_user::where($this->tb . '.id', '=', $id)
            ->select($this->tb . '.*', 'us.username', 'us.password')
            ->join($this->tb_user . ' as us', 'us.id', '=', $this->tb . '.users')
            ->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $this->sekolah_id = $data->sekolah;
        $content = $data->toArray();
        $content['sekolah'] = $this->getDetailSekolahSiswa($data['siswa']);
        $data_siswa = array();

        if ($content['sekolah'] != '') {
            $data_siswa = Siswa::where('deleted', '=', 0)
                ->where('sekolah', '=', $content['sekolah'])
                ->get();
            $data_siswa = $data_siswa->toArray();
        }

        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['data_siswa'] = $data_siswa;
        $content['module'] = $this->getModuleName();
        $content['disabled'] = 'disabled';
        $view = view("user_siswa.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'User Siswa';
        $dataput['title_top'] = 'User Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListData($with_path = "")
    {
        $data = DB::table($this->tb)
            ->join($this->tb_siswa, $this->tb_siswa . '.id', '=', $this->tb . '.siswa')
            ->join($this->tb_sekolah, $this->tb_siswa . '.' . $this->tb_sekolah, '=', $this->tb_sekolah . '.id')
            ->join($this->tb_user, $this->tb . '.' . $this->tb_user, '=', $this->tb_user . '.id')
            ->select(
                $this->tb . ".*",
                $this->tb_sekolah . ".nama_sekolah",
                $this->tb_user . '.username',
                $this->tb_user . '.password',
                $this->tb_siswa . '.nama',
                $this->tb_siswa . '.nis'
            )
            ->where(function ($query) {
                $query->where($this->tb . '.deleted', '=', '0')
                    ->where($this->tb_sekolah . '.deleted', '=', '0');
            })->where(function ($query) {
                $query->Where($this->tb . '.createddate', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_sekolah . '.nama_sekolah', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_user . '.username', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_siswa . '.nama', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_siswa . '.nis', 'like', '%' . $this->keyword . '%');
            })
            ->orderBy($this->tb . '.id', 'desc')
            ->paginate($this->limit);


        if (session('access') != 'superadmin') {
            $data->where($this->tb . '.sekolah', '=', session('sekolah_id'));
        }

        if ($with_path != '') {
            $data->withPath($with_path);
        }

        return $data;
    }

    public function cari(Request $req)
    {
        $this->keyword = trim($req->keyword);
        $data = $this->getListData('cari?keyword=' . $this->keyword . '&' . $this->link_get_url);
        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $content['keyword'] = $this->keyword;
        $view = view("user_siswa.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'User Siswa';
        $dataput['title_top'] = 'User Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getPostInput($param)
    {
        $data = array();
        $data['username'] = $param->username;
        $data['password'] = $param->password;

        return $data;
    }

    public function submit(Request $req)
    {
        $data = json_decode($req['data']);
        $id = $data->id;
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push = $this->getPostInput($data);

            if ($id == '') {
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                $push['hak_akses'] = '6';
                $user = User::insertGetId($push);

                //insert guru user
                $push = array();
                $push['siswa'] = $data->siswa;
                $push['users'] = $user;
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                $id = Siswa_has_user::insertGetId($push);
            } else {
                $user = $data->user;
                $push = array();
                $push['username'] = $data->username;
                $push['password'] = $data->password;
                $push['updateddate'] = date('Y-m-d H:i:s');
                $push['updatedby'] = session('user_id');
                DB::table($this->tb_user)->where('id', '=', $user)->update($push);
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid, 'id' => $id));
    }

    public function delete(Request $req)
    {
        $id = $req['id'];
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push['deleted'] = 1;
            DB::table($this->tb)->where('id', '=', $id)->update($push);
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }
}
