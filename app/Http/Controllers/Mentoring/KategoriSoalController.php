<?php

namespace App\Http\Controllers\Mentoring;

use Exception;
use App\Models\Guru;
use App\Helpers\User;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use App\Models\MataPelajaran;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Jurusan;
use App\Models\Kategori_soal;

class KategoriSoalController extends Controller
{
    public $limit = 5;
    public $tb = "kategori_soal";
    public $tb_sekolah = "sekolah";
    public $tb_jurusan = "jurusan";
    public $tb_mapel = "mata_pelajaran";
    public $tb_guru_mapel = "guru_has_mapel";
    public $keyword = "";
    public $sekolah_id = '';
    public $link_get_url = "menu=kategori_soal&child=mentoring";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/url.js'),
            'js-2' => asset('assets/js/message.js'),
            'js-3' => asset('assets/js/validation.js'),
            'js-4' => asset('assets/js/controllers/kategori_soal.js'),
        );
    }

    public function getModuleName()
    {
        return "kategori soal";
    }

    public function index()
    {
        $data = $this->getListData('?' . $this->link_get_url);

        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $view = view("kategori_soal.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Kategori Soal';
        $dataput['title_top'] = 'Kategori Soal';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListMapel()
    {
        $data = MataPelajaran::where('deleted', '=', '0')
            ->where(function ($query) {
                $query->where('sekolah', 'like', '%' . $this->sekolah_id . '%');
            })->get();

        return $data;
    }

    public function add()
    {
        $data = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $this->sekolah_id = session('sekolah_id');
        $content['sekolah'] = session('sekolah_id');
        $data_mapel = $this->getListMapel();

        // echo '<pre>';
        // print_r($data_jurusan->toArray());
        // die;
        $content['data_sekolah'] = json_decode($data);
        $content['data_mapel'] = $data_mapel->toArray();
        $content['module'] = $this->getModuleName();
        $view = view("kategori_soal.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Kategori Soal';
        $dataput['title_top'] = 'Kategori Soal';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function edit($id)
    {
        $data = Kategori_soal::where('id', '=', $id)->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $this->sekolah_id = $data->sekolah;
        $data_mapel = $this->getListMapel();

        $content = $data->toArray();
        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['data_mapel'] = $data_mapel->toArray();
        $content['module'] = $this->getModuleName();
        $view = view("kategori_soal.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Kategori Soal';
        $dataput['title_top'] = 'Kategori Soal';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function detail($id)
    {
        $data = Kategori_soal::where('id', '=', $id)->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $this->sekolah_id = $data->sekolah;

        $data_mapel = $this->getListMapel();

        $content = $data->toArray();
        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['data_mapel'] = $data_mapel->toArray();
        // $content['data_mapel_guru'] = $data_mapel_guru->toArray();
        $content['module'] = $this->getModuleName();
        $view = view("kategori_soal.detaildata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Kategori Soal';
        $dataput['title_top'] = 'Kategori Soal';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListData($with_path = "")
    {
        $data = DB::table($this->tb)
            ->join($this->tb_sekolah, $this->tb . '.' . $this->tb_sekolah, '=', $this->tb_sekolah . '.id')
            ->join($this->tb_mapel, $this->tb . '.' . $this->tb_mapel, '=', $this->tb_mapel . '.id')
            ->select($this->tb . ".*", $this->tb_sekolah . ".nama_sekolah", $this->tb_mapel . '.mata_pelajaran as mapel')
            ->where(function ($query) {
                $query->where($this->tb . '.deleted', '=', '0')
                    ->where($this->tb_sekolah . '.deleted', '=', '0');
            })->where(function ($query) {
                $query->Where($this->tb . '.kategori', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_sekolah . '.nama_sekolah', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_mapel . '.mata_pelajaran', 'like', '%' . $this->keyword . '%');
            })
            ->orderBy($this->tb . '.id', 'desc')
            ->paginate($this->limit);


        if (session('access') != 'superadmin') {
            $data->where($this->tb . '.sekolah', '=', session('sekolah_id'));
        }
        if ($with_path != '') {
            $data->withPath($with_path);
        }

        return $data;
    }

    public function cari(Request $req)
    {
        $this->keyword = trim($req->keyword);
        $data = $this->getListData('cari?keyword=' . $this->keyword . '&' . $this->link_get_url);
        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $content['keyword'] = $this->keyword;
        $view = view("kategori_soal.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Kategori Soal';
        $dataput['title_top'] = 'Kategori Soal';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getPostInput($param)
    {
        $data = array();
        $data['kategori'] = $param->kategori;
        $data['sekolah'] = $param->sekolah;
        $data['mata_pelajaran'] = $param->mapel;

        return $data;
    }

    public function submit(Request $req)
    {
        $data = json_decode($req['data']);
        $id = $data->id;
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push = $this->getPostInput($data);
            if ($id == '') {
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                $id = Kategori_soal::insertGetId($push);
            } else {
                $push['updateddate'] = date('Y-m-d H:i:s');
                $push['updatedby'] = session('user_id');
                DB::table($this->tb)->where('id', '=', $id)->update($push);
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid, 'id' => $id));
    }

    public function delete(Request $req)
    {
        $id = $req['id'];
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push['deleted'] = 1;
            DB::table($this->tb)->where('id', '=', $id)->update($push);
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function getPostInputGuruHasMapel($param)
    {
        $data = array();
        $data['guru'] = $param->guru;
        $data['mata_pelajaran'] = $param->mapel_id;
        $data['handled'] = $param->checked;

        return $data;
    }

    public function changeMapel(Request $req)
    {
        $data = json_decode($req['data']);
        $is_valid = false;

        DB::beginTransaction();
        try {

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $guru_mapel_id = $value->guru_mapel_id;
                    $push = $this->getPostInputGuruHasMapel($value);
                    if ($guru_mapel_id == '') {
                        DB::table($this->tb_guru_mapel)->insert($push);
                    } else {
                        DB::table($this->tb_guru_mapel)->where('id', '=', $guru_mapel_id)->update($push);
                    }
                }
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }
}
