<?php

namespace App\Http\Controllers\Master;

use Exception;
use App\Models\Guru;
use App\Helpers\User;
use App\Models\Siswa;
use App\Models\Sekolah;
use App\Models\Angkatan;
use Illuminate\Http\Request;
use App\Models\MataPelajaran;
use App\Models\Siswa_has_user;
use App\Models\Siswa_has_kelas;
use App\Models\Siswa_has_angkatan;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SiswaController extends Controller
{
    public $limit = 5;
    public $tb = "siswa";
    public $tb_sekolah = "sekolah";
    public $tb_mapel = "mata_pelajaran";
    public $tb_siswa_kelas = "siswa_has_kelas";
    public $tb_siswa_user = "siswa_has_user";
    public $tb_siswa_angkatan = "siswa_has_angkatan";
    public $tb_kelas = "kelas";
    public $tb_angkatan = "angkatan";
    public $tb_jurusan = "jurusan";
    public $tb_user = "users";
    public $keyword = "";
    public $link_get_url = "menu=siswa&child=master";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/url.js'),
            'js-2' => asset('assets/js/message.js'),
            'js-3' => asset('assets/js/validation.js'),
            'js-4' => asset('assets/js/controllers/siswa.js'),
        );
    }

    public function getModuleName()
    {
        return "siswa";
    }

    public function index()
    {
        $data = $this->getListData('?' . $this->link_get_url);

        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $view = view("siswa.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Siswa';
        $dataput['title_top'] = 'Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function add()
    {
        $data = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $content['sekolah'] = session('sekolah_id');
        $content['data_sekolah'] = json_decode($data);
        $content['module'] = $this->getModuleName();
        $view = view("siswa.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Siswa';
        $dataput['title_top'] = 'Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function edit($id)
    {
        $data = Siswa::where('id', '=', $id)->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();

        $content = $data->toArray();
        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['module'] = $this->getModuleName();
        $view = view("siswa.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Siswa';
        $dataput['title_top'] = 'Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function detail($id)
    {
        $data = Siswa::where('id', '=', $id)->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $data_kelas = DB::table($this->tb_kelas)
            ->where($this->tb_kelas . '.sekolah', '=', $data->sekolah)
            ->select($this->tb_kelas . '.*', 'jur.jurusan as nama_jurusan')
            ->join($this->tb_jurusan . ' as jur', 'jur.id', '=', $this->tb_kelas . '.' . $this->tb_jurusan)
            ->orderBy("jur.jurusan", 'asc')
            ->orderBy($this->tb_kelas . '.nama_kelas', 'asc')
            ->get();
        $data_angkatan = Angkatan::where('deleted', '=', '0')
            ->where('sekolah', '=', $data->sekolah)
            ->get();

        $data_siswa_has_kelas = Siswa_has_kelas::where('deleted', '=', '0')
            ->where('siswa', '=', $data->id)->first();

        $data_siswa_has_angkatan = Siswa_has_angkatan::where('deleted', '=', '0')
            ->where('siswa', '=', $data->id)->first();




        $content = $data->toArray();
        $content['kelas_id'] = !empty($data_siswa_has_kelas) ? $data_siswa_has_kelas->toArray()['kelas'] : '';
        $content['angkatan_id'] = !empty($data_siswa_has_angkatan) ? $data_siswa_has_angkatan->toArray()['angkatan'] : '';

        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['data_kelas'] = $data_kelas->toArray();
        $content['data_angkatan'] = $data_angkatan->toArray();
        $content['module'] = $this->getModuleName();
        $view = view("siswa.detaildata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Siswa';
        $dataput['title_top'] = 'Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListData($with_path = "")
    {
        $data = DB::table($this->tb)
            ->join($this->tb_sekolah, $this->tb . '.' . $this->tb_sekolah, '=', $this->tb_sekolah . '.id')
            ->leftJoin($this->tb_siswa_user, function ($join) {
                $join->on($this->tb_siswa_user . '.siswa', '=', $this->tb . '.id')
                    ->where($this->tb_siswa_user . '.deleted', '=', '0');
            })
            ->select(
                $this->tb . ".*",
                $this->tb_sekolah . ".nama_sekolah",
                $this->tb_siswa_user . '.id as siswa_user_id'
            )
            ->where(function ($query) {
                $query->where($this->tb . '.deleted', '=', '0')
                    ->where($this->tb_sekolah . '.deleted', '=', '0');
            })->where(function ($query) {
                $query->Where($this->tb . '.nama', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb . '.nis', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_sekolah . '.nama_sekolah', 'like', '%' . $this->keyword . '%');
            })
            ->orderBy($this->tb . '.id', 'desc')
            ->paginate($this->limit);

        if (session('access') != 'superadmin') {
            $data->where($this->tb . '.sekolah', '=', session('sekolah_id'));
        }

        if ($with_path != '') {
            $data->withPath($with_path);
        }

        return $data;
    }

    public function cari(Request $req)
    {
        $this->keyword = trim($req->keyword);
        $data = $this->getListData('cari?keyword=' . $this->keyword . '&' . $this->link_get_url);
        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $content['keyword'] = $this->keyword;
        $view = view("siswa.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Siswa';
        $dataput['title_top'] = 'Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getPostInput($param)
    {
        $data = array();
        $data['nama'] = $param->nama;
        $data['nis'] = $param->nis;
        $data['sekolah'] = $param->sekolah;

        return $data;
    }

    public function submit(Request $req)
    {
        $data = json_decode($req['data']);
        $id = $data->id;
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push = $this->getPostInput($data);
            if ($id == '') {
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                $id = Siswa::insertGetId($push);
            } else {
                $push['updateddate'] = date('Y-m-d H:i:s');
                $push['updatedby'] = session('user_id');
                DB::table($this->tb)->where('id', '=', $id)->update($push);
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid, 'id' => $id));
    }

    public function delete(Request $req)
    {
        $id = $req['id'];
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push['deleted'] = 1;
            DB::table($this->tb)->where('id', '=', $id)->update($push);
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function changeKelas(Request $req)
    {
        $is_valid = false;

        DB::beginTransaction();
        try {

            $kelas_id = $req['kelas'];
            $siswa_id = $req['siswa'];
            DB::table($this->tb_siswa_kelas)->where('siswa', '=', $siswa_id)->update(array('deleted' => '1'));


            $push['siswa'] = $siswa_id;
            $push['kelas'] = $kelas_id;
            Siswa_has_kelas::insert($push);
            // DB::table($this->tb_siswa_kelas)->insert($push);

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function changeAngkatan(Request $req)
    {
        $is_valid = false;

        DB::beginTransaction();
        try {

            $angkatan_id = $req['angkatan'];
            $siswa_id = $req['siswa'];
            DB::table($this->tb_siswa_angkatan)->where('siswa', '=', $siswa_id)->update(array('deleted' => '1'));


            $push['siswa'] = $siswa_id;
            $push['angkatan'] = $angkatan_id;
            Siswa_has_angkatan::insert($push);
            // DB::table($this->tb_siswa_kelas)->insert($push);

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function historyKelas(Request $req)
    {
        $siswa = $req['siswa'];
        $data = DB::table($this->tb_siswa_kelas . ' as ts')
            ->select('ts.*', 's.nama as nama_siswa', 'ks.nama_kelas', 'ju.jurusan as nama_jurusan')
            ->join($this->tb . ' as s', 's.id', '=', 'ts.' . $this->tb)
            ->join($this->tb_kelas . ' as ks', 'ks.id', '=', 'ts.' . $this->tb_kelas)
            ->join($this->tb_jurusan . ' as ju', 'ju.id', '=', 'ks.' . $this->tb_jurusan)
            ->where('s.id', '=', $siswa)
            ->orderBy('ts.id', 'desc')
            ->get();

        $content['data'] = $data->toArray();
        return view("siswa.historykelas", $content);
    }

    public function historyAngkatan(Request $req)
    {
        $siswa = $req['siswa'];
        $data = DB::table($this->tb_siswa_angkatan . ' as ts')
            ->select('ts.*', 's.nama as nama_siswa', 'ks.nama_angkatan')
            ->join($this->tb . ' as s', 's.id', '=', 'ts.' . $this->tb)
            ->join($this->tb_angkatan . ' as ks', 'ks.id', '=', 'ts.' . $this->tb_angkatan)
            ->where('s.id', '=', $siswa)
            ->orderBy('ts.id', 'desc')
            ->get();

        $content['data'] = $data->toArray();
        return view("siswa.historyangkatan", $content);
    }

    public function showDetailUser(Request $req)
    {
        $siswa_user = $req['siswa_user'];
        $data = Siswa_has_user::where($this->tb_siswa_user . '.id', '=', $siswa_user)
            ->select($this->tb_siswa_user . '.*', 'us.username', 'us.password', 'sis.sekolah')
            ->join($this->tb_user . ' as us', 'us.id', '=', $this->tb_siswa_user . '.users')
            ->join($this->tb . ' as sis', 'sis.id', '=', $this->tb_siswa_user . '.siswa')
            ->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $this->sekolah_id = $data->sekolah;
        $content = $data->toArray();
        $data_siswa = Siswa::where('deleted', '=', 0)
            ->where('sekolah', '=', $content['sekolah'])
            ->get();
        $data_siswa = $data_siswa->toArray();

        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['data_siswa'] = $data_siswa;
        $content['module'] = $this->getModuleName();
        $content['disabled'] = 'disabled';
        $content['back_button'] = 'hide';
        $content['title'] = 'Detail';

        return view('user_siswa.adddata', $content);
    }
}
