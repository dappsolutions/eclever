<?php

namespace App\Http\Controllers\Master;

use Exception;
use App\Models\Guru;
use App\Helpers\User;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use App\Models\MataPelajaran;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SekolahController extends Controller
{
    public $limit = 5;
    public $tb = "sekolah";
    public $tb_sekolah = "sekolah";
    public $tb_mapel = "mata_pelajaran";
    public $tb_guru_mapel = "guru_has_mapel";
    public $keyword = "";
    public $link_get_url = "menu=sekolah&child=master";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/url.js'),
            'js-2' => asset('assets/js/message.js'),
            'js-3' => asset('assets/js/validation.js'),
            'js-4' => asset('assets/js/controllers/sekolah.js'),
        );
    }

    public function getModuleName()
    {
        return "sekolah";
    }

    public function index(Request $req)
    {
        $data = $this->getListData('?' . $this->link_get_url);

        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $view = view("sekolah.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Sekolah';
        $dataput['title_top'] = 'Sekolah';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function add()
    {
        $content['module'] = $this->getModuleName();
        $view = view("sekolah.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Sekolah';
        $dataput['title_top'] = 'Sekolah';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function edit($id)
    {
        $data = Sekolah::where('id', '=', $id)->first();

        $content = $data->toArray();
        $content['module'] = $this->getModuleName();
        $view = view("sekolah.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Sekolah';
        $dataput['title_top'] = 'Sekolah';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function detail($id)
    {
        $data = Sekolah::where('id', '=', $id)->first();
        $content = $data->toArray();
        $content['module'] = $this->getModuleName();
        $view = view("sekolah.detaildata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Sekolah';
        $dataput['title_top'] = 'Sekolah';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListData($with_path = "")
    {
        $data = DB::table($this->tb)
            ->select($this->tb . ".*")
            ->where(function ($query) {
                $query->where($this->tb . '.deleted', '=', '0')
                    ->whereNotNull($this->tb . '.approve_by');
            })->where(function ($query) {
                $query->Where($this->tb . '.nama_sekolah', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb . '.alamat', 'like', '%' . $this->keyword . '%');
            })
            ->orderBy($this->tb . '.id', 'desc')
            ->paginate($this->limit);


        if ($with_path != '') {
            $data->withPath($with_path);
        }

        return $data;
    }

    public function cari(Request $req)
    {
        $this->keyword = trim($req->keyword);
        $data = $this->getListData('cari?keyword=' . $this->keyword . '&' . $this->link_get_url);
        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $content['keyword'] = $this->keyword;
        $view = view("sekolah.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Sekolah';
        $dataput['title_top'] = 'Sekolah';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getPostInput($param)
    {
        $data = array();
        $data['nama_sekolah'] = $param->sekolah;
        $data['email'] = $param->email;
        $data['no_telp'] = $param->no_telp;
        $data['alamat'] = $param->alamat;

        return $data;
    }

    public function submit(Request $req)
    {
        $data = json_decode($req['data']);
        $id = $data->id;
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push = $this->getPostInput($data);
            if ($id == '') {
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                $push['approve_by'] = session('user_id');
                $id = Sekolah::insertGetId($push);
            } else {
                $push['updateddate'] = date('Y-m-d H:i:s');
                $push['updatedby'] = session('user_id');
                DB::table($this->tb)->where('id', '=', $id)->update($push);
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid, 'id' => $id));
    }

    public function delete(Request $req)
    {
        $id = $req['id'];
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push['deleted'] = 1;
            DB::table($this->tb)->where('id', '=', $id)->update($push);
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function getPostInputGuruHasMapel($param)
    {
        $data = array();
        $data['guru'] = $param->guru;
        $data['mata_pelajaran'] = $param->mapel_id;
        $data['handled'] = $param->checked;

        return $data;
    }

    public function changeMapel(Request $req)
    {
        $data = json_decode($req['data']);
        $is_valid = false;

        DB::beginTransaction();
        try {

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $guru_mapel_id = $value->guru_mapel_id;
                    $push = $this->getPostInputGuruHasMapel($value);
                    if ($guru_mapel_id == '') {
                        DB::table($this->tb_guru_mapel)->insert($push);
                    } else {
                        DB::table($this->tb_guru_mapel)->where('id', '=', $guru_mapel_id)->update($push);
                    }
                }
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }
}
