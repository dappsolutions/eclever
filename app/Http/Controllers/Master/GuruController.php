<?php

namespace App\Http\Controllers\Master;

use Exception;
use App\Models\Guru;
use App\Helpers\User;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use App\Models\Guru_has_user;
use App\Models\MataPelajaran;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class GuruController extends Controller
{
    public $limit = 5;
    public $tb = "guru";
    public $tb_sekolah = "sekolah";
    public $tb_mapel = "mata_pelajaran";
    public $tb_guru_mapel = "guru_has_mapel";
    public $tb_guru_user = "guru_has_user";
    public $tb_user = "users";
    public $keyword = "";
    public $link_get_url = "menu=guru&child=master";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/url.js'),
            'js-2' => asset('assets/js/message.js'),
            'js-3' => asset('assets/js/validation.js'),
            'js-4' => asset('assets/js/controllers/guru.js'),
        );
    }

    public function getModuleName()
    {
        return "guru";
    }

    public function index()
    {
        $data = $this->getListData('?' . $this->link_get_url);

        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $view = view("guru.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Guru';
        $dataput['title_top'] = 'Guru';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function add()
    {
        $data = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $content['sekolah'] = session('sekolah_id');
        $content['data_sekolah'] = json_decode($data);
        $content['module'] = $this->getModuleName();
        $view = view("guru.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Guru';
        $dataput['title_top'] = 'Guru';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function edit($id)
    {
        $data = Guru::where('id', '=', $id)->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();

        $content = $data->toArray();
        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['module'] = $this->getModuleName();
        $view = view("guru.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Guru';
        $dataput['title_top'] = 'Guru';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function detail($id)
    {
        $data = Guru::where('id', '=', $id)->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $data_mapel_guru = DB::table($this->tb_mapel)
            ->where('sekolah', '=', $data->sekolah)
            ->select($this->tb_mapel . '.*', 'gm.id as guru_mapel_id', 'gm.handled')
            ->leftjoin($this->tb_guru_mapel . ' as gm', function ($join) use ($id) {
                $join->on('gm.' . $this->tb_mapel, '=', $this->tb_mapel . '.id')
                    ->where('gm.guru', '=', $id);
            })
            ->get();

        $content = $data->toArray();
        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['data_mapel_guru'] = $data_mapel_guru->toArray();
        $content['module'] = $this->getModuleName();
        $view = view("guru.detaildata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Guru';
        $dataput['title_top'] = 'Guru';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListData($with_path = "")
    {
        $data = DB::table($this->tb)
            ->join($this->tb_sekolah, $this->tb . '.' . $this->tb_sekolah, '=', $this->tb_sekolah . '.id')
            ->leftJoin($this->tb_guru_user, function ($join) {
                $join->on($this->tb . '.id', '=', $this->tb_guru_user . '.guru')
                    ->where($this->tb_guru_user . '.deleted', '=', '0');
            })
            ->select(
                $this->tb . ".*",
                $this->tb_sekolah . ".nama_sekolah",
                $this->tb_guru_user . '.id as guru_user_id'
            )
            ->where(function ($query) {
                $query->where($this->tb . '.deleted', '=', '0')
                    ->where($this->tb_sekolah . '.deleted', '=', '0');
            })->where(function ($query) {
                $query->Where($this->tb . '.nama', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb . '.nip', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb . '.alamat', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_sekolah . '.nama_sekolah', 'like', '%' . $this->keyword . '%');
            })
            ->orderBy($this->tb . '.id', 'desc')
            ->paginate($this->limit);

        if (session('access') != 'superadmin') {
            $data->where($this->tb . '.sekolah', '=', session('sekolah_id'));
        }

        if ($with_path != '') {
            $data->withPath($with_path);
        }

        return $data;
    }

    public function cari(Request $req)
    {
        $this->keyword = trim($req->keyword);
        $data = $this->getListData('cari?keyword=' . $this->keyword . '&' . $this->link_get_url);
        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $content['keyword'] = $this->keyword;
        $view = view("guru.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Guru';
        $dataput['title_top'] = 'Guru';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getPostInput($param)
    {
        $data = array();
        $data['nama'] = $param->nama;
        $data['nip'] = $param->nip;
        $data['alamat'] = $param->alamat;
        $data['sekolah'] = $param->sekolah;

        return $data;
    }

    public function submit(Request $req)
    {
        $data = json_decode($req['data']);
        $id = $data->id;
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push = $this->getPostInput($data);
            if ($id == '') {
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                $id = Guru::insertGetId($push);
            } else {
                $push['updateddate'] = date('Y-m-d H:i:s');
                $push['updatedby'] = session('user_id');
                DB::table($this->tb)->where('id', '=', $id)->update($push);
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid, 'id' => $id));
    }

    public function delete(Request $req)
    {
        $id = $req['id'];
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push['deleted'] = 1;
            DB::table($this->tb)->where('id', '=', $id)->update($push);
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function getPostInputGuruHasMapel($param)
    {
        $data = array();
        $data['guru'] = $param->guru;
        $data['mata_pelajaran'] = $param->mapel_id;
        $data['handled'] = $param->checked;

        return $data;
    }

    public function changeMapel(Request $req)
    {
        $data = json_decode($req['data']);
        $is_valid = false;

        DB::beginTransaction();
        try {

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $guru_mapel_id = $value->guru_mapel_id;
                    $push = $this->getPostInputGuruHasMapel($value);
                    if ($guru_mapel_id == '') {
                        DB::table($this->tb_guru_mapel)->insert($push);
                    } else {
                        DB::table($this->tb_guru_mapel)->where('id', '=', $guru_mapel_id)->update($push);
                    }
                }
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function getDetailSekolahGuru($guru)
    {
        $data = Guru::where('id', '=', $guru)->first();
        $sekolah_id = "";
        if (!empty($data)) {
            $sekolah_id = $data->toArray()['sekolah'];
        }

        return $sekolah_id;
    }

    public function showDetailUser(Request $req)
    {
        $guru_user_id = $req['guru_user_id'];
        $data = Guru_has_user::where($this->tb_guru_user . '.id', '=', $guru_user_id)
            ->select($this->tb_guru_user . '.*', 'us.username', 'us.password')
            ->join($this->tb_user . ' as us', 'us.id', '=', $this->tb_guru_user . '.users')
            ->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();

        $this->sekolah_id = $data->sekolah;
        $content = $data->toArray();
        $content['sekolah'] = $this->getDetailSekolahGuru($data['guru']);
        $data_guru = array();

        if ($content['sekolah'] != '') {
            $data_guru = Guru::where('deleted', '=', 0)
                ->where('sekolah', '=', $content['sekolah'])
                ->get();
            $data_guru = $data_guru->toArray();
        }

        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['data_guru'] = $data_guru;
        $content['module'] = $this->getModuleName();
        $content['disabled'] = 'disabled';
        $content['hide_button'] = 'hide';
        $content['title_content'] = 'Detail User ';
        $view = view("user_guru.adddata", $content);
        return $view;
    }
}
