<?php

namespace App\Http\Controllers\Quiz;

use App\Helpers\NoGenerator;
use Exception;
use App\Models\Guru;
use App\Helpers\User;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use App\Models\MataPelajaran;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Grading_method;
use App\Models\Jam_pelajaran;
use App\Models\Jurusan;
use App\Models\Kategori_soal;
use App\Models\Klasifikasi_jam_mapel;
use App\Models\Siswa_has_ujian;
use App\Models\Soal;
use App\Models\Soal_has_jawaban;
use App\Models\Time_limit;
use App\Models\Ujian;
use App\Models\Ujian_has_soal;
use App\Models\Ujian_jawaban_nilai;
use App\Models\Ujian_penilaian_soal;
use App\Models\Ujian_setting;
use App\Models\Ujian_soal_nilai;
use App\Models\Ujian_status;
use App\Models\Ujian_time;
use App\Models\Ujian_time_jadwal;

class PostQuizController extends Controller
{
    public $limit = 5;
    public $tb = "ujian";
    public $tb_sekolah = "sekolah";
    public $tb_jurusan = "jurusan";
    public $tb_mapel = "mata_pelajaran";
    public $tb_guru_mapel = "guru_has_mapel";
    public $tb_kategori_soal = "kategori_soal";
    public $tb_soal = "soal";
    public $tb_soal_jawaban = "soal_has_jawaban";
    public $tb_ujian_soal = "ujian_has_soal";
    public $tb_kategori_ujian = "kategori_ujian";
    public $tb_ujian_time = "Ujian_time";
    public $tb_kelas = "kelas";
    public $tb_siswa = "siswa";
    public $tb_siswa_ujian = "siswa_has_ujian";
    public $tb_siswa_kelas = "siswa_has_kelas";
    public $tb_klasifikasi_jam = "klasifikasi_jam_mapel";
    public $tb_ujian_time_jadwal = "ujian_time_jadwal";
    public $tb_ujian_setting = "ujian_setting";
    public $tb_ujian_status = "ujian_status";
    public $tb_ujian_penilaian = "ujian_penilaian_soal";
    public $tb_ujian_soal_nilai = "ujian_soal_nilai";
    public $tb_ujian_jawaban_nilai = "ujian_jawaban_nilai";
    public $keyword = "";
    public $sekolah_id = '';
    public $link_get_url = "menu=posting_quiz&child=quiz";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/url.js'),
            'js-2' => asset('assets/js/message.js'),
            'js-3' => asset('assets/js/validation.js'),
            'js-5' => asset('assets/lib/jquery.steps/jquery.steps.js'),
            'js-6' => asset('assets/lib/parsleyjs/parsley.js'),
            'css-7' => asset('assets/plugins/components/bootstrap-daterangepicker/daterangepicker.css'),
            'js-8' => asset('assets/plugins/components/bootstrap-daterangepicker/daterangepicker.js'),
            'js-9' => asset('assets/js/controllers/post_quiz.js'),
        );
    }

    public function getModuleName()
    {
        return "post quiz";
    }

    public function index()
    {
        $data = $this->getListData('?' . $this->link_get_url);

        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $view = view("post_quiz.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Post Quiz';
        $dataput['title_top'] = 'Post Quiz';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListMapel()
    {
        $data = MataPelajaran::where('deleted', '=', '0')
            ->where(function ($query) {
                $query->where('sekolah', 'like', '%' . $this->sekolah_id . '%');
            })->get();

        return $data;
    }

    public function getListKelas()
    {
        $data = Kelas::where($this->tb_kelas . '.deleted', '=', '0')
            ->join($this->tb_jurusan . ' as jur', 'jur.id', '=', $this->tb_kelas . '.jurusan')
            ->select($this->tb_kelas . '.*', 'jur.jurusan as nama_jurusan')
            ->where(function ($query) {
                $query->where($this->tb_kelas . '.sekolah', 'like', '%' . $this->sekolah_id . '%');
            })->get();

        return $data->toArray();
    }

    public function getListDataKategoriSoal($mapel_id)
    {
        $data = Kategori_soal::where('deleted', '=', '0')
            ->where('mata_pelajaran', '=', $mapel_id)->get();

        return $data;
    }

    public function getListDataSoalUjian($ujian)
    {
        $data = Ujian_has_soal::where($this->tb_ujian_soal . '.deleted', '=', '0')
            ->where($this->tb_ujian_soal . '.ujian', '=', $ujian)
            ->select($this->tb_ujian_soal . '.*', 's.soal as soal_term', 'ks.kategori')
            ->join($this->tb_soal . ' as s', 's.id', '=', $this->tb_ujian_soal . '.' . $this->tb_soal)
            ->join($this->tb_kategori_soal . ' as ks', 'ks.id', '=', 's.' . $this->tb_kategori_soal)
            ->get();

        return $data;
    }

    public function getListDataSoalUjianWithAnswer($ujian)
    {
        $data = Ujian_has_soal::where($this->tb_ujian_soal . '.deleted', '=', '0')
            ->where('sj.deleted', '=', '0')
            ->where($this->tb_ujian_soal . '.ujian', '=', $ujian)
            ->select(
                $this->tb_ujian_soal . '.*',
                's.soal as soal_term',
                'ks.kategori',
                'sj.jawaban',
                'sj.id as jawaban_id',
                'sj.true_or_false',
                'usn.id as ujian_soal_nilai_id',
                'usn.poin as poin_soal',
                'ujn.id as ujian_jawaban_id',
                'ujn.poin as poin_jawaban'
            )
            ->join($this->tb_soal . ' as s', 's.id', '=', $this->tb_ujian_soal . '.' . $this->tb_soal)
            ->join($this->tb_soal_jawaban . ' as sj', 's.id', '=', 'sj.soal')
            ->join($this->tb_kategori_soal . ' as ks', 'ks.id', '=', 's.' . $this->tb_kategori_soal)
            ->leftJoin($this->tb_ujian_soal_nilai . ' as usn', 'usn.' . $this->tb_ujian_soal, '=', $this->tb_ujian_soal . '.id')
            ->leftJoin($this->tb_ujian_jawaban_nilai . ' as ujn', function ($join) {
                // $join->on('ujn.' . $this->tb_ujian_soal_nilai, '=', 'usn.id')
                $join->on('ujn.soal_has_jawaban', '=', 'sj.id');
            })
            ->get();


        // $data = DB::select("
        //     select `ujian_has_soal`.*, `s`.`soal` as `soal_term`
        //     , `ks`.`kategori`
        //     , `sj`.`jawaban`
        //     , `sj`.`id` as `jawaban_id`
        //     , `sj`.`true_or_false`
        //     , `usn`.`id` as `ujian_soal_nilai_id`
        //     , `usn`.`poin` as `poin_soal`
        //     , `ujn`.`id` as `ujian_jawaban_id`
        //     , `ujn`.`poin` as `poin_jawaban`
        //     from `ujian_has_soal`
        //     inner join `soal` as `s` on `s`.`id` = `ujian_has_soal`.`soal`
        //     inner join `soal_has_jawaban` as `sj` on `s`.`id` = `sj`.`soal`
        //     inner join `kategori_soal` as `ks` on `ks`.`id` = `s`.`kategori_soal`
        //     left join `ujian_soal_nilai` as `usn` on `usn`.`ujian_has_soal` = `ujian_has_soal`.`id`
        //     left join `ujian_jawaban_nilai` as `ujn` on `ujn`.`ujian_soal_nilai` = `usn`.`id` and `ujn`.`soal_has_jawaban` = sj.id
        //     where `ujian_has_soal`.`deleted` = 0 and `sj`.`deleted` = 0 and `ujian_has_soal`.`ujian` = '" . $ujian . "'");
        // echo '<pre>';
        // print_r($data->toArray());
        // die;
        $result = array();
        if (!empty($data->toArray())) {
            $soal = array();
            foreach ($data->toArray() as $key => $value) {
                if (!in_array($value['soal'], $soal)) {
                    $data_jawaban = array();
                    foreach ($data->toArray() as $v_answ) {
                        if ($v_answ['soal'] == $value['soal']) {
                            array_push($data_jawaban, $v_answ);
                        }
                    }

                    $value['detail_jawaban'] = $data_jawaban;
                    array_push($result, $value);
                    $soal[] = $value['soal'];
                }
            }
        }

        // echo '<pre>';
        // print_r($result);
        // die;
        return $result;
    }

    public function add($kategori_ujian)
    {
        $data = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $this->sekolah_id = session('sekolah_id');
        $content['sekolah'] = session('sekolah_id');

        $data_mapel = $this->getListMapel();
        $content['data_sekolah'] = json_decode($data);
        $content['data_mapel'] = $data_mapel->toArray();
        $content['module'] = $this->getModuleName();
        $content['kategori_ujian'] = $kategori_ujian;
        $content['form_peserta'] = "";
        $content['form_jadwal_ujian'] = $kategori_ujian == '1' ? view('post_quiz.formjadwalujianterbuka', $content) : view('post_quiz.formjadwalujiantertutup', $content);
        $content['list_grade'] = $this->getListGradingMethod();
        $view = view("post_quiz.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Post Quiz';
        $dataput['title_top'] = 'Post Quiz';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }


    public function getListDataTimeLimit()
    {
        $data = Time_limit::where('deleted', '=', '0')
            ->where(function ($query) {
                $query->where('sekolah', 'like', '%' . $this->sekolah_id . '%');
            })
            ->get();
        return $data->toArray();
    }

    public function getDataPesertaUjian($ujian)
    {
        $data = Siswa_has_ujian::where($this->tb_siswa_ujian . '.deleted', '=', '0')
            ->select(
                $this->tb_siswa_ujian . '.*',
                'ks.nama_kelas',
                'jur.jurusan as nama_jurusan',
                's.nama',
                'kj.jam_awal',
                'kj.jam_akhir'
            )
            ->where($this->tb_siswa_ujian . '.ujian', '=', $ujian)
            ->join($this->tb_siswa_kelas . ' as sk', function ($join) {
                $join->on('sk.siswa', '=', $this->tb_siswa_ujian . '.siswa')
                    ->where('sk.deleted', '=', '0');
            })
            ->join($this->tb_kelas . ' as ks', 'ks.id', '=', 'sk.kelas')
            ->join($this->tb_jurusan . ' as jur', 'jur.id', '=', 'ks.jurusan')
            ->join($this->tb_siswa . ' as s', 's.id', '=', $this->tb_siswa_ujian . '.siswa')
            ->leftJoin($this->tb_ujian_time_jadwal . ' as ujt', function ($join) {
                $join->on('ujt.id', '=', $this->tb_siswa_ujian . '.jam_ujian')
                    ->where('ujt.deleted', '=', '0');
            })
            ->leftJoin($this->tb_klasifikasi_jam . ' as kj', 'kj.id', '=', 'ujt.' . $this->tb_klasifikasi_jam)
            ->get();
        return $data->toArray();
    }

    public function getListJamMataPelajaran()
    {
        $data = Jam_pelajaran::where('deleted', '=', '0')
            ->where(function ($query) {
                $query->where('sekolah', 'like', '%' . $this->sekolah_id . '%');
            })
            ->get();
        return $data->toArray();
    }

    public function getListDetailDataJamPelajaran($ujian)
    {
        $data = Klasifikasi_jam_mapel::where($this->tb_klasifikasi_jam . '.deleted', '=', '0')
            ->select($this->tb_klasifikasi_jam . '.*', 'utj.id as ujian_jadwal_id')
            ->leftJoin($this->tb_ujian_time_jadwal . ' as utj', function ($join) use ($ujian) {
                $join->on('utj.' . $this->tb_klasifikasi_jam, '=', $this->tb_klasifikasi_jam . '.id')
                    ->where('utj.deleted', '=', '0');
            })
            ->where('utj.ujian', '=', $ujian)
            ->get();
        return $data->toArray();
    }

    public function getListGradingMethod()
    {
        $data = Grading_method::all();
        return $data->toArray();
    }

    public function edit($id)
    {
        $data = Ujian::where($this->tb . '.id', '=', $id)
            ->select(
                $this->tb . '.*',
                'us.id as id_shuffle',
                'us.shuffle_question',
                'us.shuffle_answer',
                'us.question_per_page',
                'us.remidial',
                'us.grading_method',
                'usp.id as penilaian_id',
                'usp.penilaian'
            )
            ->leftJoin($this->tb_ujian_setting . ' as us', 'us.ujian', '=', $this->tb . '.id')
            ->leftJoin($this->tb_ujian_penilaian . ' as usp', 'usp.ujian', '=', $this->tb . '.id')
            ->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $this->sekolah_id = $data->sekolah;
        $data_mapel = $this->getListMapel();
        $data_kategori_soal = $this->getListDataKategoriSoal($data->mata_pelajaran);
        $data_ujian_has_soal = $this->getListDataSoalUjian($data->id);
        $data_ujian_has_soal_with_answer = $this->getListDataSoalUjianWithAnswer($data->id);
        $ujian_open = Ujian_time::where('ujian', '=', $data->id)->first();

        $content = $data->toArray();
        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['data_mapel'] = $data_mapel->toArray();
        $content['data_kategori_soal'] = $data_kategori_soal->toArray();
        $content['data_ujian_has_soal'] = $data_ujian_has_soal->toArray();
        $content['module'] = $this->getModuleName();
        $content['data_time_limit'] = $data->kategori_ujian == '1' ? $this->getListDataTimeLimit() : array();
        $content['ujian_open'] = empty($ujian_open) ? array() : $ujian_open->toArray();
        $content['kategori_ujian'] = $data->kategori_ujian;

        if ($data->kategori_ujian == '2') {
            $content['list_jam_pelajaran'] = $this->getListJamMataPelajaran();
            $content_jam['data'] = $this->getListDetailDataJamPelajaran($data->id);
            $content['view_list_jam_pelajaran'] = view('post_quiz.listjampelajaran', $content_jam);
            $ujian_open = Ujian_time::where('ujian', '=', $data->id)->first();
            $content['ujian_open'] = empty($ujian_open) ? array() : $ujian_open->toArray();
            $content['form_tanggal_ujian'] = view('post_quiz.formjadwalujianterbuka', $content);
        }

        $content['data_kelas'] = $this->getListKelas();
        $content['form_jadwal_ujian'] = $data->kategori_ujian == '1' ? view('post_quiz.formjadwalujianterbuka', $content) : view('post_quiz.formjadwalujiantertutup', $content);
        $content['data_peserta_ujian'] = $this->getDataPesertaUjian($data->id);
        $content['list_siswa_ujian'] = view('post_quiz.listsiswaujian', $content);
        $content['form_peserta'] = view('post_quiz.formpeserta', $content);
        $content['list_soal_and_answer'] = $data_ujian_has_soal_with_answer;
        $content['form_penilaian_soal'] = view('post_quiz.formpenilaian', $content);
        $content['list_grade'] = $this->getListGradingMethod();

        // echo '<pre>';
        // print_r($content);
        // die;
        $view = view("post_quiz.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Post Quiz';
        $dataput['title_top'] = 'Post Quiz';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function detail($id)
    {
        $data = Ujian::where($this->tb . '.id', '=', $id)
            ->select(
                $this->tb . '.*',
                'us.id as id_shuffle',
                'us.shuffle_question',
                'us.shuffle_answer',
                'us.question_per_page',
                'us.remidial',
                'us.grading_method'
            )
            ->leftJoin($this->tb_ujian_setting . ' as us', 'us.ujian', '=', $this->tb . '.id')
            ->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();
        $this->sekolah_id = $data->sekolah;
        $data_mapel = $this->getListMapel();
        $data_kategori_soal = $this->getListDataKategoriSoal($data->mata_pelajaran);
        $data_ujian_has_soal = $this->getListDataSoalUjian($data->id);

        $content = $data->toArray();
        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['data_mapel'] = $data_mapel->toArray();
        $content['data_kategori_soal'] = $data_kategori_soal->toArray();
        $content['data_ujian_has_soal'] = $data_ujian_has_soal->toArray();
        $content['module'] = $this->getModuleName();
        $content['data_time_limit'] = $this->getListDataTimeLimit();
        $ujian_open = Ujian_time::where('ujian', '=', $data->id)->first();
        $content['ujian_open'] = empty($ujian_open) ? array() : $ujian_open->toArray();
        $content['disabled'] = 'disabled';
        $content['kategori_ujian'] = $data->kategori_ujian;

        if ($data->kategori_ujian == '2') {
            $content['list_jam_pelajaran'] = $this->getListJamMataPelajaran();
            $content_jam['data'] = $this->getListDetailDataJamPelajaran($data->id);
            $content['view_list_jam_pelajaran'] = view('post_quiz.listjampelajaran', $content_jam);
            $ujian_open = Ujian_time::where('ujian', '=', $data->id)->first();
            $content['ujian_open'] = empty($ujian_open) ? array() : $ujian_open->toArray();
            $content['form_tanggal_ujian'] = view('post_quiz.formjadwalujianterbuka', $content);
        }

        $content['data_kelas'] = $this->getListKelas();
        $content['form_jadwal_ujian'] = $data->kategori_ujian == '1' ? view('post_quiz.formjadwalujianterbuka', $content) : view('post_quiz.formjadwalujiantertutup', $content);
        $content['data_peserta_ujian'] = $this->getDataPesertaUjian($data->id);
        $content['list_siswa_ujian'] = view('post_quiz.listsiswaujian', $content);
        $content['form_peserta'] = view('post_quiz.formpeserta', $content);
        $content['list_grade'] = $this->getListGradingMethod();
        $view = view("post_quiz.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Post Quiz';
        $dataput['title_top'] = 'Post Quiz';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListData($with_path = "")
    {
        $data = DB::table($this->tb)
            ->join($this->tb_sekolah, $this->tb . '.' . $this->tb_sekolah, '=', $this->tb_sekolah . '.id')
            ->join($this->tb_mapel, $this->tb . '.' . $this->tb_mapel, '=', $this->tb_mapel . '.id')
            ->join($this->tb_kategori_ujian, $this->tb . '.' . $this->tb_kategori_ujian, '=', $this->tb_kategori_ujian . '.id')
            ->leftJoin($this->tb_ujian_setting, $this->tb . '.id', '=', $this->tb_ujian_setting . '.ujian')
            ->joinSub("select max(id) as id, ujian from " . $this->tb_ujian_status . " group by ujian", "ujian_status_max", "ujian_status_max.ujian", "=", $this->tb . '.id')
            ->join($this->tb_ujian_status . ' as ujs', function ($join) {
                $join->on('ujs.id', '=', 'ujian_status_max.id');
            })
            ->select(
                $this->tb . ".*",
                $this->tb_sekolah . ".nama_sekolah",
                $this->tb_mapel . '.mata_pelajaran as mapel',
                $this->tb_kategori_ujian . '.kategori_ujian as kat_ujian',
                'ujs.status'
            )
            ->where(function ($query) {
                $query->where($this->tb . '.deleted', '=', '0')
                    ->where($this->tb_sekolah . '.deleted', '=', '0')
                    ->where($this->tb_mapel . '.deleted', '=', '0');
            })->where(function ($query) {
                $query->Where($this->tb . '.judul', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb . '.kode_ujian', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb . '.token', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_sekolah . '.nama_sekolah', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_mapel . '.mata_pelajaran', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_kategori_ujian . '.kategori_ujian', 'like', '%' . $this->keyword . '%');
            })
            ->orderBy($this->tb . '.id', 'desc')
            ->paginate($this->limit);

        if (session('access') != 'superadmin') {
            $data->where($this->tb . '.sekolah', '=', session('sekolah_id'));
        }

        if ($with_path != '') {
            $data->withPath($with_path);
        }

        return $data;
    }

    public function cari(Request $req)
    {
        $this->keyword = trim($req->keyword);
        $data = $this->getListData('cari?keyword=' . $this->keyword . '&' . $this->link_get_url);
        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $content['keyword'] = $this->keyword;
        $view = view("post_quiz.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Post Quiz';
        $dataput['title_top'] = 'Post Quiz';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function generateTokenUjian()
    {
        $token = "abcdefghijklmnopqrstuvwxyz0123456789";
        $token = str_shuffle($token);
        $token = substr($token, 0, 5);

        $data = Ujian::where('token', '=', $token)
            ->where('deleted', '=', '0')->get();

        if (!empty($data->toArray())) {
            $token = $data['token'];
        }

        return $token;
    }

    public function getPostInputQuiz($param)
    {
        $no_generator = new NoGenerator();
        $data = array();
        $data['judul'] = $param->judul;
        $data['sekolah'] = $param->sekolah;
        $data['kategori_ujian'] = $param->kategori_ujian;
        $data['mata_pelajaran'] = $param->mapel;
        $data['token'] = $this->generateTokenUjian();
        $data['kode_ujian'] = $no_generator->generateKodeUjian();
        $data['users'] = session('user_id');

        return $data;
    }

    public function submitQuiz(Request $req)
    {
        $data = json_decode($req['data']);
        $id = $data->id;
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push = $this->getPostInputQuiz($data);
            if ($id == '') {
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                $id = Ujian::insertGetId($push);

                //insert ujian status draft
                $push = array();
                $push['ujian'] = $id;
                $push['status'] = 'DRAFT';
                Ujian_status::insert($push);
            } else {
                $push['updateddate'] = date('Y-m-d H:i:s');
                $push['updatedby'] = session('user_id');
                unset($push['kode_ujian']);
                unset($push['token']);
                DB::table($this->tb)->where('id', '=', $id)->update($push);
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid, 'id' => $id));
    }

    public function delete(Request $req)
    {
        $id = $req['id'];
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push['deleted'] = 1;
            DB::table($this->tb)->where('id', '=', $id)->update($push);
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function getPostInputGuruHasMapel($param)
    {
        $data = array();
        $data['guru'] = $param->guru;
        $data['mata_pelajaran'] = $param->mapel_id;
        $data['handled'] = $param->checked;

        return $data;
    }

    public function changeMapel(Request $req)
    {
        $data = json_decode($req['data']);
        $is_valid = false;

        DB::beginTransaction();
        try {

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $guru_mapel_id = $value->guru_mapel_id;
                    $push = $this->getPostInputGuruHasMapel($value);
                    if ($guru_mapel_id == '') {
                        DB::table($this->tb_guru_mapel)->insert($push);
                    } else {
                        DB::table($this->tb_guru_mapel)->where('id', '=', $guru_mapel_id)->update($push);
                    }
                }
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function getListSoal(Request $req)
    {
        $kategori_soal = $req['kategori_soal'];
        $ujian = $req['ujian'];
        // echo $ujian;
        // die;
        $data_soal = Soal::where($this->tb_soal . '.deleted', '=', '0')
            ->select(
                $this->tb_soal . '.*',
                'ks.kategori',
                'uhs.soal as soal_id'
            )
            ->join($this->tb_kategori_soal . ' as ks', 'ks.id', '=', $this->tb_soal . '.' . $this->tb_kategori_soal)
            ->leftJoinSub("select max(id) as id, soal from " . $this->tb_ujian_soal . " where deleted = 0 and ujian = '" . $ujian . "' group by soal", "ujian_max", "ujian_max.soal", "=", $this->tb_soal . '.id')
            ->leftJoin($this->tb_ujian_soal . ' as uhs', function ($join) use ($ujian) {
                $join->on('uhs.id', '=', 'ujian_max.id')
                    ->where('uhs.ujian', '=', $ujian);
            })
            ->where($this->tb_soal . '.kategori_soal', '=', $kategori_soal)
            ->get();
        $content['data'] = $data_soal->toArray();
        return view('post_quiz.listsoal', $content);
    }

    public function chooseSoal(Request $req)
    {
        $id_soal = $req['id_soal'];
        $id_ujian = $req['id_ujian'];

        $is_valid = false;

        DB::beginTransaction();
        try {
            $push = array();
            $push['soal'] = $id_soal;
            $push['ujian'] = $id_ujian;
            $push['createddate'] = date('Y-m-d H:i:s');
            $push['createdby'] = session('user_id');
            Ujian_has_soal::insert($push);

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function deleteSoal(Request $req)
    {
        $id = $req['id'];

        $is_valid = false;

        DB::beginTransaction();
        try {
            $push = array();
            $push['deleted'] = 1;
            DB::table($this->tb_ujian_soal)->where('id', '=', $id)->update($push);

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function getListUjianSoal(Request $req)
    {
        $ujian = $req['id_ujian'];
        $data = $this->getListDataSoalUjian($ujian);
        $content['data'] = $data;
        return view('post_quiz.listujiansoal', $content);
    }

    public function submitJadwalOpenQuiz(Request $req)
    {
        $id = $req['id'];
        $is_valid = false;

        $message = "";
        DB::beginTransaction();
        try {

            list($date_awal, $date_akhir) = explode(' - ', $req['tanggal']);
            $push = array();
            $push['ujian'] = $req['ujian'];
            if ($req['time_limit'] != '') {
                $push['time_limit'] = $req['time_limit'];
            }
            $push['tanggal_ujian'] = date('Y-m-d', strtotime($date_awal));
            $push['tanggal_akhir'] = date('Y-m-d', strtotime($date_akhir));

            if ($id == '') {
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                Ujian_time::insert($push);
            } else {
                $push['updateddate'] = date('Y-m-d H:i:s');
                $push['updatedby'] = session('user_id');
                DB::table($this->tb_ujian_time)->where('id', '=', $id)->update($push);
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }

        return json_encode(array('is_valid' => $is_valid, 'message' => $message));
    }

    public function getListSiswa(Request $req)
    {
        $kelas = $req['kelas'];
        $ujian = $req['ujian'];
        $data_soal = Siswa::where($this->tb_siswa . '.deleted', '=', '0')
            ->select(
                $this->tb_siswa . '.*',
                'ks.nama_kelas',
                'jur.jurusan as nama_jurusan',
                'uhs.id as siswa_ujian_id'
            )
            ->join($this->tb_siswa_kelas . ' as sk', function ($join) {
                $join->on('sk.siswa', '=', $this->tb_siswa . '.id')
                    ->where('sk.deleted', '=', '0');
            })
            ->join($this->tb_kelas . ' as ks', 'ks.id', '=', 'sk.kelas')
            ->join($this->tb_jurusan . ' as jur', 'jur.id', '=', 'ks.jurusan')
            ->leftJoinSub("select max(id) as id, siswa from " . $this->tb_siswa_ujian . " where deleted = 0 and ujian = '" . $ujian . "' group by siswa", "ujian_max", "ujian_max.siswa", "=", $this->tb_siswa . '.id')
            ->leftJoin($this->tb_siswa_ujian . ' as uhs', function ($join) use ($ujian) {
                $join->on('uhs.id', '=', 'ujian_max.id')
                    ->where('uhs.ujian', '=', $ujian);
            })
            ->where('sk.kelas', '=', $kelas)
            ->get();
        $content['data'] = $data_soal->toArray();

        return view('post_quiz.listsiswa', $content);
    }

    public function submitChooseSiswa(Request $req)
    {
        $data = json_decode($req['data']);
        $ujian = $req['ujian'];
        $is_valid = false;

        $message = "";
        DB::beginTransaction();
        try {

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $push = array();
                    $push['ujian'] = $ujian;
                    $push['siswa'] = $value->siswa;
                    $push['createddate'] = date('Y-m-d H:i:s');
                    $push['createdby'] = session('user_id');
                    Siswa_has_ujian::insert($push);
                }
            }
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }

        return json_encode(array('is_valid' => $is_valid, 'message' => $message));
    }

    public function deleteSiswa(Request $req)
    {
        $id = $req['id'];
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push = array();
            $push['deleted'] = 1;
            DB::table($this->tb_siswa_ujian)->where('id', '=', $id)->update($push);
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function getListSiswaUjian(Request $req)
    {
        $ujian = $req['id_ujian'];
        $data = $this->getDataPesertaUjian($ujian);
        $content['data_peserta_ujian'] = $data;
        return view('post_quiz.listsiswaujian', $content);
    }

    public function detailSoal(Request $req)
    {
        $soal = $req['soal'];
        $data = Soal_has_jawaban::where('deleted', '=', '0')
            ->where('soal', '=', $soal)->get();
        $content['data'] = $data->toArray();
        return view('post_quiz.detailsoal', $content);
    }

    public function getListDataJamPelajaran(Request $req)
    {
        $jam_mapel = $req['jam_mapel'];
        $data = Klasifikasi_jam_mapel::where($this->tb_klasifikasi_jam . '.deleted', '=', '0')
            ->where($this->tb_klasifikasi_jam . '.jam_pelajaran', '=', $jam_mapel)
            ->select($this->tb_klasifikasi_jam . '.*', 'utj.id as ujian_jadwal_id')
            ->leftJoin($this->tb_ujian_time_jadwal . ' as utj', function ($join) {
                $join->on('utj.' . $this->tb_klasifikasi_jam, '=', $this->tb_klasifikasi_jam . '.id')
                    ->where('utj.deleted', '=', '0');
            })
            ->get();
        $content['jam_mapel'] = $jam_mapel;
        $content['data'] = $data->toArray();
        return view('post_quiz.listjampelajaran', $content);
    }

    public function submitJamQuiz(Request $req)
    {
        $ujian = $req['ujian'];
        $data = json_decode($req['data']);

        $is_valid = false;
        $message = "";

        DB::beginTransaction();
        try {
            $push = array();
            $push['deleted'] = 1;
            DB::table($this->tb_ujian_time_jadwal)->where('ujian', '=', $ujian)->update($push);

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $push = array();
                    $push['klasifikasi_jam_mapel'] = $value->klasifikasi_jam;
                    $push['ujian'] = $ujian;
                    $push['jam_pelajaran'] = $req['jam_pelajaran'];
                    Ujian_time_jadwal::insert($push);
                }
            }
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }

        return json_encode(array('is_valid' => $is_valid, 'message' => $message));
    }

    public function showListJadwalJamPelajaran(Request $req)
    {
        $ujian = $req['ujian'];
        $data = $this->getListDetailDataJamPelajaran($ujian);
        $content['data'] = $data;
        return view('post_quiz.listjamselect', $content);
    }

    public function setJamUjianSiswa(Request $req)
    {
        $data = json_decode($req['data']);
        $ujian = $req['ujian'];
        $jam_ujian = $req['jam_ujian'];
        $is_valid = false;

        $message = "";
        DB::beginTransaction();
        try {

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $push = array();
                    $push['ujian'] = $ujian;
                    $push['siswa'] = $value->siswa;
                    $push['jam_ujian'] = $jam_ujian;
                    $push['createddate'] = date('Y-m-d H:i:s');
                    $push['createdby'] = session('user_id');
                    Siswa_has_ujian::insert($push);
                }
            }
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }

        return json_encode(array('is_valid' => $is_valid, 'message' => $message));
    }

    public function getPostInputSetting($data)
    {
        $push['ujian'] = $data->ujian;
        $push['shuffle_question'] = $data->shuffle_question;
        $push['shuffle_answer'] = $data->shuffle_answer;
        $push['question_per_page'] = $data->question_per_page;
        $push['remidial'] = $data->remidial;
        $push['grading_method'] = $data->grading_method;
        return $push;
    }

    public function setSettingUjian(Request $req)
    {
        $data = json_decode($req['data']);
        $id_shuffle = $data->id_shuffle;
        $is_valid = false;

        $message = "";
        DB::beginTransaction();
        try {
            $push = $this->getPostInputSetting($data);
            if ($id_shuffle == '') {
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                Ujian_setting::insert($push);
            } else {
                $push['updateddate'] = date('Y-m-d H:i:s');
                $push['updatedby'] = session('user_id');
                DB::table($this->tb_ujian_setting)->where('id', '=', $id_shuffle)->update($push);
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }

        return json_encode(array('is_valid' => $is_valid, 'message' => $message));
    }

    public function publish(Request $req)
    {
        $ujian = $req['ujian'];
        $is_valid = false;

        $message = "";
        DB::beginTransaction();
        try {
            $push['ujian'] = $ujian;
            $push['status'] = 'PUBLISH';
            $push['createddate'] = date('Y-m-d H:i:s');
            $push['createdby'] = session('user_id');
            Ujian_status::insert($push);

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }

        return json_encode(array('is_valid' => $is_valid, 'message' => $message));
    }

    public function submitPenilaian(Request $req)
    {
        $data = json_decode($req['data']);

        // echo '<pre>';
        // print_r($data);
        // die;
        $is_valid = false;

        $message = "";
        DB::beginTransaction();
        try {
            $push = array();
            if ($data->header->penilaian_id == '') {
                $push['ujian'] = $data->ujian;
                $push['penilaian'] = $data->header->penilaian;
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                Ujian_penilaian_soal::insert($push);
            } else {
                $push['penilaian'] = $data->header->penilaian;
                $push['updateddate'] = date('Y-m-d H:i:s');
                $push['updatedby'] = session('user_id');
                DB::table($this->tb_ujian_penilaian)->where('id', '=', $data->header->penilaian_id)->update($push);
            }

            if (!empty($data->item)) {
                foreach ($data->item as $value) {
                    $push = array();
                    $ujian_soal_nilai_id = $value->ujian_soal_nilai_id;
                    if ($ujian_soal_nilai_id == '') {
                        $push['ujian_has_soal'] = $value->ujian_soal_id;
                        $push['poin'] = $value->poin;
                        $push['createddate'] = date('Y-m-d H:i:s');
                        $push['createdby'] = session('user_id');
                        $ujian_soal_nilai_id = Ujian_soal_nilai::insertGetId($push);
                    } else {
                        $push['poin'] = $value->poin;
                        $push['updateddate'] = date('Y-m-d H:i:s');
                        $push['updatedby'] = session('user_id');
                        DB::table($this->tb_ujian_soal_nilai)->where('id', '=', $ujian_soal_nilai_id)->update($push);
                    }

                    //data jawaban
                    if (!empty($value->data_jawaban)) {
                        foreach ($value->data_jawaban as $v_jawab) {
                            $push = array();
                            $jawaban_ujian_id = $v_jawab->ujian_jawaban_id;
                            if ($jawaban_ujian_id == '') {
                                $push['ujian_soal_nilai'] = $ujian_soal_nilai_id;
                                $push['soal_has_jawaban'] = $v_jawab->jawaban_id;
                                $push['poin'] = $v_jawab->poin;
                                $push['createddate'] = date('Y-m-d H:i:s');
                                $push['createdby'] = session('user_id');
                                Ujian_jawaban_nilai::insert($push);
                            } else {
                                $push['poin'] = $v_jawab->poin;
                                $push['updateddate'] = date('Y-m-d H:i:s');
                                $push['updatedby'] = session('user_id');
                                DB::table($this->tb_ujian_jawaban_nilai)->where('id', '=', $jawaban_ujian_id)->update($push);
                            }
                        }
                    }
                }
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }

        return json_encode(array('is_valid' => $is_valid, 'message' => $message));
    }
}
