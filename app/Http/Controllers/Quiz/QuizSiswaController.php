<?php

namespace App\Http\Controllers\Quiz;

use Exception;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Siswa_has_jawaban;
use App\Models\Siswa_has_ujian;
use App\Models\Time_limit;
use App\Models\Ujian_has_soal;

class QuizSiswaController extends Controller
{
    public $limit = 5;
    public $tb = "siswa_has_ujian";
    public $tb_user = "users";
    public $tb_user_profile = "user_has_profile";
    public $tb_ujian = "ujian";
    public $tb_sekolah = "sekolah";
    public $tb_jurusan = "jurusan";
    public $tb_mapel = "mata_pelajaran";
    public $tb_guru_mapel = "guru_has_mapel";
    public $tb_kategori_soal = "kategori_soal";
    public $tb_soal = "soal";
    public $tb_soal_penilaian = "soal_penilaian";
    public $tb_soal_jawaban = "soal_has_jawaban";
    public $tb_ujian_soal = "ujian_has_soal";
    public $tb_kategori_ujian = "kategori_ujian";
    public $tb_ujian_time = "ujian_time";
    public $tb_kelas = "kelas";
    public $tb_siswa = "siswa";
    public $tb_siswa_ujian = "siswa_has_ujian";
    public $tb_siswa_kelas = "siswa_has_kelas";
    public $tb_siswa_user = "siswa_has_user";
    public $tb_siswa_jawaban = "siswa_has_jawaban";
    public $tb_klasifikasi_jam = "klasifikasi_jam_mapel";
    public $tb_ujian_time_jadwal = "ujian_time_jadwal";
    public $tb_ujian_setting = "ujian_setting";
    public $tb_ujian_status = "ujian_status";
    public $keyword = "";
    public $link_get_url = "menu=quiz_siswa&child=master";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/url.js'),
            'js-2' => asset('assets/js/message.js'),
            'js-3' => asset('assets/js/validation.js'),
            'js-3' => asset('assets/js/helper.js'),
            'js-5' => asset('assets/lib/jquery.steps/jquery.steps.js'),
            'js-6' => asset('assets/lib/parsleyjs/parsley.js'),
            'js-4' => asset('assets/js/controllers/quiz_siswa.js'),
        );
    }

    public function getModuleName()
    {
        return "Quiz Siswa";
    }

    public function index()
    {
        $data = $this->getListData('?' . $this->link_get_url);

        // echo '<pre>';
        // print_r($data->toArray());
        // die;
        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $view = view("quiz_siswa.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Quiz Siswa';
        $dataput['title_top'] = 'Quiz Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListDataSoal($req)
    {
        $ujian = $req['ujian'];
        $siswa = $req['siswa'];
        $data = Ujian_has_soal::where($this->tb_ujian_soal . '.deleted', '=', '0')
            ->select(
                $this->tb_ujian_soal . '.id',
                's.soal',
                's.id as soal_id',
                'sj.jawaban',
                'sj.id as jawaban_id',
                'shj.id as siswa_answer_id',
                'shj.jawaban as answer_id'
            )
            ->join($this->tb_soal . ' as s', 's.id', '=', $this->tb_ujian_soal . '.soal')
            ->join($this->tb_soal_jawaban . ' as sj', function ($join) {
                $join->on('sj.soal', '=', 's.id')
                    ->where('sj.deleted', '=', '0');
            })
            ->leftJoin($this->tb_siswa_jawaban . ' as shj', function ($join) use ($siswa) {
                $join->on($this->tb_ujian_soal . '.id', '=', 'shj.' . $this->tb_ujian_soal)
                    ->where('shj.siswa', '=', $siswa);
            })
            ->where($this->tb_ujian_soal . '.ujian', '=', $ujian)
            ->orderBy('shj.id', 'asc')
            ->get();

        $result = array();
        if (!empty($data->toArray())) {
            DB::beginTransaction();
            try {
                $data_soal = $this->getRandomSoal($data, $req['shuffle_question'], $req['siswa']);
                $i = 0;
                foreach ($data_soal as $key => $value) {
                    list(
                        $soal_id, $soal, $siswa_answer_id,
                        $ujian_soal_id, $answer_id, $insert
                    ) = explode(' [AND] ', $value);
                    $req['shuffle_answer'] = $insert == 1 ? $req['shuffle_answer'] : 0;
                    $data_jawaban = $this->getRandomAnswer($soal_id, $data, $req['shuffle_answer']);
                    $result[$i][$soal_id]['soal'] = $soal;
                    $result[$i][$soal_id]['ujian_soal_id'] = $ujian_soal_id;
                    $result[$i][$soal_id]['siswa_answer'] = $siswa_answer_id;
                    $result[$i][$soal_id]['answer_id'] = $answer_id;
                    $result[$i][$soal_id]['jawaban'] = $data_jawaban;
                    $i += 1;
                }
                DB::commit();
            } catch (Exception $ex) {
                // echo $ex->getMessage();
                // die;
                DB::rollBack();
            }
        }

        // echo '<pre>';
        // print_r($result);
        // die;

        return $result;
    }

    public function getRandomAnswer($soal, $data, $is_random)
    {
        $data_jawaban = array();
        if (!$is_random) {
            foreach ($data->toArray() as $v_jawaban) {
                if ($v_jawaban['soal_id'] == $soal) {
                    array_push($data_jawaban, $v_jawaban);
                }
            }
        } else {
            $temp_data_answer = array();
            foreach ($data->toArray() as $key => $value) {
                if ($value['soal_id'] == $soal) {
                    if (!in_array($value['jawaban_id'], $temp_data_answer)) {
                        $temp_data_answer[] = $value['jawaban_id'];
                    }
                }
            }

            $numbers = range(0, count($temp_data_answer) - 1);
            // Randomize the order of array items
            shuffle($numbers);
            $data_answer_random = array();
            foreach ($numbers as $value) {
                $data_answer_random[] = $temp_data_answer[$value];
            }

            $data_jawaban = array();
            foreach ($data_answer_random as $val) {
                foreach ($data->toArray() as $v_jawaban) {
                    if ($v_jawaban['jawaban_id'] == $val) {
                        array_push($data_jawaban, $v_jawaban);
                    }
                }
            }
        }

        return $data_jawaban;
    }

    public function insertSiswaHasJawaban($id, $siswa)
    {
        $data['siswa'] = $siswa;
        $data['ujian_has_soal'] = $id;
        $data['jawaban'] = 0;
        $id = Siswa_has_jawaban::insertGetId($data);
        return $id;
    }

    public function getRandomSoal($data, $is_random, $siswa)
    {
        $data_soal = array();
        $reshufle = 1;
        foreach ($data->toArray() as $key => $value) {
            $value['insert'] = 0;
            if (!in_array($value['soal_id'], $data_soal)) {
                $data_soal[] = $value['soal_id']
                    . ' [AND] ' . $value['soal']
                    . ' [AND] ' . $value['siswa_answer_id']
                    . ' [AND] ' . $value['id']
                    . ' [AND] ' . $value['answer_id']
                    . ' [AND] ' . $value['insert'];
            }
        }

        if ($is_random) {
            $numbers = range(0, count($data_soal) - 1);
            // Randomize the order of array items
            shuffle($numbers);
            $data_soal_random = array();
            foreach ($numbers as $value) {
                list(
                    $soal_id, $soal, $siswa_answer_id,
                    $ujian_soal_id, $answer_id, $insert
                ) = explode(' [AND] ', $data_soal[$value]);
                if ($siswa_answer_id == '') {
                    $siswa_answer_id = $this->insertSiswaHasJawaban($ujian_soal_id, $siswa);
                    $answer_id = 0;
                    $insert = 1;
                } else {
                    $insert = 0;
                    $reshufle = 0;
                }

                $data_soal[$value] = $soal_id
                    . ' [AND] ' . $soal
                    . ' [AND] ' . $siswa_answer_id
                    . ' [AND] ' . $ujian_soal_id
                    . ' [AND] ' . $answer_id
                    . ' [AND] ' . $insert;
                $data_soal_random[] = $data_soal[$value];
            }
            if ($reshufle == 1) {
                $data_soal = $data_soal_random;
            }
        }

        return $data_soal;
    }

    public function kerjakan($id, Request $req)
    {
        $content['siswa_has_ujian'] = $id;
        $content['shuffle_question'] = $req['shuffle_question'];
        $content['shuffle_answer'] = $req['shuffle_answer'];
        $content['question_per_page'] = $req['question_per_page'];
        $content['remidial'] = $req['remidial'];
        $content['grading_method'] = $req['grading_method'];
        $content['siswa'] = $req['siswa'];
        $content['ujian'] = $req['ujian'];

        $data_soal = $this->getListDataSoal($req);
        $content['data_soal'] = $data_soal;
        $content['module'] = $this->getModuleName();
        $view = view("quiz_siswa.formujian", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Quiz Siswa';
        $dataput['title_top'] = 'Quiz Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function detail($id)
    {
        $data = Time_limit::where('id', '=', $id)->first();
        $data_sekolah = Sekolah::where('deleted', '=', '0')->whereNotNull('approve_by')->get();

        $content = $data->toArray();
        $content['data_sekolah'] = json_decode($data_sekolah);
        $content['module'] = $this->getModuleName();
        $view = view("quiz_siswa.detaildata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Quiz Siswa';
        $dataput['title_top'] = 'Quiz Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListData($with_path = "")
    {
        // echo '<pre>';
        // print_r(session()->all());
        // die;
        $data = DB::table($this->tb)
            ->join($this->tb_ujian, $this->tb . '.ujian', '=', $this->tb_ujian . '.id')
            ->join($this->tb_siswa_user, $this->tb_siswa_user . '.siswa', '=', $this->tb . '.siswa')
            ->join($this->tb_sekolah, $this->tb_ujian . '.' . $this->tb_sekolah, '=', $this->tb_sekolah . '.id')
            ->join($this->tb_mapel, $this->tb_ujian . '.' . $this->tb_mapel, '=', $this->tb_mapel . '.id')
            ->join($this->tb_ujian_setting, $this->tb_ujian . '.id', '=', $this->tb_ujian_setting . '.ujian')
            ->leftJoin($this->tb_ujian_time, $this->tb_ujian . '.id', '=', $this->tb_ujian_time . '.ujian')
            ->join($this->tb_kategori_ujian, $this->tb_ujian . '.' . $this->tb_kategori_ujian, '=', $this->tb_kategori_ujian . '.id')
            ->joinSub("select max(id) as id, ujian from " . $this->tb_ujian_status . " group by ujian", "ujian_status_max", "ujian_status_max.ujian", "=", $this->tb_ujian . '.id')
            ->join($this->tb_ujian_status . ' as ujs', function ($join) {
                $join->on('ujs.id', '=', 'ujian_status_max.id');
            })
            ->select(
                $this->tb . ".*",
                $this->tb_sekolah . ".nama_sekolah",
                $this->tb_ujian . ".kode_ujian",
                $this->tb_ujian . ".judul",
                $this->tb_ujian . ".token",
                $this->tb_ujian_time . ".tanggal_ujian",
                $this->tb_ujian_time . ".tanggal_akhir",
                $this->tb_ujian_setting . ".shuffle_question",
                $this->tb_ujian_setting . ".shuffle_answer",
                $this->tb_ujian_setting . ".question_per_page",
                $this->tb_ujian_setting . ".remidial",
                $this->tb_ujian_setting . ".grading_method",
                $this->tb_mapel . '.mata_pelajaran as mapel',
                $this->tb_kategori_ujian . '.kategori_ujian as kat_ujian',
                'ujs.status'
            )
            ->where(function ($query) {
                $query->where($this->tb . '.deleted', '=', '0')
                    ->where($this->tb_sekolah . '.deleted', '=', '0')
                    ->where($this->tb_mapel . '.deleted', '=', '0')
                    ->where($this->tb_siswa_user . '.users', '=', session('user_id'))
                    ->where('ujs.status', '=', 'PUBLISH');
            })
            ->where(function ($query) {
                $query->Where($this->tb_ujian . '.judul', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_ujian . '.kode_ujian', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_ujian . '.token', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_sekolah . '.nama_sekolah', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_mapel . '.mata_pelajaran', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb_kategori_ujian . '.kategori_ujian', 'like', '%' . $this->keyword . '%');
            })
            ->orderBy($this->tb . '.id', 'desc')
            ->paginate($this->limit);

        if ($with_path != '') {
            $data->withPath($with_path);
        }

        // echo '<pre>';
        // print_r($data->toArray());
        // die;

        return $data;
    }

    public function cari(Request $req)
    {
        $this->keyword = trim($req->keyword);
        $data = $this->getListData('cari?keyword=' . $this->keyword . '&' . $this->link_get_url);
        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $content['keyword'] = $this->keyword;
        $view = view("quiz_siswa.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Quiz Siswa';
        $dataput['title_top'] = 'Quiz Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getPostInput($param)
    {
        $data = array();
        $data['time_limit'] = $param->time_limit;
        $data['sekolah'] = $param->sekolah;

        return $data;
    }

    public function answer(Request $req)
    {
        $siswa_answer_id = $req['siswa_answer_id'];
        $jawaban = $req['jawaban'];
        $is_valid = false;
        $message = "";

        DB::beginTransaction();
        try {
            $push['jawaban'] = $jawaban;
            $push['updateddate'] = date('Y-m-d H:i:s');
            $push['updatedby'] = session('user_id');
            DB::table($this->tb_siswa_jawaban)->where('id', '=', $siswa_answer_id)
                ->update($push);

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            DB::rollback();
        }

        return json_encode(array(
            'is_valid' => $is_valid,
            'id' => $siswa_answer_id,
            'message' => $message
        ));
    }

    public function finish(Request $req)
    {
        $siswa_has_ujian = $req['siswa_has_ujian'];

        $is_valid = false;
        $message = "";

        DB::beginTransaction();
        try {
            $push['status'] = 'DONE';
            $push['updateddate'] = date('Y-m-d H:i:s');
            $push['updatedby'] = session('user_id');
            DB::table($this->tb_siswa_ujian)->where('id', '=', $siswa_has_ujian)
                ->update($push);

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            DB::rollback();
        }

        return json_encode(array(
            'is_valid' => $is_valid,
            'message' => $message
        ));
    }

    public function getDetailNilaiQuiz($siswa_has_ujian)
    {
        $data = Siswa_has_ujian::where($this->tb_siswa_ujian . '.id', '=', $siswa_has_ujian)
            ->select(
                $this->tb_siswa_ujian . '.*',
                'usp.foto',
                'ss.nama',
                'ss.nis'
            )
            ->join($this->tb_siswa_user . ' as su', 'su.siswa', '=', $this->tb_siswa_ujian . '.siswa')
            ->join($this->tb_user . ' as us', 'us.id', '=', 'su.users')
            ->join($this->tb_siswa . ' as ss', 'ss.id', '=', $this->tb_siswa_ujian . '.siswa')
            ->leftJoin($this->tb_user_profile . ' as usp', 'us.id', '=', 'usp.users')
            ->first();
        // echo '<pre>';
        // print_r($data->toArray());
        // die;
        $data_jawaban = Siswa_has_jawaban::where('us.ujian', '=', $data['ujian'])
            ->where($this->tb_siswa_jawaban . '.siswa', '=', $data['siswa'])
            ->join($this->tb_ujian_soal . ' as us', 'us.id', '=', $this->tb_siswa_jawaban . '.' . $this->tb_ujian_soal)
            ->join($this->tb_soal . ' as s', 's.id', '=', 'us.soal')
            ->join($this->tb_soal_jawaban . ' as sj', $this->tb_siswa_jawaban . '.jawaban', '=', 'sj.id')
            ->join($this->tb_soal_penilaian . ' as sp', 's.id', '=', 'sp.soal')
            ->get();

        $nilai = 0;
        $data_penilaian_bersarkan_soal = array();
        $data_penilaian_bersarkan_jawaban = array();
        $jawaban_benar = 0;
        $jawaban_salah = 0;
        $total_soal = 0;
        if (!empty($data_jawaban->toArray())) {
            foreach ($data_jawaban->toArray() as $key => $value) {
                if ($value['penilaian'] == '1') {
                    if ($value['true_or_false']) {
                        $nilai += 1;
                        $jawaban_benar += 1;
                    } else {
                        $jawaban_salah += 1;
                    }
                    array_push($data_penilaian_bersarkan_soal, $value);
                } else {
                    array_push($data_penilaian_bersarkan_jawaban, $value);
                }
            }

            $total_soal = count($data_jawaban->toArray());
        }

        $nilai = ($nilai / $total_soal) * 100;
        $push['updateddate'] = date('Y-m-d H:i:s');
        $push['updatedby'] = session('user_id');
        $push['nilai'] = number_format($nilai, 2);
        DB::table($this->tb_siswa_ujian)->where('id', '=', $siswa_has_ujian)->update($push);

        return array(
            'data' => $data->toArray(),
            'nilai' => number_format($nilai, 2),
            'benar' => $jawaban_benar,
            'salah' => $jawaban_salah,
            'total' => $total_soal
        );
    }

    public function getNilaiQuiz($siswa_has_ujian)
    {
        $content['siswa_has_ujian'] = $siswa_has_ujian;
        $content['module'] = $this->getModuleName();
        $data = $this->getDetailNilaiQuiz($siswa_has_ujian);
        $content['data'] = $data['data'];
        $content['nilai'] = $data['nilai'];
        $content['benar'] = $data['benar'];
        $content['salah'] = $data['salah'];
        $content['total'] = $data['total'];
        // echo '<pre>';
        // print_r($content);
        // die;
        $view = view("quiz_siswa.gradequiz", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Nilai Quiz Siswa';
        $dataput['title_top'] = 'Nilai Quiz Siswa';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }
}
