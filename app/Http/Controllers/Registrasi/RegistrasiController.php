<?php

namespace App\Http\Controllers\Registrasi;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Sekolah;

class RegistrasiController extends Controller
{
    public $tb_sekolah = "sekolah";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function index()
    {
        $content['csrf_token'] = csrf_token();
        return view("registrasi.registrasi", $content);
    }


    public function signUp(Request $req)
    {
        $is_valid = false;

        $message = "";
        DB::beginTransaction();
        try {
            $push = array();
            $push['email'] = strtolower($req['email']);
            $push['no_telp'] = $req['no_telp'];
            $push['nama_sekolah'] = strtoupper($req['nama_sekolah']);
            $push['alamat'] = $req['alamat'];
            Sekolah::insert($push);

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            DB::rollback();
        }

        return json_encode(array(
            'is_valid' => $is_valid,
            'message' => $message
        ));
    }
}
