<?php

namespace App\Http\Controllers\Notifikasi;

use Exception;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Sekolah_has_user;
use App\Models\User;

class RegistrasiSekolahController extends Controller
{
    public $limit = 5;
    public $tb = "sekolah";
    public $tb_sekolah = "sekolah";
    public $tb_mapel = "mata_pelajaran";
    public $tb_guru_mapel = "guru_has_mapel";
    public $tb_user = "users";
    public $tb_sekolah_user = "sekolah_has_user";
    public $keyword = "";
    public $link_get_url = "menu=registrasi_sekolah&child=notifikasi";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/url.js'),
            'js-2' => asset('assets/js/message.js'),
            'js-3' => asset('assets/js/validation.js'),
            'js-4' => asset('assets/js/controllers/registrasi_sekolah.js'),
        );
    }

    public function getModuleName()
    {
        return "registrasi sekolah";
    }

    public function index(Request $req)
    {
        $data = $this->getListData('?' . $this->link_get_url);

        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $view = view("registrasi_sekolah.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Registrasi Sekolah';
        $dataput['title_top'] = 'Registrasi Sekolah';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function add()
    {
        $content['module'] = $this->getModuleName();
        $view = view("registrasi_sekolah.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Registrasi Sekolah';
        $dataput['title_top'] = 'Registrasi Sekolah';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function detail($id)
    {
        $data = Sekolah::where('id', '=', $id)->first();
        $data_user = Sekolah_has_user::where($this->tb_sekolah_user . '.deleted', '=', '0')
            ->where('sekolah', '=', $data->id)
            ->join($this->tb_user . ' as us', 'us.id', '=', $this->tb_sekolah_user . '.users')
            ->get();

        $content = $data->toArray();
        $content['module'] = $this->getModuleName();
        $content['data_user'] = $data_user->toArray();
        $view = view("registrasi_sekolah.detaildata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Registrasi Sekolah';
        $dataput['title_top'] = 'Registrasi Sekolah';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListData($with_path = "")
    {
        $data = DB::table($this->tb)
            ->select($this->tb . ".*")
            ->where(function ($query) {
                $query->where($this->tb . '.deleted', '=', '0')
                    ->whereNull($this->tb . '.approve_by');
            })->where(function ($query) {
                $query->Where($this->tb . '.nama_sekolah', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb . '.alamat', 'like', '%' . $this->keyword . '%');
            })
            ->orderBy($this->tb . '.id', 'desc')
            ->paginate($this->limit);


        if ($with_path != '') {
            $data->withPath($with_path);
        }

        return $data;
    }

    public function cari(Request $req)
    {
        $this->keyword = trim($req->keyword);
        $data = $this->getListData('cari?keyword=' . $this->keyword . '&' . $this->link_get_url);
        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $content['keyword'] = $this->keyword;
        $view = view("registrasi_sekolah.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Registrasi Sekolah';
        $dataput['title_top'] = 'Registrasi Sekolah';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getPostInput($param)
    {
        $data = array();
        $data['nama_sekolah'] = $param->sekolah;
        $data['alamat'] = $param->alamat;

        return $data;
    }

    public function approve(Request $req)
    {
        $data = json_decode($req['data']);
        $id = $data->id;
        $is_valid = false;
        $message = "";

        DB::beginTransaction();
        try {
            if ($id != '') {
                $push = array();
                $push['updateddate'] = date('Y-m-d H:i:s');
                $push['updatedby'] = session('user_id');
                $push['approve_by'] = session('user_id');
                DB::table($this->tb)->where('id', '=', $id)->update($push);

                //insert user sekolah
                $password = substr(str_shuffle('1234567890abcdefghijklmn'), 0, 7);

                $push = array();
                $push['username'] = $data->email;
                $push['password'] = $password;
                $push['hak_akses'] = 3;
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                $user = User::insertGetId($push);

                //insert sekolah has user
                $push = array();
                $push['sekolah'] = $id;
                $push['users'] = $user;
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                Sekolah_has_user::insert($push);
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
    }

    public function delete(Request $req)
    {
        $id = $req['id'];
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push['deleted'] = 1;
            DB::table($this->tb)->where('id', '=', $id)->update($push);
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function getPostInputGuruHasMapel($param)
    {
        $data = array();
        $data['guru'] = $param->guru;
        $data['mata_pelajaran'] = $param->mapel_id;
        $data['handled'] = $param->checked;

        return $data;
    }

    public function changeMapel(Request $req)
    {
        $data = json_decode($req['data']);
        $is_valid = false;

        DB::beginTransaction();
        try {

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $guru_mapel_id = $value->guru_mapel_id;
                    $push = $this->getPostInputGuruHasMapel($value);
                    if ($guru_mapel_id == '') {
                        DB::table($this->tb_guru_mapel)->insert($push);
                    } else {
                        DB::table($this->tb_guru_mapel)->where('id', '=', $guru_mapel_id)->update($push);
                    }
                }
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }
}
